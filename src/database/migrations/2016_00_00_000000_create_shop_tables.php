<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateShopTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        //PRODUCT
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });

        //PRODUCT_ITEM
        Schema::create('product_items',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->decimal('price', 10, 2);
            $table->integer('product_id')->unsigned();
            $table->integer('stock')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        //PRODUCT IMAGES
        Schema::create('product_images',function(Blueprint $table){
            $table->increments('id');
            $table->enum('extension', ['jpg','jpeg','png']);
            $table->integer('product_item_id')->unsigned();
            $table->foreign('product_item_id')
                ->references('id')->on('product_items')
                ->onDelete('cascade');
            $table->timestamps();
        });

        //PRODUCT TICKET
         Schema::create('tickets',function(Blueprint $table){
            $table->increments('id');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->integer('product_item_id')->unsigned();
            $table->foreign('product_item_id')
                ->references('id')->on('product_items')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

         //PHYSICAL PRODUCT
         Schema::create('physical',function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_item_id')->unsigned();
            $table->foreign('product_item_id')
                ->references('id')->on('product_items')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

          //ZONES
        Schema::create('zones',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
        });

        //POSTAGE
        Schema::create('postage',function(Blueprint $table){
            $table->increments('id');
            $table->decimal('price', 10, 2);
            $table->integer('product_item_id')->unsigned();
            $table->integer('zone_id')->unsigned();
            $table->foreign('zone_id')
                  ->references('id')->on('zones')
                  ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });



        //COUNTRIES
        Schema::create('countries',function(Blueprint $table){
            $table->string('iso')->unique();
            $table->string('name');
            $table->integer('zone_id');
            $table->foreign('zone_id')
                  ->references('id')->on('zones')
                  ->onDelete('cascade');
        });


        //BESPOKE
         Schema::create('bespoke',function(Blueprint $table){
            $table->increments('id');
            $table->integer('commision');
            //TODO: ADD SELLER ID
            $table->string('quote');
            $table->integer('product_item_id')->unsigned();
            $table->foreign('product_item_id')
                ->references('id')->on('product_items')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

         //ADDRESS
         Schema::create('addresses',function(Blueprint $table)
         {
            $table->increments('id');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->string('city');
            $table->string('county');
            $table->string('postcode');
            $table->string('country');
            $table->timestamps();
            $table->softDeletes();
         });

        //Customer address
        Schema::create('user_addresses',function(Blueprint $table)
         {
            $table->increments('id');
            $table->boolean('is_active');
            $table->boolean('is_billing');
            $table->boolean('is_shipping');
            $table->integer('user_id')->unsigned();
            //customer
            $table->foreign('user_id')
                  ->references('id')->on('users');
            //address
            $table->integer('address_id')->unsigned();
            $table->foreign('address_id')
                  ->references('id')->on('addresses');
            $table->timestamps();
         });

        Schema::create('phone_numbers', function(Blueprint $table){
            $table->increments('id');
            $table->string('code',4);
            $table->string('number',50);
            $table->timestamps();
            $table->softDeletes();
         });

        Schema::create('user_numbers',function(Blueprint $table)
         {
            $table->increments('id');
            $table->boolean('is_active');
            $table->enum('type',['home','work','mobile']);
            $table->integer('user_id')->unsigned();
            //customer
            $table->foreign('user_id')
                  ->references('id')->on('users');

            $table->integer('phone_numbers_id')->unsigned();
            $table->foreign('phone_numbers_id')
                  ->references('id')->on('phone_numbers');
            $table->timestamps();
         });

         //Order
         Schema::create('orders',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            //customer
            $table->foreign('user_id')
                  ->references('id')->on('users');
            //billing
            $table->integer('billing_id')->unsigned();
            $table->foreign('billing_id')
                  ->references('id')->on('addresses');

            $table->integer('phone_number_id')->unsigned();
            $table->foreign('phone_number_id')
                  ->references('id')->on('phone_numbers');

            //shipping
            $table->integer('shipping_id')->unsigned();
            $table->foreign('shipping_id')
                  ->references('id')->on('addresses');

            $table->enum('status',['new','pending','completed','fulfilled']);
            $table->decimal('price', 10, 2);
            $table->timestamps();
         });

        //Order Items
        Schema::create('order_items', function(Blueprint $table){

            $table->increments('id');
            //order
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')
                  ->references('id')->on('orders')
                  ->onDelete('cascade');
            //product item
            $table->integer('product_item_id');
            $table->foreign('product_item_id')
                  ->references('id')->on('product_items');
            //qty
            $table->integer('qty')->unsigned();
            //price at time
            $table->decimal('price', 10, 2);
            $table->decimal('shipping',10,2);
            $table->timestamps();
        });


        Schema::create('order_users', function(Blueprint $table){
            $table->increments('id');
            $table->string('stripe_id')->nullable();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')
                  ->references('id')->on('orders')
                  ->onDelete('cascade');
            $table->timestamps();
        });



        Schema::create('stripe_cards', function(Blueprint $table){
            $table->increments('id');
            $table->string('fingerprint');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  -> references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('stripe_cards');
        Schema::drop('user_numbers');
        Schema::drop('order_users');
        Schema::drop('order_items');
        Schema::drop('orders');
        Schema::drop('phone_numbers');
        Schema::drop('user_addresses');
        Schema::drop('addresses');
        Schema::drop('tickets');
        Schema::drop('physical');
        Schema::drop('bespoke');
        Schema::drop('countries');
        Schema::drop('postage');
        Schema::drop('zones');
        Schema::drop('product_images');
        Schema::drop('product_items');
        Schema::drop('products');
    }
}
