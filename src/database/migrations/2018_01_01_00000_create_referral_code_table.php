<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //visibility
       Schema::create('referral_codes', function (Blueprint $table) {
          
            $table->string('code');
            $table->primary('code');
            $table->integer('uses')->unsigned();
            $table->dateTime('expiry');
            $table->timestamps();
            $table->softDeletes();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_types');
    }
}
