<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //visibility
       Schema::create('blog_authors', function (Blueprint $table) {
			$table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->timestamps();
            $table->softDeletes();
       });

       Schema::table('blog_posts', function($table)
		{
    		    $table->dropColumn('user_id');
    		    $table->integer('author_id')->unsigned();
				$table->foreign('author_id')->references('id')->on('blog_authors');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('blog_posts', function($table)
		{
    		    $table->dropColumn('author_id');
    	});
        Schema::drop('blog_authors');

    }
}
