<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        //tags
       Schema::create('blog_tags', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
       });

        //visibility
       Schema::create('blog_visibility', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
       });

        //status
       Schema::create('blog_statuses', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
       });

        //CATEGORIES
       Schema::create('blog_categories', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
       });



        //POSTS
        Schema::create('blog_posts', function (Blueprint $table) {

            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('excerpt');
            $table->text('content');
            //keys
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users');
            //category
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('blog_categories');
            //status
            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')
                ->references('id')->on('blog_statuses');
            //visibility
            $table->integer('visibility_id')->unsigned();
            $table->foreign('visibility_id')
                ->references('id')->on('blog_visibility');
             //reviewer
            $table->integer('reviewer_id')->unsigned();
            $table->foreign('reviewer_id')
                ->references('id')->on('users');

            $table->timestamp('publish_date');
            $table->timestamps();
            $table->softDeletes();
        });

        //POST SECTIONS
        Schema::create('blog_post_sections', function(Blueprint $table){
            $table->increments('id');
            //keys
            $table->integer('blog_post_id')->unsigned();
            $table->foreign('blog_post_id')
                ->references('id')->on('blog_posts');
            $table->text('content');
        });

        //tags pivot table
        Schema::create('blog_posts_have_tags',function(Blueprint $table){

            $table->integer('blog_post_id')->unsigned()->index();
            $table->foreign('blog_post_id')
                ->references('id')->on('blog_posts')
                ->onDelete('cascade');

            $table->integer('blog_tag_id')->unsigned()->index();
            $table->foreign('blog_tag_id')
                ->references('id')->on('blog_tags')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_posts_have_tags');
        Schema::drop('blog_posts');
        Schema::drop('blog_post_sections');
        Schema::drop('blog_tags');
        Schema::drop('blog_statuses');
        Schema::drop('blog_visibility');
        Schema::drop('blog_categories');
    }
}
