<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post_sections', function(Blueprint $table){
            $table->increments('id');
            //keys
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
                ->references('id')->on('blog_posts');
            $table->text('content');
            $table->integer('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_sections');
    }
}
