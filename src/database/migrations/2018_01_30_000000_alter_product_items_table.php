<?php

use Illuminate\Database\Migrations\Migration;

class AlterProductItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_items', function ($table) {
            $table->integer('stock_alert')->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('stock_alert');
    }
}
