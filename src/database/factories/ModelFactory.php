<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname'    => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Shop\Product\Product::class, function(Faker\Generator $faker) {
	return [
		'name' => $faker->sentence(1),
		'description' => $faker->paragraph(2),
	];
});


$factory->define(App\Shop\Product\Product_item::class, function(Faker\Generator $faker) {
	return [
		'name' => $faker->sentence(1),
		'slug' => $faker->word(1),
		'description' => $faker->paragraph(2),
		'stock' => $faker->numberBetween(0, 50),
		'price' => $faker->randomFloat(2, 1.00, 999.99),
	];
});

$factory->define(App\Shop\Product\Ticket::class, function(Faker\Generator $faker) {
	return [
		'start_time' => $faker->dateTime(),
		'end_time' => $faker->dateTime(),
	];
});

$factory->define(App\Shop\Product\Physical::class, function(Faker\Generator $faker) {
	return [];

});

$factory->define(App\Shop\Product\Bespoke::class, function(Faker\Generator $faker) {
	return [
		'commision' => $faker->randomNumber(2),
		'quote'     => $faker->paragraph(1),
	];

});
