<?php

namespace Rosscturner\Larashop\Repositories;
use Rosscturner\Larashop\Shop\Postage\Country;
use Rosscturner\Larashop\Shop\Postage\Postage;
class PostageRepository
{
	public function getZoneByCountryCode($iso_code)
	{
		
		return Country::where('iso',strtolower($iso_code))->with('zone')->first()->zone;
	}

	public function getPostageByProductAndZone($zone_id, $product_id)
	{
		$where = ['zone_id' => $zone_id, 'product_id' => $product_id];
		return Postage::where($where)->first();
	}


}
