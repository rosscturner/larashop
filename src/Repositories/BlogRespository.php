<?php

namespace Rosscturner\Larashop\Repositories;
use Rosscturner\Larashop\Blog\Category;
use Rosscturner\Larashop\Blog\Tag;

class BlogRepository
{

    public function allCategories()
    {
        return Category::all();
    }

}
