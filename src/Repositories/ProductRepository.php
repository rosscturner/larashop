<?php

namespace Rosscturner\Larashop\Repositories;

use Rosscturner\Larashop\Shop\Product\Product;
use Rosscturner\Larashop\hop\Product\Item;
use Rosscturner\Larashop\Shop\Product\Ticket;
use Rosscturner\Larashop\Shop\Product\Bespoke;
use Rosscturner\Larashop\Shop\Product\Physical;
use Rosscturner\Larashop\Shop\Product\CompoundGroup;
use Carbon\Carbon;

class ProductRepository
{

    public function events(int $limit = null)
    {
        if($limit)
        {
            $events = Product::join('tickets as t', 't.product_id', '=', 'products.id')
            //->where('product_items.stock', '>',0)
            ->whereDate('t.start_time','>',Carbon::today()->toDateString())
            ->orderBy('t.start_time', 'desc')
            ->limit($limit)
            ->has('tickets')
            ->get();
        }
        else{
            $events = Product::
            join('tickets as t', 't.product_id', '=', 'products.id')
            //->orderBy('t.start_time', 'desc')
            
            ->has('tickets')
            //->distinct('product_id')
            ->orderBy('t.start_time','desc')
            ->get();
            // //->unique(function ($item) {
            //     return $item['product_id'] . "|" . $item['seller_id'] . "|" .          
            //               $item['color_id'] . "|" . $item['warranty_id'];
            // });

                // $events = \DB::table('product_items')
                // ->selectRaw(
                //     'product_items.product_id, 
                //     max(product_items.id) as id,
                //     max(product_items.name) as name,
                //     max(product_items.slug) as slug')
                // ->groupBy('product_id')
                // ->join('tickets as t', 't.product_item_id', '=', 'product_items.id')
                
                // ->get();
        }

    	return $events;
    }

    public function eventWithTicketsSold($id)
    {
        //with tickets sold
        return Product::with('tickets','orderItems')->where('id','=',$id)->first();
    }

    public function FindEventByName($name)
    {
        return Product::with(['tickets'])->where('slug', '=', $name)->get();
    }

    public function FindProductByName($name)
    {
        return Product::where('slug', '=', $name)->firstOrFail();
    }

    public function Physical($type)
    {
    	$physical = Physical::with(['product'])->get();
        return $physical;
    }

     public function magazine()
    {
        $physical = Physical::with(['product'])->orderBy('created_at','desc')->first();
        return $physical;
    }

    public function book()
    {
        return Product::with(['physical'])->where('slug','the-food-almanac')->first();
    }

    public function Bespoke(int $limit = null)
    {
        if(!$limit) {
            return Product::with('bespoke','postage')->published()->has('bespoke')->get();
        } else {
            return Product::with('bespoke','postage')->published()->limit($limit)->has('bespoke')->get();
        }
    }

    public function Products()
    {
        return Product::with('physical','bespoke')->doesntHave('tickets')->get();
    }

    public function ticketGroup($slug)
    {
        return \DB::table( 'compound_groups' )
        ->join( 'compound_group_product as cg', 'compound_groups.id', '=', 'cg.compound_group_id' )
        ->join( 'products as pi', 'pi.id', '=', 'cg.product_id' )
        ->join('tickets as ti','ti.product_id' ,'=','cg.product_id')
        ->where( 'compound_groups.slug', $slug )
        ->distinct('ti.product_id')
        ->orderBy('ti.start_time','desc')
        ->select("compound_groups.id","compound_groups.name as group_name","pi.id as product_id","pi.name","pi.slug","pi.sub_heading","pi.description","ti.start_time")
        ->get();
    }

    public function ticketGroups()
    {
        return CompoundGroup::with('products','products.tickets')->whereHas('products' , function($query){
            $query->has('tickets');
        })->get();
    
    }

    public function createEvent($data)
    {
        $product = new Product();
        $ticket = new Ticket();

        $product->name = $data['title'];
        $product->description = $data['description'];

        $product->name = $product->name;
        $product->slug = $data['slug'];
        $product->description = $product->description;
        $product->price = $data['price'];
        $product->stock = $data['stock'];
        $product->sub_heading = $data['sub_heading'];

        $ticket->start_time = $data['start_time'];
        $ticket->end_time   = $data['end_time'];

        \DB::transaction(function() use ($product, $ticket) {
            try{
                $product->product_id = $product->id;
                $product->save();
                $ticket->product_id = $product->id;
                $ticket->save();
            }
             catch (\Exception $e) {
                 dd($e->getMessage());
                //return withErrors(['error' => $e->getMessage()]);
             }
        });

        return $product;
    }

    public function update($id,$data)
    {
        $product = Product::find($id);

        $product->update([
            'name' => $data['title'],
            'slug' => $data['slug'],
            'description' => $data['description'],
            'price' => $data['price'],
            'stock' => $data['stock'],
            'sub_heading' => $data['sub_heading']
        ]);

        $product->ticket->update([
            'start_time' => $data['start_time'],
            'end_time'  => $data['end_time'],
        ]);
        
        return $product;
    }
}
