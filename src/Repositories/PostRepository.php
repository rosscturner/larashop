<?php

namespace Rosscturner\Larashop\Repositories;
use Rosscturner\Larashop\Blog\Post;
use Rosscturner\Larashop\Blog\Visibility;
use Rosscturner\Larashop\Blog\Status;
use Rosscturner\Larashop\Blog\Category;
use Rosscturner\Larashop\Blog\Author;


class PostRepository
{

    public function publishedPosts($limit = null)
    {
        if($limit){
            return Post::with(['author','category'])
                ->published()
                ->visible()
                ->orderBy('publish_date','desc')
                ->limit($limit)
                ->get();
        }
        else {
             return Post::with(['author','category'])
                ->published()
                ->visible()
                ->orderBy('publish_date','desc')
                ->get();  
        }
    }

	public function createPost($data)
	{
		return Post::create($data);
	}

    public function updatePost($id, $data)
    {
        $post = Post::find($id);
        $post->update($data);
        return $post;
    }

	public function posts()
    {
      return Post::with(['author'])->get();
    }

    public function findBySlug($slug)
    {
        $posts['post'] =  $post = Post::with(['author'])
                            ->where('slug', '=', $slug)
                            ->firstOrFail();

        $posts['prev'] = Post::where('id', '<', $post->id)
                        ->visible()
                        ->published()
                        ->orderBy('id','desc')
                        ->limit(1)
                        ->first();

        $posts['next'] = Post::where('id', '>', $post->id)
                        ->visible()
                        ->published()
                        ->limit(1)
                        ->first();


        return $posts;


    }

    public function findById($id)
    {
          return Post::with(['author','category','status','visibility'])->where('id', '=', $id)->firstOrFail();
    }

    public function getSettings()
    {
    	$data = [];
    	$data['visibility'] = Visibility::all();
    	$data['statuses']   = Status::all();
    	$data['categories'] = Category::all();
    	$data['authors']    = Author::all();
    	return $data;
    }

}
