<?php

namespace Rosscturner\Larashop\Repositories;
use Rosscturner\Larashop\Shop\PhoneNumber;

class PhoneRepository
{
  public function CreateNumber($data)
    {
      return PhoneNumber::create([
        'number' => $data['telephone']
      ]);
    }
}
