<?php

namespace Rosscturner\Larashop\Repositories;
use App\User;


class UserRepository
{


    public function Create($data)
    {
    	return User::create([
           'firstname' => $data['first-name'],
           'lastname'    => $data['surname'],
           'email' => $data['email'],
           'password' => bcrypt(str_random(20)),
       ]);
    }
}
