<?php

namespace Rosscturner\Larashop\Repositories;

use Rosscturner\Larashop\Shop\Order;
use Rosscturner\Larashop\Shop\OrderItem;
use Rosscturner\Larashop\Shop\OrderUser;

class OrderRepository
{


    public function all()
    {
      return Order::orderBy('updated_at','desc')->get();
    }

    public function shipped()
    {
      return Order::orderBy('updated_at','desc')->shipped()->get();
    }

    public function unfulfilled()
    {
      return Order::orderBy('updated_at','desc')->unfulfilled()->get();
    }

    public function find($order_id)
    {
      return Order::with(['items','items.bespoke','items.physical','items.ticket','orderUser','shippingAddress'])->findOrFail($order_id);
    }

    public function Create($user_id, $orderDetails, $billingAddress, $shippingAddress, $phone_number, $cart,$code = null)
    {
      //calculate total from cart
      $total = 0.00;
      foreach($cart as &$item)
      {
        if($item->qty > 0){
          if($code)
          {

            $item->price  =  $item->price - ($item->price/100 * $code->discount);
            $total += ((float)$item->price + (float)$item->shipping_rate) * $item->qty;
          }
          else
            $total += ((float)$item->price + (float)$item->shipping_rate) * $item->qty;
          }
      }

      //decrement code usage
      if($code)
      {
        $code->uses = $code->uses -1;
        $code->save();
      }

      $order = Order::create([
            'user_id'         => $user_id,
            'billing_id'      => $billingAddress->id,
            'shipping_id'     => $shippingAddress->id,
            'phone_number_id' => $phone_number->id,
            'status'          => 'new',
            'price'           => $total,
       ]);
      
      foreach($cart as $item) {
        //discount to verv only
        if($item->qty > 0){
          OrderItem::create([
              'order_id'        => $order->id,
              'product_id'      => $item->id,
              'qty'             => $item->qty,
              'shipping'        => $item->shipping_rate,
              'price'           => $item->price,
              'type'            => $item->associatedModel
          ]);
        }
      }

      $orderDetails['order_id'] = $order->id;
      //find anon user with similar details
      $orderUser = OrderUser::where('email',$orderDetails['email'])->first();
      //stripe_id ?
      if($orderUser && $orderUser->stripe_id)
      {
        $orderDetails['stripe_id'] = $orderUser->stripe_id;
      }
      $orderUser = OrderUser::Create($orderDetails);
      return compact('order','orderUser');
    }

    public function findOrderUserByEmail($email)
    {
      return OrderUser::where('email',$email);
    }


    public function destroyOrder($order_id)
    {
        Order::destroy($order_id);
    }


    public function setStatus($order_id,$status)
    {
      $order = Order::find($order_id)->fill(['status' => $status]);
      $order = $order->update() ? $order:false;
      return $order;
    }

    public function findCompletedOrders()
    {
      return Order::leftJoin('order_users', function($join) {
          $join->on('order_users.order_id', '=', 'orders.id');
      })

      ->leftJoin('addresses', function($join){
        $join->on('addresses.id','=','orders.billing_id');
      })

      ->leftJoin('phone_numbers', function($join){
        $join->on('phone_numbers.id','=','orders.phone_number_id');
      })
      ->unfulfilled()
      ->get();
    }
}
