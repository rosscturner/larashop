<?php

namespace Rosscturner\Larashop\Repositories;
use Rosscturner\Larashop\Shop\Address;
use Rosscturner\Larashop\Shop\CustomerAddress;

class AddressRepository
{

	public function createShipping($data)
	{
		return Address::create([

			'address_line_1' 	=> $data['ship_address_line_1'],
			'address_line_2' 	=> $data['ship_address_line_2'],
			'city'				=> $data['ship_city'],
			'county'			=> $data['ship_county'],
			'country'			=> $data['ship_country'],
			'postcode'			=> $data['ship_postcode'],

		]);
	}

    public function createBilling($data)
    {
      return Address::create($data);
    }

}
