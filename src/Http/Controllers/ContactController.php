<?php

namespace Rosscturner\Larashop\Http\Controllers;

use Rosscturner\Larashop\Http\Requests\CreateContactRequest;
use Rosscturner\Larashop\Contact;
use Rosscturner\Larashop\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ContactController extends Controller
{
     /**
     * @param Object $pr
     * @return \Illuminate\View\View
     */
    public function index(PostRepository $pr)
    {
        
    }

    /**
     * Store a contact request
     *
     * @param \jorenvanhocht\Blogify\Requests\PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateContactRequest $request)
    {   
        if($request->input('b_4134b2be4b8935e8947e1879e_404e0abd9b') != '')
        {

            //bot request?
             return view('pages.contact');
        } else {
            try {

                $contact = Contact::create($request->input());
                 $request->session()->flash('success', "Thank you - we'll be in touch soon");
                  \Mail::send(['html' => 'emails.contact'],['contact' => $contact], function($m) use ($contact){
                     $m->to('info@verv.energy', $contact->name)->subject('Verv Contact Form');
                 });
            }
            catch (\Exception $e)
            {
                dd($e->getMessage());
            }
            return view('pages.contact');
        }

    }
}
