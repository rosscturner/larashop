<?php

namespace Rosscturner\Larashop\Http\Controllers;

use Rosscturner\Larashop\Repositories\ProductRepository;
use Rosscturner\Larashop\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use League\Csv;


class ShopController extends Controller
{
     public function index(ProductRepository $pr)
     {
          $products = $pr->bespoke();
          return View('larashop:shop.index',compact('products'));
     }

     public function product($slug, ProductRepository $pr)
     {
          $product = $pr->findProductByName($slug);
          $images = $product->images;
          return view('larashop::shop.product',compact('product','images'));
     }

     public function magazine(ProductRepository $pr)
     {
          $magazine = $pr->magazine();
          $images = $magazine->productItem->images;
          return view('larashop::shop.magazine',compact('magazine','images'));
     }

     public function orders(OrderRepository $or)
     {
        $orders = $or->findCompletedOrders();
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
       
        $orders = $orders->toArray();
//dd($orders);
        $csv->insertOne(array_keys($orders[0]));
        foreach ($orders as $order) {
        
            $csv->insertOne($order);
        }
        $csv->output('orders.csv');

     }
}
