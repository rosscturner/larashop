<?php

namespace Rosscturner\Larashop\Http\Controllers;

use Illuminate\Http\Request;
// use Rosscturner\Larashop\Http\Requests;
use Rosscturner\Larashop\Shop\Product\Bespoke;
use Rosscturner\Larashop\Shop\Product\Ticket;
use Rosscturner\Larashop\Shop\Product\Physical;

use Cart;

class CartController extends \App\Http\Controllers\Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cart(Request $request) {

        $this->removeEmptyItems();
        //fake change
        if ($request->isMethod('post')) 
        {
            if ($request->input('product_id')){
                $product_id = $request->input('product_id');
                $product = Bespoke::find($product_id);
                Cart::add($product);
            }
            if ($request->input('ticket_id')){
                $ticket_id = $request->input('ticket_id');
                $product = Ticket::find($ticket_id);
                $cartItem = Cart::add($product);
            }
            if ($request->input('physical_id')){
                $product_id = $request->input('physical_id');
                $product = Physical::find($product_id);
                Cart::add($product);
            }
        }

        //increment the quantity
        if ($request->input('product_id') && ($request->input('increment')) == 1) {
            $items = Cart::search(
                function($item) use ($request) {
                     return $item->id == $request->input('product_id');
                }
            );
            foreach($items as $rowId => $item)
            {
              Cart::update($rowId, $item->qty + 1);
            }

        }

        //decrease the quantity
        if ($request->input('product_id') && ($request->input('decrease')) == 1) {
            $items = Cart::search(function($item) use ($request) {
                   return $item->id == $request->input('product_id');
                }
            );

            foreach($items as $rowId => $item)
            {
              Cart::update($rowId, $item->qty - 1);
            }
        }

         if ($request->input('product_id') && ($request->input('remove')) == 1) {

            $items = Cart::search(function($item) use ($request) {
                    return $item->id == $request->input('product_id');
                });
            foreach($items as $rowId => $item)
            {
                Cart::remove($rowId);
            }

        }

        $cart = Cart::content();
        // foreach(Cart::content() as $item)
        // {
        //     dd($item->model instanceof \Rosscturner\Larashop\Shop\Product\Product);
        // }
        return view('shop.cart', array('cart' => $cart, 'title' => 'Cart', 'description' => '', 'page' => 'home'));
    }

    /**
    *
    * Remove cart
    * @return void
    *
    */
    public function delete(Request $request)
    {
        Cart::destroy();
        return redirect('/cart');

    }

    private function removeEmptyItems()
    {
        //mremove any zeroed items
         $items = Cart::search(function($item)  {
                    return $item->qty == 0;
                }
            );

        if(!$items->isEmpty())
        {
            foreach($items as $item){
                Cart::remove($item->rowId);
            }
        }

    }

}
