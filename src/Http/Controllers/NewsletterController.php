<?php

namespace Rosscturner\Larashop\Http\Controllers;

use Rosscturner\Larashop\Newsletter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        try {
            Newsletter::create($request->all());
            return array('success' => 'Signed up');
        }
        catch(\Exception $e) {
            return array('error' => 'Already signed up');
        }


    }


    public function unsubscribe(Request $request)
    {
        $email = $request->input('email');
        $nl = Newsletter::where('email',$email)->first();
        $nl->products = false;
        $nl->affiliates = false;
        $nl->events = false;
        $nl->save();
        return view('emails.unsubscribe',compact('email'));
    }


}
