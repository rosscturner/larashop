<?php

namespace Rosscturner\Larashop\Http\Controllers;
// new version
// package imports
use Rosscturner\Larashop\Http\Requests\CreateOrderRequest;
use Rosscturner\Larashop\Http\Requests\ReviewOrderRequest;
use Rosscturner\Larashop\Repositories\UserRepository;
use Rosscturner\Larashop\Repositories\AddressRepository;
use Rosscturner\Larashop\Repositories\OrderRepository;
use Rosscturner\Larashop\Repositories\PhoneRepository;
use Rosscturner\Larashop\Repositories\PostageRepository;
use Rosscturner\Larashop\Notifications\OrderConfirmed;
use Rosscturner\Larashop\Shop\Product\Product;
use App\Newsletter;
// framework imports
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
// Fasades
use Mail;
use Cart;

class CheckoutController extends Controller
{

    public function index()
    {
        $cart = \Cart::content();
        if(is_null($cart)) {
            return redirect('/cart');
        }
        return view('checkout.index');
    }

    public function details(CreateOrderRequest $request,UserRepository $userRepo, AddressRepository $addRepo, OrderRepository $orderRepo,PhoneRepository $phoneRepo,PostageRepository $postRepo)
    {
        //what country are they from - fetch zone
        $zone = $postRepo->getZoneByCountryCode($request->input('country'));
        //get postage for zone
        $cart = $this->checkStock();
        //work out shipping
        $cart = $this->getShippingRate($cart, $zone->id, $postRepo);
        //check cart 
        //NULL if we have a shipping address
        $shippingAddress = null;
        if(is_null($request->input('shipping_address')))
        {
            $shippingAddress = $addRepo->createShipping($request->all());
        }
        //create billing
        $billingAddress = $addRepo->createBilling($request->all());
        $shippingAddress = !is_null($shippingAddress) ? $shippingAddress : $billingAddress;
        //create phone
        $phone = $phoneRepo->createNumber($request->all());

        //create order - anonymous order
        try{
            extract($orderRepo->create( null,
                                        $request->all(),
                                        $billingAddress,
                                        $shippingAddress,
                                        $phone,
                                        $cart
                                    ));
        }
        catch(\Exception $e)
        {
            dd($e);
        }

        //proceed to payment
        return view('checkout.payment',compact( 'orderUser',
                                                'order',
                                                'billingAddress',
                                                'shippingAddress',
                                                'phone',
                                                'cart'
                                            ));
    }

    public function payment(CreateOrderRequest $request, UserRepository $userRepo, AddressRepository $addRepo, OrderRepository $orderRepo,PhoneRepository $phoneRepo,PostageRepository $postRepo)
    {
        try {
            // accepted newsletter
            if($request->input('signup')){
                try {
                    Newsletter::create(['email' => $request->input('email'), 'events'=>true, 'products'=>true,'affiliates'=>false]);
                }
                catch(\Illuminate\Database\QueryException $e) {
                    // already in DB
                }
               
            }

            // check for code
            $code = null;
            if($request->input('referral-code'))
            {
                $result = $this->checkCode($request->input('referral-code'));  
                if($result['verified'] == true)
                {
                    $code = $result['code'];
                }
            }
            

            $order_id = null;
            // what country are they from - fetch zone
            $zone = $postRepo->getZoneByCountryCode($request->input('country'));
            
            // get postage for zone
            $cart = $this->checkStock();
            // work out shipping
            $cart = $this->getShippingRate($cart, $zone->id, $postRepo);

            // NULL if we have a shipping address
            $shippingAddress = null;
            if(!is_null($request->input('shipping_address')))
            {
                $shippingAddress = $addRepo->createShipping($request->all());
            }
            
            // create billing
            $billingAddress = $addRepo->createBilling($request->all());
            $shippingAddress = !is_null($shippingAddress) ? $shippingAddress : $billingAddress;
            
            // create phone
            $phone = $phoneRepo->createNumber($request->all());

            // create order - anonymous order
            try{
                extract($orderRepo->create( null,
                                            $request->all(),
                                            $billingAddress,
                                            $shippingAddress,
                                            $phone,
                                            $cart,
                                            $code));
                                            
            }
            catch(\Exception $e)
            {
                throw new \Exception($e->getMessage());
            }

            $billing = \App::make('Rosscturner\Larashop\Billing\BillingInterface');
  
            // billing id?
            $data = [   'email' => \Request::input('email'),
                        'token' => \Request::input('stripe-token'
                    )];
                   
            // order
            $order  = $orderRepo->setStatus($order->id,'pending');
            $orderUser = $order->orderUser;

            // check for stripe_id on this customer
             if($orderUser->stripe_id)
             {
                 $data['stripe_id'] = $orderUser->stripe_id;
             }



            // create customer
            $customer  = $billing->customer($data);
        
            if(!is_null($customer)){
                //save stripe id to user
                $orderUser->stripe_id = $customer->id;
                $orderUser->save();
            }
                
            // create order key - stops repeat billing for this invoice
            $data['key'] = md5($order->id . $customer->id);

            //c harge customer card
            $data['stripe_id']  = $customer->id;
            $data['total']      = $order->price;
            
            $payment  = $billing->charge($data);

            if(!is_null($payment))
            {
                $status = $payment->status;
                //order sucess
                if($status == 'succeeded')
                {
                    \Cart::destroy();
                    $orderRepo->setStatus($order->id, 'completed');
                    $this->sendSuccessEmails($order);
                    //if checked/subscribe
                    $signup = null;
                    if($request->input('subscribe'))
                    {
                        $signup = $this->signUp($order->orderUser);
                    }
                    return view('checkout.success',compact('payment','order'));
                    //return ['success' => ['payment' => $payment,'order' => $order,'subscribe' => $signup]];
                }
            } else {
                return view('checkout.failed',compact('payment'));
                //return ['failed' => $payment];
            }

        }

        

        catch(\Rosscturner\Larashop\Billing\BillingException $e) {
            //declined for some reason
            $orderRepo->destroyOrder($order->id);
            $errors = $e->getMessage();
            return ['failed' => ['user' => $orderUser,'errors' => $errors]];
        }

        catch(\Rosscturner\Larashop\Billing\StripeException $e)
        {
            //repeat order?
            dd('stripe');
        }

        
    }

    public function checkCode($code,$obj = false)
    {
        if(!strlen($code) >= 5 && !strlen($code) <= 8)
        {
            return ['verified' => false];
        } else {
            
            try {
                $code = \Rosscturner\Larashop\ReferralCode::where(['code' => $code])
                                          ->where('uses','>','0')
                                          ->whereDate('expiry', '>', date('Y-m-d'))
                                          ->firstOrFail();

                return ['verified' => true,'code' => $code];
                
            }
            catch(\Exception $e) {
                return ['verified' => false];
            }
            
        }
    }

    private function sendSuccessEmails($order)
    {
        $order      = $order->load('items', 'billingAddress','shippingAddress','items.bespoke','items.ticket','items.physical');
        $orderUser  = $order->orderUser;
        $items      = $order->items;
        $orderUser->notify(new OrderConfirmed($order,$items));

        // Mail::send(['html' => 'emails.invoice'], ['order'=>$order,'orderUser' => $orderUser,'invoice_id' => $order->id,'items' => $items,'billingAddress' => $order->billingAddress], function($m) use ($orderUser){
        //     $m->to($orderUser->email, $orderUser->firstname . ' ' . $orderUser->lastname)->subject('Payment Received');
        // });
   }

    private function getShippingRate($cart, $zone_id, $postRepo)
    {
        $cart->shipping = 0.00;
        if(is_null($cart))
        {
            return redirect('/cart');
        }

        foreach($cart as $item)
        {
            if(!$item->model instanceof  \Rosscturner\Larashop\Shop\Product\Ticket) {
                $postage = $postRepo->getPostageByProductAndZone($zone_id, $item->model->product_id);

                if(!is_null($postage)){
                    $cart->shipping += $postage->price * $item->qty;
                    $item->shipping_rate = $postage->price;
                    $item->shipping = true;
                } else {
                    //we dont ship this item to this zone
                    $item->qty = 0;
                    $item->shipping_rate += 0.00;
                    $item->shipping = false;
                }
            }
            else
            {
                $item->shipping_rate += 0.00;
                $item->shipping = true;
            }
        }
        return $cart;
    }

    private function checkStock()
    {
        //get cart - no checkout without contents
        $cart = \Cart::content();
        if(is_null($cart)) {
            return redirect('/cart');
        }

        //check stock
        foreach($cart as $item) {
            $product = $item->model;
            // //in stock?
            // if($product instanceof \Rosscturner\Larashop\Shop\Product\Ticket) {
            //     $product = $item->model->type;
            // }
            
            if($product->stock > 0) {
                //enough to satify order?
                if($product->stock >= $item->qty) {
                    $item->altered = false;
                }
                else{
                    $item->altered = true;
                    $item->qty = $product->stock;
                    $product->stock = 0;
                    $product->save();
                }
            }
            else{
                $item->qty = 0;
                $item->altered = true;
            }
            
        }

        return $cart;
    }


    //sign customer up to Mailchimp
    private function signUp($user)
    {
         try {
            $result = \Chimp::post('lists/404e0abd9a/members', [
                    'email_address' => $user->email,
                    'status' => 'subscribed',
                    "email_type" => "html",
                    "merge_fields" => [
                        "FNAME" => $user->firstname,
                        "LNAME" => $user->lastname
                    ],
                ]);
            return $result;
            }
            catch(\Exception $e)
            {
                return $e->getMessage();
            }
    }

}
