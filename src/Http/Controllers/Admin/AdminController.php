<?php

namespace Rosscturner\Larashop\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;

class AdminController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashBoard()
    {
        return view('admin.dashboard');
    }
}
