<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderRepository $or)
    {
        $orders = $or->all();
        $unfulfilled = $or->unfulfilled();
        $shipped = $or->shipped();
        return view('admin.order.index',compact('orders','unfulfilled','shipped'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,OrderRepository $orderRepo)
    {
        $order = $orderRepo->find($id);
        
        return view('admin.order.edit',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fulfill(Request $request, OrderRepository $or,$id)
    {
        $order = $or->setStatus($id,'fulfilled');
        //$request->session()->flash('success', "ORDER SUCCESSFULLY UPDATED");
        return redirect('/admin/orders#unfulfilled')->with('success', 'ORDER SUCCESSFULLY UPDATED');
    }
}
