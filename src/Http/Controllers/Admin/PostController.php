<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePostRequest;
use Carbon\Carbon;

class PostController extends \App\Http\Controllers\Controller
{

    public function index(PostRepository $postRepo)
    {
        $posts = $postRepo->posts();
        return view('admin.blog.index',compact('posts'));

    }

    /**
     * @return \Illuminate\View\View
     */
    public function show($id,PostRepository $postRepo)
    {
        $post = $postRepo->findById($id);
        $settings = $postRepo->getSettings();
        extract($settings);
        return view('admin.blog.edit',compact('post','categories','authors','statuses','visibility'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create(PostRepository $postRepo)
    {
        $settings = $postRepo->getSettings();
        extract($settings);
        return view('admin.blog.create',compact('categories','authors','statuses','visibility'));
    }

    public function store(CreatePostRequest $request, PostRepository $postRepo)
    {
        $data = $request->all();
        $data['author_id'] = $data['author_id'];
        $data['reviewer_id'] = \Auth::user()->id;
        $data['slug'] = str_slug($data['slug']);
        $data['category_id'] = $data['category_id'];
        $data['status_id'] = $data['status_id'];
        $data['visibility_id'] = $data['visibility_id'];
        $data['publish_date'] = $data['publish_date'];
        $post = $postRepo->createPost($data);
        //get post id save the image
        $image = $request->file('jimage');
        $this->saveImage($image,'index.jpg',$post->id . '/');
        return redirect('/admin/blog');
    }


    public function update($id,Request $request,PostRepository $pr)
    {
        $post = $pr->updatePost($id,$request->all());
        $settings = $pr->getSettings();
        extract($settings);
        if($request->file('jimage')){
            $image = $request->file('jimage');
             $this->saveImage($image,'index.jpg',$post->id . '/');
        }
        $request->session()->flash('success', "POST SUCCESSFULLY UPDATED");
         return view('admin.blog.edit',compact('post','categories','authors','statuses','visibility'));
    }

    public function upload(Request $request,$folder = '')
    {
        $image = $request->file('file');
        $imageFileName = time() . '.' . $image->getClientOriginalExtension();
        $filePath = '/blog-images/' . $folder . $imageFileName;
        $url = $this->saveImage($image,$imageFileName);
        return ['url' => "https://s3-eu-west-1.amazonaws.com/at-the-table". $filePath,"id" => 1];

    }

    public function images()
    {

        $images = \Storage::disk('s3')->allFiles('blog-images');
        $urls = [];
        $count = 1;
        foreach($images as $image)
        {
            $urls[] = ['url' => "https://s3-eu-west-1.amazonaws.com/at-the-table/" . $image,
            'thumb' => "https://s3-eu-west-1.amazonaws.com/at-the-table/" . $image,
            'id' => $count];

            $count ++;
        }
        return $urls;
    }

    private function saveImage($image, $filename, $folder = '')
    {
        $s3 = \Storage::disk('s3');
        $filePath = '/blog-images/' . $folder . $filename;
        return  $s3->put($filePath, file_get_contents($image), 'public');
    }



}
