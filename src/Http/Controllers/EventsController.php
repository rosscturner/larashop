<?php

namespace Rosscturner\Larashop\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;


class EventsController extends Controller
{
    public function Events(\Rosscturner\Larashop\Repositories\ProductRepository $pr)
    {
    	$e = $pr->events();

        $event = $e->pop();
        $events = [];
        foreach($e as $ev)
        {
            $events[date('F',strtotime($ev->start_time))][$ev->id] = $ev;
        }

    	return View('events.index',compact('events','event'));
    }


    public function Event(\Rosscturner\Larashop\Repositories\ProductRepository $pr,$name)
    {
    	$event = $pr->findEventByName($name);
    	return View('events.event',compact('event'));
    }
}
