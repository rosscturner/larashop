<?php

namespace Rosscturner\Larashop\Http\Controllers\Api\V1;

// use App\Http\Requests\CreateNewsletterRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rosscturner\Larashop\Shop\Product\Product;
use Rosscturner\Larashop\Shop\Product\Physical;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductController extends Controller
{

    public function __construct()
    {
        // $this->authorizeResource(Newsletter::class,'Newsletter');
    }

    /**
     * Display a listing of Newsletters.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // email is searchable
        return ['data' => Product::where('type','physical')->get()];
        // $posts->distinct();

        // // search string
        // if ($request->has('search')) {
        //     $posts->where(function ($query) use ($request) {
        //         $query->where('title', 'like', "%$request->search%");
        //     });
        // }

        // // order
        // if ($request->has(['orderBy', 'order'])) {
        //     $posts->orderBy('blog_posts.'.$request->orderBy, $request->order);
        // }

        // return $posts->paginate();
    }


    public function getPhysical(Request $request, $id)
    {
        return Physical::where('product_id', $id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $product = Product::create($request->all());
            if($request->input('landscapeIndex')){
                $this->saveImage($product,$request->input('landscapeIndex'),
                                    $request->input('title'),'landscape');
            }
    
            if($request->input('portraitIndex')){
                $this->saveImage($product, $request->input('portraitIndex'),
                                    $request->input('title'),'portrait');
            }

            if($request->input('tags')){
                $product->syncTagsWithType($request->input('tags'),'post');
            }

            return $product;

        } catch (Exception $e){
            //TODO handle this
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product )
    {
        $product = $product->load([
                    'status',
                    'tags',
                    'media' => function($query){
                        $query->where('custom_properties->orientation', ['portrait'])
                                ->orWhere('custom_properties->orientation', ['landscape'])
                                ->orWhere('collection_name', 'product-images');
                            }   
                ]);
        return $product;
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if($request->input('landscapeIndex')){
            $this->saveImage($product,$request->input('landscapeIndex'),
                                $request->input('name'),'landscape');
        }

        if($request->input('portraitIndex')){
            $this->saveImage($product, $request->input('portraitIndex'),
                                $request->input('name'),'portrait');
        }

        if($request->input('tags')){
            $product->syncTagsWithType($request->input('tags'),'product');
        }

        // Update Newsletter
        $product->fill($request->all());
        $product->save();
        return $product;
    }


    public function images( Request $request, Product $product )
    {
        try{
            Media::setNewOrder($request->input('images'));
            return  $media = $product->getMedia('product-images');
        } catch (\Exception $e){
            return $e->getMessage();
        }
        return $request->input('images');
    }


    public function saveGroupImage(Request $request, Product $product) 
    {
        $product->addMediaFromBase64($request->input('image'))
                ->toMediaCollection('product-images','products');

         return $product->load([
                    
                    'media' => function($query){
                        $query->where('custom_properties->orientation', ['portrait'])
                                ->orWhere('custom_properties->orientation', ['landscape'])
                                ->orWhere('collection_name', 'product-images');
                            }   
                ]);
    }

    public function removeImage(Request $request, product $product, Media $media){
       return $media->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $ids = $request->ids;
        if (is_array($ids)) {
            return Product::destroy($ids);
        }
    }


    public function postage( Request $request, Product $product)
    {
        return $product->postage; 
    }


    private function saveImage($product, $image, $title, $orientation)
    {
        // save latest
        $name =  strtolower(str_replace(" ", "-", $title. "-{$orientation}-index"));
        $image = $product->addMediaFromBase64($image)
            ->withCustomProperties(['orientation' => $orientation])
            ->setName($name)
            ->setFileName($name)
            ->toMediaCollection("product-{$orientation}",'products');
    }

}
