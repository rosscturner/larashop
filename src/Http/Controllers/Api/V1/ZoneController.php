<?php

namespace Rosscturner\Larashop\Http\Controllers\Api\V1;

// use App\Http\Requests\CreateNewsletterRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rosscturner\Larashop\Shop\Postage\Zone;

class ZoneController extends Controller
{

    public function __construct()
    {
        // $this->authorizeResource(Newsletter::class,'Newsletter');
    }

    /**
     * Display a listing of Available Zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // email is searchable
        return Zone::all();
    }

}
