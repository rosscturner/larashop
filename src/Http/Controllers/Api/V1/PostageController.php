<?php

namespace Rosscturner\Larashop\Http\Controllers\Api\V1;

// use App\Http\Requests\CreateNewsletterRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rosscturner\Larashop\Shop\Postage\Postage;

class PostageController extends Controller
{

    public function __construct()
    {
        // $this->authorizeResource(Newsletter::class,'Newsletter');
    }

    /**
     * 
     * Return a postage rate
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Postage $postage)
    {
        // email is searchable
        return $postage->load(['zone']);
    }

}
