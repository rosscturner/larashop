<?php

namespace Rosscturner\Larashop\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rosscturner\Larashop\Shop\Product\Physical;

class PhysicalController extends Controller
{

    public function __construct()
    {
        // $this->authorizeResource(Newsletter::class,'Newsletter');
    }

    /**
     * Display a listing of Newsletters.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // email is searchable
        return Ticket::whereIn('product_id',$request->input('parent_id'));
        // $posts->distinct();

        // // search string
        // if ($request->has('search')) {
        //     $posts->where(function ($query) use ($request) {
        //         $query->where('title', 'like', "%$request->search%");
        //     });
        // }

        // // order
        // if ($request->has(['orderBy', 'order'])) {
        //     $posts->orderBy('blog_posts.'.$request->orderBy, $request->order);
        // }

        // return $posts->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            return Physical::create($request->all());

        } catch (Exception $e){
            //TODO handle this
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Physical $physical )
    {
        return $physical;
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        return $request->all();
        // return $ticket->fill($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $ids = $request->ids;
        if (is_array($ids)) {
            return Event::destroy($ids);
        }
    }

    private function saveImage($post, $image, $title, $orientation)
    {
        // save latest
        $name =  strtolower(str_replace(" ", "-", $title. "-{$orientation}-index"));
        $image = $post->addMediaFromBase64($image)
            ->withCustomProperties(['orientation' => $orientation])
            ->setName($name)
            ->setFileName($name)
            ->toMediaCollection("post-{$orientation}",'posts');
    }

}
