<?php

namespace  Rosscturner\Larashop\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Order extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'user'   =>    OrderUser::make($this->orderUser),
            'shipping'  => Address::make($this->shippingAddress),
            'billing'   => Address::make($this->billingAddress),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
