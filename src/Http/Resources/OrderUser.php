<?php

namespace  Rosscturner\Larashop\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrderUser extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname'   => $this->lastname,
            'email'      => $this->email,
            'updated_at' => $this->updated_at,
        ];
    }
}
