<?php

namespace Rosscturner\Larashop\Http\Requests;
use App\Http\Requests\Request;

class CreateContactRequest extends Request {

	public function authorize()
	{
		return true;
	}

	public function rules()
	{

		return [
			'email' => 'required|email',
			'name' => 'required|min:2',
			'phone'  => 'sometimes|numeric|min:6',
            'message' => 'required'
		];
	}

}
