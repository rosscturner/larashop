<?php

namespace Rosscturner\Larashop\Http\Requests;
use App\Http\Requests\Request;

class ReviewOrderRequest extends Request {


	public function authorize()
	{
		return true;
	}

	public function rules()
	{

		return [
			'email' => 'required|email',
			'firstname' => 'required|min:2',
			'lastname'    => 'required|min:2',
			'code'      =>  'required|numeric',
			'telephone'  => 'sometimes|numeric|min:6',
			'address_line_1' => 'required',
			'city'	=> 'required|min:2',
			'country' => 'required',
			'postcode' => 'required',
			//sometimes
			'ship_address_line_1' => 'sometimes|required|min:50',
			'ship_city'	=> 'sometimes|required|min:2',
			'ship_country' => 'sometimes|required',
			'ship_postcode' => 'sometimes|required'

		];
	}

}
