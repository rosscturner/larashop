<?php

namespace Rosscturner\Larashop\Http\Requests;
use App\Http\Requests\Request;

class CreatePostRequest extends Request {

	public function authorize()
	{
		return true;
	}

	public function rules()
	{

		return [
			'title' => 'required|max:200',
			'slug' => 'required|alpha_dash|unique:blog_posts,slug',
			'excerpt'    => 'required|min:10|max:255',
			'content'      =>  'required |min:10',
			'status_id'    => 'required|numeric',
			 'visibility_id'    => 'required|numeric',
			 'author_id'    => 'required|numeric',
			 'category_id'    => 'required|numeric',
			'publish_date' => 'required|date',
		];
	}

}
