<?php
namespace  Rosscturner\Larashop\Billing;

class StripeBilling implements BillingInterface {

	public function __construct()
	{
		\Stripe\Stripe::setApiKey(\Config::get('stripe.secret_key'));
	}
	public function customer(array $data)
	{
		try {
			$customer = null;
			if(!isset($data['stripe_id'])) {
				//create customer
				$customer = \Stripe\Customer::create([
					'card'			=> $data['token'],
					'description' 	=> $data['email']
				]);

			} else {
				$customer = \Stripe\Customer::retrieve($data['stripe_id']);
				$customer->card = $data['token'];
				$customer->save();
			}
			return $customer;
		}
		catch(\Stripe\Error\Card $e) {
			$message = $e->getMessage();
		  	throw new BillingException($message);
		}
		catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
			dd($e->getMessage());
		} catch (\Stripe\Error\InvalidRequest $e) {
			$message = 'attempt repeat transaction';
			throw new BillingException($message);
		  	// Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
			dd($e->getMessage());
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
			dd($e->getMessage());
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
			dd($e->getMessage());
		} catch (\Exception $e) {

		  // Something else happened, completely unrelated to Stripe
			dd($e->getMessage());
		}
	}

	public function charge(array $data)
	{
		try {

			//charge them
			$charge = \Stripe\Charge::create([
				'customer' 	    => $data['stripe_id'],
				'amount' 		=> filter_var($data['total'], FILTER_SANITIZE_NUMBER_INT),
				'currency' 		=> 'gbp',
			],
			[ "idempotency_key" => $data['key']]);

			return $charge;
		}

		catch(\Stripe\Error\Card $e) {
			// Since it's a decline, \Stripe\Error\Card will be caught

			$message = $e->getMessage();
		  	throw new BillingException($message);
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
			dd('rate');
			dd($e->getMessage());
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
			dd($e->getMessage());
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
			dd('auth failed');
			dd($e->getMessage());
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
			dd($e->getMessage());
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
			dd($e->getMessage());
		} catch (\Exception $e) {
		  // Something else happened, completely unrelated to Stripe
			dd($e->getMessage());
		}
	}
}
