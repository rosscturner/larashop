<?php

namespace Rosscturner\Larashop\Billing;

interface BillingInterface {

	public function customer(array $data);
	public function charge(array $data);

}
