<?php

namespace Rosscturner\Larashop;

use Illuminate\Support\ServiceProvider;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        //routes
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        //routes
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        //views
        $this->loadViewsFrom(__DIR__.'/views', 'larashop');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('App\Billing\BillingInterface','App\Billing\StripeBilling');
        // $this->app->make('Rosscturner\Larashop\Http\Controllers\CheckoutController');
    }
}
