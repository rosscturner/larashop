<?php 
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['api'],'prefix'=>'admin', 'namespace' => 'Rosscturner\\Larashop\\Http\\Controllers\\Api\\Admin'], function() {
    
   
        // //dashboard
        // Route::get('/', 'AdminController@getDashboard')->name('dashboard');
        // //newsletter
        // Route::get('/newsletter','NewsletterController@index')->name('newsletter');
        // //orders
        // Route::resource('/orders','OrderController');
        // Route::get('/orders/fulfill/{id}','OrderController@fulfill');
        // //events
        // Route::get('/events','EventController@index');
        // Route::get('/event/{id}','EventController@show');
        // //products
        // Route::get('/products','ProductController@index');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web'],'namespace'=>'Rosscturner\\Larashop\\Http\\Controllers'], function() {
  
    Route::get('/users','Api\UserController@index');

    // Route::get('/contact', function(){
    //     return view('larashop::pages.contact');
    // });
   
    Route::get('/order/success/{order_id}','CheckoutController@success');
    Route::get('/checkout','CheckoutController@index');
    Route::post('/checkout','CheckoutController@details');
    Route::get('/checkout/checkcode/{code}','CheckoutController@checkcode');
    Route::post('/checkout/payment','CheckoutController@payment');
    
    //cart
    Route::get('/cart', 'CartController@cart');
    Route::post('/cart', 'CartController@cart');
    Route::get('/clear-cart', 'CartController@delete');
    
    //view invoice in browser
    // Route::get('/invoice/{invoice_id}',function($invoice_id){
    //     $or = new \Rosscturner\Larashop\Repositories\OrderRepository;
    //     $order = $or->find($invoice_id);
    //     $order      = $order->load('items', 'billingAddress','shippingAddress','items.bespoke','items.physical','items.ticket');
    //     $orderUser  = $order->orderUser;
    //     $items      = $order->items;
    //     return view('larashop::emails.invoice',['order'=>$order,'orderUser' => $orderUser,'invoice_id' => $order->id,'items' => $items,'billingAddress'=> $order->billingAddress, 'shippingAddress' => $order->shippingAddress]);
    // });

    //editorial
    Route::get('/editorial','PostController@index');
    Route::get('/editorial/{slug}','PostController@view');

    //magazine
    Route::get('/magazine','ShopController@magazine')->name('magazine');

    //view email newsletter
    // Route::get('/email/{template}',function($template){

    //     //  \Mail::send(['html' => 'emails.product-launch'], ['email' => 'ross.c.turner@gmail.com'], function($m) use ($email){
    //     //     $m->to($email)->subject('At The Table Products');
    //     // });
    //     $email = '';
    //     return view('larashop::emails.'.$template,compact('email'));
    // });

   

    // Route::post('/contact','ContactController@store');

    
    

});


