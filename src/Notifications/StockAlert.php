<?php

namespace Rosscturner\Larashop\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class StockAlert extends Notification
{
    use Queueable;

    private $product;
    private $item;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($product,$item)
    {
        $this->product = $product;
        $this->item    = $item;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
        $product  = $this->product;
        $item     = $this->item;

        return (new SlackMessage)
                ->warning()
                ->content('Product Stock Alert')
                ->attachment(function ($attachment) use ($product,$item) {
                    $attachment->title('Product below threshold')
                        ->fields([
                                    'Stock'        => $product->stock - $item->qty,
                                    'Name'         => $product->description
                                    ]
                                );
        });
    }

}
