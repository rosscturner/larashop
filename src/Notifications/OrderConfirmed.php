<?php

namespace Rosscturner\Larashop\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Messages\MailMessage;
class OrderConfirmed extends Notification
{
    use Queueable;

    private $order;
    private $orderUser;
    private $items;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order,$items)
    {
        $this->order = $order;
        $this->orderUser = $order->orderUser;
        $this->items = $items;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toSlack($notifiable)
    {
        $order  = $this->order;
        $orderUser = $this->orderUser;
        $items     = $this->items;

        return (new SlackMessage)
                ->success()
                ->content("New Order Received  ( " . \App::environment()  . " )")
                ->attachment(function ($attachment) use ($order,$orderUser,$items) {
                    $attachment->title('New Order Received')
                        ->fields([
                                    'Order No'     => $order->id,
                                    'Name'         => $orderUser->firstname . ' ' . $orderUser->lastname,
                                    'Email'        => $orderUser->email,
                                    'items'         => $items,

                                    ]
                                );
        });
    }

    public function toMail($notifiable)
    {
        $order = $this->order;
        $orderUser   = $this->orderUser;
        $items     = $this->items;

        $mail   = new MailMessage();
        $mail->subject('Payment Received');
        $mail->bcc(\Config::get('shop.order_confirmed'));
        $mail->view('emails.invoice',[
            'order'         =>  $order,
            'orderUser'     =>  $order->orderUser,
            'invoice_id'    =>  $order->id,
            'items'         => $items,
            'billingAddress' => $order->billingAddress,
            'shippingAddress' => $order->shippingAddress
        ]);
        return $mail;
    }

}
