<?php

return [
	'order_confirmed' 			=> env('ORDER_CONFIRMED', 'orders@verv.energy'),
	'order_confirmed_webhook' 	=> env('ORDER_CONFIRMED_WEBHOOK','https://hooks.slack.com/services/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'),
];