<?php

namespace Rosscturner\Larashop\Providers;

use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Rosscturner\Larashop\Billing\BillingInterface','Rosscturner\Larashop\Billing\StripeBilling');
    }
}
