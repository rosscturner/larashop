<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	protected $fillable = ['order_id','product_id','price','shipping','qty','type'];

	public function order()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Order');
	}

	public function bespoke()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Bespoke','product_id');
	}

	public function physical()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Physical','product_id');
	}

	public function ticket()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Ticket','product_id');
	}
}
