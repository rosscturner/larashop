<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
	protected $primaryKey = null;
    public $incrementing = false;
	protected $fillable = ['is_active','is_billing','is_shipping','user_id','address_id'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function address()
	{
		return $this->hasOne('App\Shop\Address');
	}
}
