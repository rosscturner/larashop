<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{


	protected $fillable = ['is_active','type','code','number','user_id'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
