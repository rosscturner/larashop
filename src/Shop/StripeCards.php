<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class StripeCards extends Model
{


	protected $fillable = ['user_id','fingerprint'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
