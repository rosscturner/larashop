<?php

namespace Rosscturner\Larashop\Shop\Product;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model implements Buyable
{

	protected $dates = [
		'start_time',
		'end_time'
    ];
	protected $fillable = ['start_time','end_time','description','price','stock','product_id'];
	
	public function getBuyablePrice($options = null)
	{
		return $this->price;
	}

	public function getBuyableIdentifier($options = null)
	{
		return $this->id;
	}

	public function getBuyableDescription($options = null)
	{
		return $this->description;
	}

	public function product()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Product');
	}

	public function orderItems()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem');
	}
}
