<?php

namespace Rosscturner\Larashop\Shop\Product;

use Illuminate\Database\Eloquent\Model;

class Product_image extends Model
{
	public function product()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Product');
	}
}
