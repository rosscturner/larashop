<?php

namespace Rosscturner\Larashop\Shop\Product;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class Bespoke extends Model implements Buyable
{
	protected $table = 'bespoke';

	public function getBuyableIdentifier($options = null)
	{
		return $this->id;
	}

	public function getBuyableDescription($options = null)
	{
		return $this->item->name;
	}

	public function getBuyablePrice($options = null)
	{
		return $this->price;
	}

	public function item()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Product','product_id');
	}

	public function orderItems()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem');
	}
}
