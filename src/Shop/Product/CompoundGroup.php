<?php

namespace Rosscturner\Larashop\Shop\Product;

use Illuminate\Database\Eloquent\Model;

class CompoundGroup extends Model
{
	public function products()
	{
		return $this->belongsToMany('Rosscturner\Larashop\Shop\Product\Product');
	}

	
}
