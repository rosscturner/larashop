<?php

namespace Rosscturner\Larashop\Shop\Product;

use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class Product extends Model implements HasMedia 
{
	use InteractsWithMedia;
	use HasTags;
	
	protected $fillable = [
			'name','slug','description',
			'price','stock','sub_heading',
			'stock_alert','teaser'];

	public function images()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Product\Product_image');
	}

	public function physical()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Product\Physical');
	}

	public function tickets()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Product\Ticket');
	}

	public function bespoke()
	{
		return $this->hasOne('Rosscturner\Larashop\Shop\Product\Bespoke');
	}

	public function group()
	{
		return $this->belongsToMany('Rosscturner\Larashop\Shop\Product\CompoundGroup');
	}

	public function postage()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Postage\Postage');
	}

	public function orderItems()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem');
	}

	public function status()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Status');
	}

	public function scopeIsTicket($query)
	{
		return $query->has('ticket');
	}

	public function scopePublished($query)
	{
		return $query->whereHas('status',function($q){
			$q->where('name','published');
		});
	}

	public function registerMediaCollections(): void
	{
		$this->addMediaCollection('product-landscape')->singleFile();
		$this->addMediaCollection('product-portrait')->singleFile();
		$this->addMediaCollection('product-images')->onlyKeepLatest(8); 
	}

}
