<?php

namespace Rosscturner\Larashop\Shop\Product;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Contracts\Buyable;


class Physical extends Model implements Buyable
{
	protected $table = 'physical';

	protected $fillable = ['description','price','stock','product_id'];

	public function product()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Product','product_id');
	}

	// cart interface
	public function getBuyablePrice($options = null)
	{
		return $this->product->price;
	}

	public function getBuyableIdentifier($options = null)
	{
		return $this->id;
	}

	public function getBuyableDescription($options = null)
	{
		return $this->product->name;
	}

	public function orderItems()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem');
	}
}
