<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name'];
}
