<?php

namespace Rosscturner\Larashop\Shop;


use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{

    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email','order_id','stripe_id'
    ];

    public function orders()
    {
        return $this->belongsTo('Rosscturner\Larashop\Shop\Order');
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return \Config::get('shop.order_confirmed_webhook');
    }

}
