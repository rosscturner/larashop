<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Orders extends Model
{
    protected $table = 'orders_view';

    protected $fillable = [];


    public function items()
    {
        return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem','order_id');
    }

    // public function products()
    // {
    // 	 return $this->items()->with('product')->get();
    // }

    public function billingAddress()
    {
       return $this->belongsTo('Rosscturner\Larashop\Shop\Address','billing_id');
    }

    public function shippingAddress()
    {
        return $this->belongsTo('Rosscturner\Larashop\Shop\Address','shipping_id');
    }

    public function scopeUnfulfilled($query)
    {
        return $query->where('status', '=', 'completed');
    }

    public function scopeShipped($query)
    {
        return $query->where('status', '=', 'fulfilled');
    }

}
