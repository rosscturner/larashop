<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{

    use Notifiable;

    protected $fillable = ['user_id','phone_number_id','billing_id','shipping_id','status','price'];

	public function user()
	{
		return $this->belongsTo('Rosscturner\Larashop\User');
	}

	public function orderUser()
	{
		return $this->hasOne('Rosscturner\Larashop\Shop\OrderUser');
	}

    public function items()
    {
        return $this->hasMany('Rosscturner\Larashop\Shop\OrderItem');
    }

    public function products()
    {
    	 return $this->items()->with('product')->get();
    }

      public function billingAddress()
    {
       return $this->belongsTo('Rosscturner\Larashop\Shop\Address','billing_id');
    }

    public function shippingAddress()
    {
        return $this->belongsTo('Rosscturner\Larashop\Shop\Address','shipping_id');
    }

    public function scopeUnfulfilled($query)
    {
        return $query->where('status', '=', 'completed');
    }

    public function scopeShipped($query)
    {
        return $query->where('status', '=', 'fulfilled');
    }

}
