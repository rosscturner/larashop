<?php

namespace Rosscturner\Larashop\Shop;


use Illuminate\Database\Eloquent\Model;

class UserNumber extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function user()
    {
       return $this->belongsTo('App\User');
    }

    public function number()
    {
        return $this->hasOne('App\Shop\PhoneNumber');
    }
}
