<?php

namespace Rosscturner\Larashop\Shop\Postage;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public $incrementing = false;
	protected $primaryKey = 'iso';

	public function zone()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Postage\Zone');
	}
}
