<?php

namespace Rosscturner\Larashop\Shop\Postage;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{

	public function postageRates()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Postage\Postage');
	}

	public  function countries()
	{
		return $this->hasMany('Rosscturner\Larashop\Shop\Postage\Country');
	}
}
