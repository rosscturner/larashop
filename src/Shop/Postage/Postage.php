<?php

namespace Rosscturner\Larashop\Shop\Postage;

use Illuminate\Database\Eloquent\Model;

class Postage extends Model
{
	protected $table = 'postage';


	public function product()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Product\Product');
	}

	public function zone()
	{
		return $this->belongsTo('Rosscturner\Larashop\Shop\Postage\Zone');
	}
}
