<?php

namespace Rosscturner\Larashop\Shop;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $fillable = ['customer_address_id','address_line_1','address_line_2','city','county','country','postcode'];

	public function customerAddress()
	{
		return $this->belongsTo('App\Shop\CustomerAddress');
	}

	public function OrderUser()
	{
		return $this->belongsTo('App\Shop\OrderUser');
	}
}
