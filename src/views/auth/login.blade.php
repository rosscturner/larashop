@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="log-in-form" role="form" method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="medium-6 medium-centered large-4 large-centered columns">
                           <div class="row column log-in-form">
                                <h4 class="text-center">Log in with you email account</h4>
                                <label>Email
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <input type="text" name="email" value="{{ old('email') }}" placeholder="somebody@example.com">
                                </label>
                                <label>Password
                                 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <input type="password" name="password" placeholder="Password">
                                </label>
                                <!-- <input id="show-password" type="checkbox"><label for="show-password">Show password</label> -->
                                <p><input type="submit" class="button expanded" value="Log In"></p>
                                <p class="text-center"><a href="{{ url('/password/reset') }}">Forgot your password?</a></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
