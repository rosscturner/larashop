
@section('facebook_meta') 
<meta property="og:url" content="{{ url('editorial/' . $post->slug) }}">
@if(!$post->teaser)
<meta property="og:image" content="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}">
@endif
<meta property="og:description" content="{{$post->excerpt}}"> 
@endsection 
@extends('layouts.app') 
@section('pageType', $post->title . ' by ' . $post->author->firstname . ' ' . $post->author->lastname . ' | '  .  ' Editorial |' )
@section('pageDesc',$post->excerpt)
@section('content')
<div class="editorial-wrapper">
    <article role="main" class="light article">
        <div class="grid-container">
            <header class="grid-x {{ ($post->teaser) ? 'align-center-middle':'align-justify'}}">
                @if(!$post->teaser)
                <div class="cell small-11 medium-5 align-center-middle flex-container flex-dir-column post-title ">
                    <h3 class="flex-child-shrink">{{$post->category->name}}</h3>
                    <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">
                        {{$post->title}}
                    </h2>
                    <p class="flex-child-shrink">
                        {{$post->excerpt}}
                    </p>
                </div>

                 @foreach($post->sections as $section)
                 <div class='cell small-11 post-content mobile-social'>
                    {!! $section->content !!}
                    <p class="social">
                        <a href="mailto:?subject={{$post->title}}&amp;body={{ url('/editorial/') }}/{{$post->slug}}"
                            title="Share by Email" >
                            <img src='/img/social/mail.svg' />
                        </a>
                        <a  onclick="window.open(this.href,'_blank',
    'width=500,height=500,toolbar=0'); return false;" 
                            href="http://twitter.com/intent/tweet?status={{$post->title}}+{{ url('/editorial/') }}/{{$post->slug}}">
                            <img src='/img/social/twitter.svg' />
                        </a>
                        <a onclick="window.open(this.href,'_blank',
    'width=500,height=500,toolbar=0'); return false;"  
                            href="http://www.facebook.com/sharer/sharer.php?u={{ url('/editorial/') }}/{{$post->slug}}&title={{$post->title}}">
                            <img src='/img/social/facebook.svg' />
                        </a>
                    </p>
                </div>
                @break
                 @endforeach
                <div class="cell small-11 medium-6 flex-container align-center-middle header-image">
                    <img  src="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}"/>
                </div>
                @else
                <div class="cell small-11 medium-6 align-center-middle flex-container flex-dir-column post-title ">
                    <h3 class="flex-child-shrink">{{$post->category->name}}</h3>
                    <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">
                        {{$post->title}}
                    </h2>
                    <p class="flex-child-shrink">
                        {{$post->excerpt}}
                    </p>
                </div>
                @foreach($post->sections as $section)
                 <div class='cell small-11 post-content mobile-social'>
                    {!! $section->content !!}
                    <p class="social">
                        <a href="mailto:?subject={{$post->title}}&amp;body={{ url('/editorial/') }}/{{$post->slug}}"
                            title="Share by Email" >
                            <img src='/img/social/mail.svg' />
                        </a>
                        <a  onclick="window.open(this.href,'_blank',
    'width=500,height=500,toolbar=0'); return false;" 
                            href="http://twitter.com/intent/tweet?status={{$post->title}}+{{ url('/editorial/') }}/{{$post->slug}}">
                            <img src='/img/social/twitter.svg' />
                        </a>
                        <a onclick="window.open(this.href,'_blank',
    'width=500,height=500,toolbar=0'); return false;"  
                            href="http://www.facebook.com/sharer/sharer.php?u={{ url('/editorial/') }}/{{$post->slug}}&title={{$post->title}}">
                            <img src='/img/social/facebook.svg' />
                        </a>
                    </p>
                </div>
                @break
                @endforeach
                @endif
            </header>
        </div>
        <section>
            <?php $count = 1 ?>
            @foreach($post->sections as $section)
                @if($section->type !== 'pull-out')
                @if($count % 2 != 0)
                <div class="grid-container">
                    <div class="grid-x row-container">
                @endif
                    <div class='cell small-11 medium-6 post-content section section-{{($count == 1) ? "1":$count}} {{ (( $count % 2 ) == 0) ? "right":"left" }} '>
                        {!! $section->content !!}
                        @if($count == 1)
                            <p class="social">
                                <a href="mailto:?subject={{$post->title}}&amp;body={{ url('/editorial/') }}/{{$post->slug}}"
                                    title="Share by Email" >
                                    <img src='/img/social/mail.svg' />
                                </a>
                                <a  onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;" 
                                    href="http://twitter.com/intent/tweet?status={{$post->title}}+{{ url('/editorial/') }}/{{$post->slug}}">
                                    <img src='/img/social/twitter.svg' />
                                </a>
                                <a onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;"  
                                    href="http://www.facebook.com/sharer/sharer.php?u={{ url('/editorial/') }}/{{$post->slug}}&title={{$post->title}}">
                                    <img src='/img/social/facebook.svg' />
                                </a>
                            </p>
                        @endif
                    </div>
                    @if($count % 2 == 0)
                    </div>
                </div>
                @endif
                <?php $count++ ?>
                @else
                <div class="section-{{ $section->order }} pull-out">
                    <div class="grid-container">
                        <div class="grid-x align-center text-center ">
                            <blockquote class='cell small-11 medium-8 quote'>
                                    {!! $section->content !!}
                            </blockquote>
                        </div>
                    </div>
                </div>
                @endif
            @endforeach
        </section>    
    </article>
</div>
@include('partials.readmore')
@endsection
@section('scripts')
<script>
   $(document).ready(function(){
        $('.post-content').has('img').addClass('with-border');
    });
</script>

@endsection
