@extends('layouts.app') @section('pageType', 'Podcast | ' )@section('content')

<!-- Quote -->
<div class="light"> 
    <div class="grid-container">
        <div class="row-section grid-x flex-column-dir align-center-middle  section text-center">
            <h3 class="flex-child-shrink">PODCAST</h3>
            <p class="cell grid-x align-center-middle text-center">
                <span class="cell small-11 medium-8">
            To celebrate the festive season and ring in a new year we are excited to be publishing the
            first episode of our new podcast! The rest of series one will be released fortnightly in the new
            year.
            <br/>
            <br/>
            Voices At The Table is a food podcast that brings together an eclectic mix of voices across
            the literary, performance and gastronomic worlds. An evolution of the popular Voices At
            The Table event series, it aims to look at food from a fresh perspective, exploring both the
            light and dark side of food culture through stories, poems, essays and plays.
            Hosted by writer, editor and At The Table founder Miranda York, and writer and poet Anna
            Sulan Masing, each episode features readings and performances from the live events with
            insights and anecdotes from Miranda and Anna.
            </span>  
            </p>       
        </div>
    </div>
</div>
@include('pages.partials.podcast-detail',['podcast' => $podcasts->pop()])

@include('partials.readmore')
@endsection('content')
@section('scripts')
<script>
//Podcast = TODO: move to external
    $('.podcast-container').on('click', function(){
        
        var $audio = $(this).find('audio')[0];
        var $playbutton = $(this).find('.play-button');
        var $pausebutton = $(this).find('.pause-button'); 
        $audio.play().then(function() {
            $playbutton.fadeOut(); 
            $pausebutton.fadeTo( "slow" , 1.0);
            $($audio).addClass('active');
        });

        $pausebutton.on('click', function(eve){
                eve.stopPropagation();
                $audio.pause();
                $pausebutton.fadeTo( "slow" , 0);
                $playbutton.fadeIn(); 
        });
    });
</script>
@endsection