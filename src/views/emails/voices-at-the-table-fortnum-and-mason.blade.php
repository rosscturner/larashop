@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table
</strong><br/>
                        
                        </span> 
                        <br/>
                        Join us for the 4th Voices At The Table – a curated evening of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds.
                        <br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums">
                        <img src="{{\Storage::url('email/fortnums.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Rachel Roddy</strong> – award-winning food writer, cook, Guardian columnist and author of two cookbooks, ‘Five Quarters’ and ‘Two Kitchens’, reading a piece comparing Northern and Roman food commissioned for At The Table magazine.
                    <br/>
                    <br/>
                    <strong>Marina O’Loughlin</strong> – journalist and restaurant critic known for her anonymous reviews and perfectly phrased commentary. To keep her identity secret, actor Katie Leung (best known for her role as Cho Chang in Harry Potter and recent star in the RSC’s Snow in Midsummer) will be reading one of her favourite restaurant reviews.
                    <br/>
                    <br/>
                    <strong> Annie Gray</strong>  – food historian, writer, broadcaster and presenter of BBC’s The Sweet Makers, reading from her book ‘The Greedy Queen: Eating with Victoria’.
                    <br/>
                    <br/>
                    <strong>Eli Goldstone </strong> – writer and author of debut novel ‘Strange Heart Beating’, published by Granta, reading a specially commissioned piece entitled ‘Something’s Flesh’. 
                    <br/>
                    <br/>
                    <strong>Stephen Harris</strong>  – chef-patron of The Sportsman in Seasalter, recently voted the best restaurant in the UK, reading from his new cookbook, published by Phaidon.
                    <br/>
                    <br/>
                    <strong>Fuchsia Dunlop</strong>  – cook and food writer specialising in Chinese cuisine, and the author of numerous award-winning food books, including ‘Every Grain of Rice’ and most recently ‘Land of Fish and Rice’, reading from her memoir ‘Shark's Fin and Sichuan Pepper’. 
                    <br/>
                    <br/>
                    <strong>Laura Yasmin Elliott</strong>  – poet, librarian and co-founder of para-text, a poetry publishing experiment, reading from her forthcoming volume of poetry 'Lemon, Egg, Bread', published by Test Centre in November. 
                    <br/>
                    <br/>
                    <strong>Mina Holland</strong>  – editor of the Guardian's Saturday Cook supplement and the author of two books, ‘The Edible Atlas’ and ‘Mamma: Reflections on the Food That Makes Us’, reading a piece about doughnuts by Nora Ephron. 
                    <br/>
                    <br/>
                    <strong>Andrew Wong </strong> – chef-patron of A.Wong restaurant in Pimlico, named 3rd in this year’s UK Restaurant Awards, reading an excerpt from one of his favourite books about food. 
                </p>
            </td>
        </tr>


            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <hr/>
                    <p>
                    Tickets include champagne on arrival and fabulous food and drink from Fortnum & Mason throughout the evening. Books by the speakers will also be available on the night from Review Books. 
                        <br/>
                        <br/>
                        <span style="display:block;text-align:center;">
                        Thursday 5th October 2017<br/>
                        The Drawing Room, Fortnum & Mason, 181 Piccadilly, London, W1A 1ER <br/>
                      6.30pm-9pm</br/>
                        </span>
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums"><strong>BOOK TICKETS HERE</strong></a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2017 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-fortnum-and-mason?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums">At The Table Ltd</a>
    </td>
</tr>
@endsection
