@extends('emails.layouts.wrapper')
@section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>

                <td>
                    <hr/>
                        <a href="https://atthetable.co.uk/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">
                        <img src="{{\Storage::url('pages/home.jpg')}}" alt="At The Table"/>
                        </a>
                    <hr/>
                    </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p>
                        There have been some big changes happening behind the scenes at TOAST… and we’re incredibly excited to announce that we’ve now relaunched as At The Table.
                        <br/>
                        <br/>
                        We hope you’ve enjoyed following us on our journey so far, exploring British food culture and celebrating the people behind it. Though we have a shiny new look, you can expect our usual mix of talks, debates and dinners, plus our annual food magazine showcasing the best British food writing, photography and illustration. A few surprises, collaborations and new projects too…
                        <br/>
                        <br/>
                        Food can be a banal, everyday necessity, but it can also inspire and bring people together: to share conversations, to share ideas, to share knowledge, and feel connected. By bringing people together from different industries and backgrounds – chefs, producers, writers, historians, scientists, artists, filmmakers, entrepreneurs – we aim to look at food from a fresh perspective.
                        <br/>
                        <br/>
                        Join us at the table.

                    </p>
                </td>
            </tr>

        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                     <hr/>
                      <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">
                    <img src="{{\Storage::url('events/4/hero.jpg')}}" alt="At The Table"/>
                    </a>
                     <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="margin:10px 0 0 0;">
                        <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">SUMMER CAMP
                        </a>
                    </h2>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p>Tickets are now on sale for our latest event series – Summer Camp – a programme of culinary and creative workshops at Town Hall Hotel hosted by our favourite chefs and makers. From ceramics to calligraphy, pickling to pasta making, gather at the table and learn a new skill that will last long after summer fades.
                    <br />
                    <br/>
                    <strong>First up</strong> – Tacos, Pickles & Tepache-iladas! with F.A.T founder Freddie Janssen
                     </p>
                </td>


            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">BOOK NOW</a>

                </td>
            </tr>


        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <hr/>
                     <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">
                        <img src="{{\Storage::url('products/2/hero.jpg')}}" alt="At The Table"/>
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="margin:10px 0 0 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">
                         AT THE TABLE PRODUCTS
                         </a>
                     </h2>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p>We’ve been working with a talented group of independent makers to create a collection of the finest goods for your table. Focusing on timeless objects that are both beautiful and functional, every piece is handcrafted by makers with a passion for what they do. We believe in products that are sustainable, thoughtful and made with care, each with their own unique story. 
                    <br/>
                    <br/>
                    Each item is a bespoke collaboration exclusively available through our website and pop ups. Our first pop up shop launches today at Town Hall Hotel in Bethnal Green. Drop in or browse online…
                     </p>
                </td>


            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">SHOP PRODUCTS</a>
                </td>
            </tr>
        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a href="https://atthetable.co.uk/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">atthetable.co.uk</a>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a href="https://twitter.com/__atthetable__"><img style="width:35px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
         <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:35px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
    </td>

</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
    <hr/>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/at-the-table/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
       ©2016 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=toastischanging&utm_medium=email&utm_campaign=Toast%20is%20changing">At The Table Ltd</a>
    </td>
</tr>
@endsection
