@extends('larashop::emails.layouts.email-wrapper') @section('content')

<div class="content">
    <span class="preheader">Thank you for your order</span>

    <table class="main">
        <!-- Branding -->
        <tr>
            <td class="verv-logo-container">
                <img class="verv-logo" alt="Verv" src="{{url('/img/email/logo.png')}}" />
            </td>
        </tr>
        
        <!-- Tagline -->
        <tr>
            <td class="verv-tagline">
                <p class="verv-tagline-content">Order Number: <a href="{{url('/invoice')}}/{{$order->id}}">{{$order->id}}</a> | Date Ordered: {{date('d M Y',strtotime($order->created_at))}}</p>
            </td>
        </tr>

        <!-- Welcome Banner -->
        <tr>
            <td class="verv-header-image">
                <img class="verv-banner" alt="Verv" src="{{url('/img/email/verv-product-banner.png')}}" />
            </td>
        </tr>

        <!-- Primary Content -->
        <tr>
            <td class="primary-content">
                <h1 class="verv-content-header">Hi {{$orderUser->firstname}},</h1>

                    <p class="email-description">
                        Thank you for your order. Your Verv Hub will be delivered to you in Winter 2017. As soon as it’s ready to ship we’ll send you an email confirming the delivery date and your tracking information.
                    </p>

                    <p class="email-description">
                        In the meantime, please check out the <a href="{{url('/news')}}">news</a> section on our website to learn more about your clever, new home energy assistant and follow us on social media to keep up-to-speed with our exciting developments!
                    </p>

                    <p class="email-social-links">
                        <a href="https://www.facebook.com/verv.hub" class="email-social-link" alt="Like us on Facebook">
                            <img src="{{url('/img/email/social-facebook.png')}}" class="email-social-image" alt="Facebook">
                        </a>
                        <a href="{{url('https://twitter.com/verv_energy')}}" class="email-social-link" alt="Follow us on Twitter">
                            <img src="{{url('/img/email/social-twitter.png')}}" class="email-social-image" alt="Twitter">
                        </a>
                    </p>

                    <p class="email-description">
                        <span class="normalise-weight">All the best</span><br />
                        Team Verv
                    </p>
            </td>
        </tr>
        
        <!-- Blank seperator -->
        <tr> <td class="verv-seperator">&nbsp;</td> </tr>

        <!-- Order Information -->
        <tr>
            <td class="primary-content">
                <h1 class="verv-content-header-shipping">Items to be shipped</h1>

                    <table>
                        <tr class="verv-order-info-header">
                            <td class="verv-order-info-header-text" colspan="2">Shipment 1</td>
                            <td class="verv-order-info-header-text right-align">Quantity</td>
                            <td class="verv-order-info-header-text right-align">Price</td>
                        </tr>
                        @foreach($order->items as $item)
                        <tr class="verv-order-info-body">
                            <td><img src="{{url('/img/email/verv-box.jpg')}}" class="verv-order-product-image" alt="Verv Hub"></td>
                            <td class="verv-order-info-body-text">{{$item->product->name}}</td>
                            <td class="verv-order-info-body-text center-align">{{$item->qty}}</td>
                            <td class="verv-order-info-body-text right-align">&pound;{{$order->price}}</td>
                        </tr>
                        @endforeach
                    </table>

                    <p class="verv-order-shipping-info">
                        <!-- <span class="normalise-weight">Delivery</span>: {date} by standard shipping. <a href="" >More details about delivery dates</a> -->
                    </p>

                    <table>
                        <tr class="verv-shipping-info">
                            <td width="140px" class="normalise-weight">Delivering to:</td>
                            <td>
                                {{$orderUser->firstname}} {{$orderUser->lastname}}<br/>
                                {{$shippingAddress->address_line_1}}<br/>
                                {{$shippingAddress->address_line_2}}<br/>
                                {{$shippingAddress->city}}<br/>
                                {{$shippingAddress->postcode}}<br/>
                                {{strtoupper($shippingAddress->country)}}
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
                                    
        <!-- Blank seperator -->
        <tr> <td class="verv-seperator">&nbsp;</td> </tr>

        <!-- Secondary Content -->
        <tr>
            <td class="secondary-content">
                <h1 class="verv-secondary-header">Got a question? We’re here to help!</h1>

                <span class="button-bordered"><a href="mailto:info@verv.energy">ASK A QUESTION ></a></span>
            </td>
        </tr>

        <!-- Blank seperator -->
        <tr> <td class="verv-seperator">&nbsp;</td> </tr>

        <!-- Footer Glyph Header -->
        <tr class="cancel-margins">
            <td class="cancel-margins">
                <img src="{{url('/img/email/glyph.png')}}" class="seperator-bar-glyph" alt="">
            </td>
        </tr>

        <!-- Footer Content -->
        <tr class="cancel-margins">
            <td class="footer-content">
                <p class="footer-address">Green Running LTD, St. Magnus House, 3 Lower Thames Street, London, EC3R 6HD</p>
                <p class="footer-tagline">Verv - The Advanced Home Energy Monitor</p>

                <p class="footer-manage-preferences"><a href="{{url('/invoice')}}/{{$order->id}}">View in browser</a></p>
            </td>
        </tr>
    </table>
</div>
@endsection