
@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/slate.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>The Geometry of Pie
</strong><br/>
                        
                        </span> 
                        <br/>
                        To mark the launch of ATT Films, we’re hosting a special dinner at Holborn Dining Room, celebrating the premiere of ‘The Geometry of Pie’, a film exploring one chef’s obsession with a British classic.

                        <br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">
                        <img src="{{\Storage::url('email/pies.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                ‘The Geometry of Pie’ follows Executive Head Chef and renowned pastry expert Calum Franklin around London, capturing glimpses of churches, museums and architectural details – from Whiteleys to the British Museum – that influence his pie designs, as well as showing the process behind the pies served to diners each day at Holborn Dining Room. Fascinated by his attention to detail, the film delves into the intricacies of perfect pastry and reveals the inspiration behind his spectacular creations. From art to architecture, MC Escher exhibitions to antique moulds, Calum draws on both historical references and everyday details to design and build pies that look as good as they taste. 
                <br/>
                <br/>
                </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">
                        <img src="{{\Storage::url('email/glenlivet-glade.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                Hosted within The Glenlivet Glade at Holborn Dining Room, a cosy winter terrace under a starry “night sky”, the film screening will be followed by a special pie menu created by Calum and his team exclusively for the premiere, paired with drinks by The Glenlivet. 
                <br/>
                <br/>
                </p>
                </td>
            </tr>
           


            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <hr/>
                        <br/>
                        <span style="display:block;text-align:center;">
                       
                            ‘The Geometry of Pie’ Premiere at Holborn Dining Room<br/>
                            Monday 25th February<br/>
                            Rosewood London, 252 High Holborn, London WC1V 7EN
                            <br/>
                        </span>
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie"><strong>BOOK TICKETS HERE</strong></a>
                    <br/>
                    <br/>
                </td>
            </tr>

            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">
                        <img width="150" src="{{\Storage::url('email/glenlivet.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>

            
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/geometry-of-pie-event?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2019 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/geometry-of-pie?utm_source=newsletter&utm_medium=email&utm_campaign=geometry-of-pie">At The Table Ltd</a>
    </td>
</tr>
@endsection
