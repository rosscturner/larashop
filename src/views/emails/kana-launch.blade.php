@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/products/#kana?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/kana-header.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Kana London x At The Table
</strong><br/>
                        
                        </span> 
                        <br/>
                        We’ve long admired the work of Ana Kerin, the creative force behind Kana London ceramics, so we’re thrilled to be launching a limited-edition collection exclusive to At The Table this season. 
    <br/>
    <br/>
    With a background in fine art and sculpture, Ana Kerin has applied the principles of her work – the tactile, hand-built aesthetic, the traces of her fingerprints and hands on the surface – to functional ceramics. Each piece is made in her East London studio, hand built rather than turned on the wheel so as to add a spontaneous individuality to each piece. 


                        <br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/products/#kana?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch">
                        <img src="{{\Storage::url('email/kana-collection.jpg')}}" alt="Kana Collection" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                The collection includes a deep dinner plate, side plate and bowl – conceived as a set but also perfect to mix and match with other ceramics and tableware. Each piece is glazed in Green Moss – a dark green and black glaze constructed of pigments that blend to reflect the deep greens of the British countryside at dusk. 
                <br/>
                <br/>
                Stoneware and durable enough for everyday use, the collection will last for years to come. Small imperfections such as fingerprints and indentations are part of the nature of Ana’s technique, adding to the beauty and integrity of the ceramics. 


                </p>
            </td>
        </tr>

         <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/products/#kana?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch">
                        <img src="{{\Storage::url('email/kana-video.jpg')}}" alt="Kana Video" />
                    </a>
                </td>
            </tr>
        <tr>
        <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                To celebrate the collaboration we’ve made a short film about Ana and her work, shot and directed by our Digital Director,<a style="text-decoration:none;" href="https://instagram.com/rosscturner"> Ross Turner</a>. 
                <br/>
                <br/>
                To watch the film and discover more about the collection, <a style="text-decoration:none;" href="https://atthetable.co.uk/products">click here</a>
                <br/>

                </p>
            </td>
        </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/products/#kana?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/kana-launch?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/products/#kana?utm_source=newsletter&utm_medium=email&utm_campaign=kana-launch">At The Table Ltd</a>
    </td>
</tr>
@endsection
