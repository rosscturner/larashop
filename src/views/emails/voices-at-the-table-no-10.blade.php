@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table No.10
</strong><br/>
                        
                        </span> 
                        <br/>
                        Voices At The Table is a series of curated evenings of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds.
    <br/>
    <br/>
For our final event of 2018 - and our 10th in the series! - we’re returning to the fabulous Fortnum & Mason with an incredible line up of talented food writers, chefs, novelists, broadcasters, poets, performers and ice cream makers...

                        <br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2">
                        <img src="{{\Storage::url('email/fortnums.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Matthew Fort</strong> – food writer, broadcaster and author of several award-winning books including ‘Sweet Honey, Bitter Lemons’. Matthew was the Guardian Food and Drink editor for 15 years, and now is perhaps best known as an expert judge on popular TV series The Great British Menu.
                    <br/>
                    <br/>
                    <strong>Jackson Boxer</strong> – chef and restaurateur of the acclaimed Brunswick House and newly opened St Leonards. Jackson comes from a family of writers, artists and culinary expertise and has written for the Guardian and The Financial Times.

                    <br/>
                    <br/>
                    <strong>Elisabeth Luard</strong>  – award winning food writer, artist and broadcaster. Elizabeth has written 24 books, including two novels and four food memoirs with recipes - ‘Squirrell Pie and Other Stories’ being the most recent - and is the incoming Chair of the Oxford Symposium on Food & Cookery.
                    <br/>
                    <br/>
                    <strong>Kitty Travers </strong> – ice cream maker and founder of La Grotta Ices. Kitty trained at the Institute of Culinary Education in New York and returned to London to become pastry chef at St John Bread & Wine before setting up La Grotta. Her debut cookbook, ‘La Grotta Ices’, was shortlisted for the Jane Grigson Trust Award.
                    <br/>
                    <br/>
                    <strong>Rhik Samadder</strong>  – writer, actor and broadcaster. Rhik writes the kitchen tech column ‘Inspect A Gadget’ for The Guardian as well as features for various publications. As an actor, Rhik has worked for the Royal Shakespeare Company, the BBC, ITV and HBO.
                    <br/>
                    <br/>
                    <strong>Rowan Hisayo Buchanan</strong>  – Japanese-British-Chinese-American writer and author of ‘Harmless Like You’. Rowan’s short writing has appeared in the Guardian, Granta, Guernica and The Harvard Review, and her debut novel was a New York Times Editors’ Choice, and winner of The Authors’ Club First Novel Award.
                    <br/>
                    <br/>
                    <strong>Nikita Gill</strong>  –  Indian poet based in the UK. Nikita has recently published her fourth poetry collection, ‘Fierce Fairytales’, and is currently working on short stories and her first novel. She is one of the key voices in the “Instapoetry” movement, using the visual medium to tell stories.
                    <br/>
                    <br/>
                    <strong>Alice Lascelles</strong>  –  journalist, author, presenter and drinks expert. Alice writes about drinks for the Financial Times, co-founded the award-winning drinks magazine Imbibe, and in 2015 she published her first book, ‘Ten Cocktails: The Art of Convivial Drinking’. A second life as a musician has seen her tour with the White Stripes and record sessions for national radio.
                    <br/>
                    <br/>
                    <strong>Abubakar Salim </strong> – screen and theatre actor. Abubakar is currently playing Pedro in Sky’s ‘Jamestown’ series and has appeared on stage at the National Theatre and Donmar Warehouse.
                    <br/>
                    <br/>
                    <strong>Tim Hayward </strong> – food writer, author, broadcaster and proprietor of Fitzbillies bakery and restaurant in Cambridge. Tim writes a column for the Financial Times and has written four books including ‘Food DIY’ and ‘The Modern Kitchen’. He is also a regular presenter on BBC Radio 4’s The Food Programme and The Kitchen Cabinet.

                </p>
            </td>
        </tr>

         <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2">
                        <img src="{{\Storage::url('email/_miranda-anna.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
        <tr>
        <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                Tickets include the performances, a glass of champagne on arrival and fabulous food and drink from Fortnum & Mason throughout the evening.
                    <br/>
                    <br/>
                    <span style="display:block;text-align:center;">
                    Wednesday 7th November 2018<br/>
                    Fortnum & Mason, 181 Piccadilly, London<br/>
                    6.30pm-9pm<br/>
                    </span>
                </p>
            </td>
        </tr>
        
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2"><strong>BOOK TICKETS HERE</strong></a>
                <br/>
                <br/>
            </td>
        </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table-no-10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no10?utm_source=newsletter&utm_medium=email&utm_campaign=voices-fortnums-2">At The Table Ltd</a>
    </td>
</tr>
@endsection
