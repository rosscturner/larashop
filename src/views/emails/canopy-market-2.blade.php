@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                        <img style="width:auto;margin-top:20px;" src="{{\Storage::url('email/canopy-market-logo.png')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="font-size: 18px;display:block;text-align:center;"><strong>At The Table Presents... <br/>Makers Market Workshops at Canopy Market
</strong><br/>
                        
                        </span>
                        <br/>
                        <span style="display:block;text-align:center;">
                        Join us next weekend for our second series of creative and culinary workshops at Canopy Market, King’s Cross. Hosted by our favourite chefs, creatives and makers, we’re getting into the festive spirit with everything from wreath making and Christmas cooking demos to winter tonics to keep you healthy and boozy workshops to make you merry.
                        </span>
                        <br/>
                        <span style="display:block;text-align:center;">
                        <strong>Free</strong> to attend, simply drop us a line to secure your place.
                        </span>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                            Friday December 15th
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;">
                        ~ 6pm - Festive table setting &amp; floral design with Worm London ~
                        </span>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                        <img src="{{\Storage::url('email/cheese.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                            Saturday December 16th
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;">
                        ~ 12pm - Winter tonics with Jamu Kitchen ~
                        </span>
                        <span style="display:block;text-align:center;">
                        ~ 1.30pm - Christmas wreaths with Rebel Rebel ~
                        </span>
                         <span style="display:block;text-align:center;">
                         ~ 3pm - The perfect cheeseboard with The School of Artisan Food ~
                        </span>
                        <span style="display:block;text-align:center;">
                        ~ 4.30pm - Christmas wines with Above Sea Level ~
                        </span>
                    </p>
                </td>
            </tr>


            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                        <img src="{{\Storage::url('email/rebel-rebel.jpeg')}}" alt="rebel-rebel" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
                         Sunday December 17th
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                    <span style="display:block;text-align:center;">
                    ~ 12pm - Festive flavours &amp; Christmas leftovers with Elly Pear ~
                    </span>
                    <span style="display:block;text-align:center;">
                    ~ 1.30pm - The art of recycling imagery with Made in Arts London ~
                    </span>
                     <span style="display:block;text-align:center;">
                     ~ 3pm - Block printing &amp; textile design with Ren London ~
                    </span>
                    <span style="display:block;text-align:center;">
                    ~ 4.30pm - The art of the aperitif with Kamm &amp; Sons ~
                    </span>
                    </p>
                </td>
            </tr>

            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                <br/>
                    <span style="display:block;text-align:center;">
                    <strong>Canopy Market</strong><br/>
                    <strong>December 15th-17th</strong><br/>
                    West Handyside Canopy, off Granary Square<br/>
                    Kings Cross, N1C</br/>
                    <a style="color:black;" href="https://goo.gl/maps/EGT4rjVLVxu">Go to map</a>
                    </span>
                </p>
            </td>
        </tr>
        
        
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=makers-market">All workshops and demos are free to attend. Head to atthetable.co.uk for more information and email us to reserve your place at </a><a href="mailto:info@atthetable.co.uk"> info@atthetable.co.uk</a>
                <br/>
                <br/>
            </td>
        </tr>
        </table>
    </td>
</tr>

<tr>
    <td>
        <hr/>
        <a href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">
            <img src="{{\Storage::url('email/canopymarket.jpg')}}" alt="Canopy Market" />
        </a>
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
            <a style="text-decoration:none;color:black;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">For more information on individual workshops take <br/> a look at the <span style="text-decoration:underline;">At The Table website</a> </a>
            <br/>
            <br/>
            <a style="text-decoration:none;color:black;" href="https://www.kingscross.co.uk/canopy-market">For more information on Canopy Market <br/> take a look  at the  <span style="text-decoration:underline;">King’s Cross website</span> </a>

                
            </td>
        </tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2017 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/makers-market?utm_source=newsletter&utm_medium=email&utm_campaign=canopymarket">At The Table Ltd</a>
    </td>
</tr>
@endsection
