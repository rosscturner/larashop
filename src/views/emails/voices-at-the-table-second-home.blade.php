@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table No.6</strong><br/></span> 
                        <br/>
                        Voices At The Table is a series of curated evenings of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds.
                        <br/>
                        <br/>
                        For our 6th event of the series, we’re heading to Second Home Spitalfields for our biggest event to date. Join us for an evening of food and entertainment, drawing together writers, actors, editors, poets, chefs, novelists and broadcasters to explore the joys and complexities of the culinary world. 
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home">
                        <img src="{{\Storage::url('email/anna-jones.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Anna Jones</strong> – cook, writer and stylist, Guardian columnist and author of the bestselling ‘A Modern Way to Eat’, ‘A Modern Way to Cook’ and ‘The Modern Cook’s Year’. 
                    <br/>
                    <br/>
                    <strong>Pete Brown</strong> – award-winning writer, broadcaster and beer lover, author of nine books including ‘Man Walks into a Pub’ and ‘Three Sheets to the Wind’.
                    <br/>
                    <br/>
                    <strong>Lisa Markwell</strong>  – The Sunday Times Food Editor, CODE Quarterly Editor and private chef.

                    <br/>
                    <br/>
                    <strong>Darren McHugh </strong> – General Manager of The Ledbury.

                    <br/>
                    <br/>
                    <strong>Selina Nwulu </strong>  – Yorkshire-born writer, poet, essayist and Young Poet Laureate for London 2015-6.
                    <br/>
                    <br/>
                    <strong>Laura Freeman</strong>  – journalist and author of ‘The Reading Cure: How Books Restored My Appetite’. 

                    <br/>
                    <br/>
                    <strong>Will Harris</strong>  – Anglo-Indonesian writer and Assistant Editor at The Rialto.

                    <br/>
                    <br/>
                    <strong>Emma Glass</strong>  – author of debut novel 'Peach'.
                    <br/>
                    <br/>
                    <strong>Dan Saladino</strong> – producer and presenter of BBC Radio 4’s The Food Programme. 
                    <br/>
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home">
                        <img src="{{\Storage::url('email/dona-rita.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
        <tr>
        <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                Plus, to tie in with the entrepreneurial spirit of Second Home, we’re collaborating with some of our favourite up-and-coming chefs and start-ups. Feast on Brazilian pão de queijo from Dona Rita, savoury bites from pastry chef Henrietta Inman and incredible ice cream sandwiches from Happy Endings.
                </p>
            </td>
        </tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                    <span style="display:block;text-align:center;">
                        <br/>
                    Wednesday 25th April 2018<br/>
                    Second Home Spitalfields, 68 Hanbury Street, London, E1 5JL<br/>
                    Doors open 6.30pm
                    </br/>
                    </span>
                </p>
            </td>
        </tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home"><strong>BOOK TICKETS HERE</strong></a>
                <br/>
                <br/>
            </td>
        </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-second-home?utm_source=newsletter&utm_medium=email&utm_campaign=second-home">At The Table Ltd</a>
    </td>
</tr>
@endsection
