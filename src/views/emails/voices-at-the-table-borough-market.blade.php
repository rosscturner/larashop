@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-borough-market?utm_source=newsletter&utm_medium=email&utm_campaign=voices-borough">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/Chalkboard.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table for Borough Market
</strong><br/>
                        
                        </span> 
                        <br/>
                        Join us on August 1st for a very special edition of Voices At The Table at Borough Market. We’ll be bringing together writers, chefs, poets and performers for an evening of readings celebrating market life and reflecting on the unique communities it creates. All of the funds raised will go to the Borough Market Trader Support Fund, to help those affected by the recent attack at London Bridge.
                        <br/>
                    </p>
                </td>
            </tr>
             
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                      On the night we’ll be bringing you performances and readings from writer and anthropologist Claudia Roden, restaurant critic extraordinaire Grace Dent, Ukrainian chef and food writer Olia Hercules, rapper and poet Suley Muhidin, actor Elizabeth Chan, food writer Ed Smith, actor and storyteller Gabby Wong, architectural foodsmith Sam Bompas of Bompas & Parr, as well as a special piece written by a Borough Market trader.
                      <br>
                      <br/>
                      Plus delicious food and cocktails from Our/London!
                    </p>
                </td>
            </tr>
           


            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <hr/>
                    <p>
                    <br/>
                        <span style="display:block;text-align:center;">
                      Tuesday 1st August 2017<br/>
                      Borough Market, 1 Cathedral St, London, SE1 9DE<br/>
                      6.30pm-9pm</br/>
                        </span>
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-borough-market?utm_source=newsletter&utm_medium=email&utm_campaign=voices-borough"><strong>BOOK TICKETS HERE</strong></a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=newsletter&utm_medium=email&utm_campaign=voices-borough">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table-borough-market?utm_source=newsletter&utm_medium=email&utm_campaign=voices-borough" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2017 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=newsletter&utm_medium=email&utm_campaign=voices-borough">At The Table Ltd</a>
    </td>
</tr>
@endsection
