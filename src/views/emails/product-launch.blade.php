@extends('emails.layouts.email-wrapper')
@section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>

                <td>
                    <hr/>
                        <a href="https://atthetable.co.uk/?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">
                        <img src="{{\Storage::url('email/table.jpg')}}" alt="At The Table"/>
                        </a>
                    <hr/>
                    </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">
                         SUPPERCLUB AT KIT AND ACE
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p>
                        Last week we officially launched our first tableware collection with a very special supperclub at Kit and Ace on Redchurch St. James Lowe and the Lyle’s team cooked up a feast of seasonal flavours – think Peach, Lardo &amp; Wild Marjoram, Sea Trout with Samphire &amp; Cider Butter, and Loganberry Fool – while guests discovered our new range in the perfect dinner party setting.
                        <br/>
                        <br/>
                        For more photos of the night, take a look at our <a href="https://facebook.com/joinusatthetable/">Facebook page</a>
                    </p>
                </td>
            </tr>

        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                     <hr/>
                      <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">
                      <img src="{{\Storage::url('products/19/hero.jpg')}}" alt="At The Table"/>

                    </a>
                     <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                        <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">AT THE TABLE PRODUCTS
                        </a>
                    </h2>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p style="margin:10px 0;">We’ve been working with a talented group of independent makers to create a collection of the finest goods for your table. Focusing on timeless objects that are both beautiful and functional, every piece is handcrafted by makers with a passion for what they do. We believe in products that are sustainable, thoughtful and made with care, each with their own unique story.
                    <br/>
                    <br/>
                    Each item is a bespoke collaboration exclusively available through our website and pop ups.
                    </p>
                </td>


            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">SHOP PRODUCTS</a>
                </td>
            </tr>


        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <hr/>
                     <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events?utm_source=productlaunch&utm_medium=email&utm_campaign=Product%20Launch">
                        <img src="{{\Storage::url('events/16/email.jpg')}}" alt="At The Table"/>
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events/?utm_source=product-launch&utm_medium=email&utm_campaign=product%20launch">
                         SUMMER CAMP
                         </a>
                     </h2>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p style="margin:10px 0;">Want to learn something new this summer? There are just a few tickets left for our Summer Camp series at Town Hall Hotel. Create a bouquet of seasonal British flowers with Grace &amp; Thorn, learn the art of fabric printing with Ren London, or design your own signature fragrance with Experimental Perfume Club.

                     </p>
                </td>


            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/events/?utm_source=product-launch&utm_medium=email&utm_campaign=product%20launch">BOOK TICKETS</a>
                </td>
            </tr>
        </table>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a href="https://atthetable.co.uk/?utm_source=product-launch&utm_medium=email&utm_campaign=product%20launch">atthetable.co.uk</a>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a href="https://twitter.com/__atthetable__"><img style="width:35px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
         <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:35px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
    </td>

</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
    <hr/>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/product-launch?utm_source=product-launch&utm_medium=email&utm_campaign=product%20launch" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
       ©2016 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=product-launch&utm_medium=email&utm_campaign=product%20launch">At The Table Ltd</a>
    </td>
</tr>
@endsection
