@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                        <img style="height:55%;width:auto;margin-top:20px;" src="{{\Storage::url('email/ham-yard-map.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Late night shopping &amp; jazz tonight!
</strong><br/>
                        
                        </span> 
                        <br/>
                        Our Christmas pop up is now open! We’re at Ham Yard Village in Soho until December 6th so drop by for food conversations, magazines and free tea from Joe’s Tea Co. Plus tonight there’s late-night shopping and jazz in the courtyard, festive treats and goodie bags. See you later!
                        <br/>
                        <br/>
                        <span style="display:block;text-align:center;">
                        Late-night shopping 7pm-9pm<br/>
                        Ham Yard Village, Soho, W1D 7DT
                        </span>
                    </p>
                </td>
            </tr>
             <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                        <img src="{{\Storage::url('email/joe-tea.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
             <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/foodsalon?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                         Join us for breakfast
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        Starting to feel festive? Ready to brave the shops this weekend? Join us for breakfast this Saturday before you start your Christmas shopping and fuel up for the day. We’ll have delicious pastries from Gail’s Bakery, porridge from Rude Health, fresh fruit and delicious tea from Joe’s Tea Co. 

                        <br/>
                        <br/>
                         <span style="display:block;text-align:center;">
                        Drop in any time from 10am on Saturday 3rd December
                        </span>
                    </p>
                </td>
            </tr>


            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                        <img src="{{\Storage::url('email/shop-front.jpg')}}" alt="Joe's Tea" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/foodsalon?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                         Spoken Word
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        Join us on Monday 5th December for a live poetry and prose reading at The Food Salon. Poet Anna Sulan Masing – who’s poem adorns the walls of the pop up – will lead a host of writers and poets in an evening of spoken word. 
                        <br/>
                        <br/>
                         <span style="display:block;text-align:center;">
                        Monday 5th November<br/>
                        6.30pm-7.30pm<br/>
                        3 Denman Place, Soho, W1D 7DT<br/><br/>
                        </span>
                         <span style="display:block;text-align:center;">RSVP<a style="text-decoration:none;color:black;" href="mailto:info@atthetable.co.uk"> HERE</a></span>
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                        <img src="{{\Storage::url('email/mag-stack.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">
                         Shop Online
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        Can’t make it to our pop up? Starting to think/panic about Christmas gifts? Order At The Table magazine online before December 20th and we’ll deliver in time for Christmas. Presents sorted.
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">ORDER YOUR COPY HERE</a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/foodsalon?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2016 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=foodsalon&utm_medium=email&utm_campaign=foodsalon">At The Table Ltd</a>
    </td>
</tr>
@endsection
