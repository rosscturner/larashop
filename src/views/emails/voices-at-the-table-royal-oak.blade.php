@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table No.8</strong><br/></span> 
                        <br/>
                        Voices At The Table is a series of curated evenings of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds.
                        <br/>
                        <br/>
                        
                        For our summer event we’re heading to Marylebone, popping up at Dan Doherty’s new pub The Royal Oak for an evening of Tanqueray cocktails, classic pub snacks and, of course, an incredible line up of talented food writers, chefs, novelists, broadcasters, poets and performers.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak">
                        <img src="{{\Storage::url('email/royal-oak.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Dan Doherty</strong> – chef, food writer and owner of The Royal Oak. Dan has also written two cookbooks, co-founded the TMRW Project, and is one of the judges on the BBC’s ‘Britain's Best Home Cook’ with Mary Berry.


                    <br/>
                    <br/>
                    <strong>Honey &amp; Co</strong> –  husband and wife team Sarit Packer and Itamar Srulovich have taken Middle Eastern food in the UK by storm, opening two buzzing restaurants and a spice deli in London’s Fitzrovia. They have also written three bestselling cookbooks, including their latest book ‘Honey & Co At Home’.

                    <br/>
                    <br/>
                    <strong>Sharlene Teo</strong> – author of debut novel ‘Ponti’, winner of the inaugural Deborah Rogers Writer’s Award and the David T.K Wong Creative Writing Award, shortlisted for the 2017 Berlin Writing Prize and the 2018 Hearst Big Book Award.


                    <br/>
                    <br/>
                    <strong>Jonathan Nunn </strong> – food writer from London who writes about the intersection of food, immigration and race in the city. In his spare time he manages Postcard Teas, a tea shop in Mayfair working with small farmers around East Asia.


                    <br/>
                    <br/>
                    <strong>Mallika Basu</strong> – Indian cook, food writer, Evening Standard columnist and the author of two books, including her latest cookbook ‘Masala: Indian Cooking for Modern Living’. 

                     <br/>
                    <br/>
                    <strong>Russell Norman</strong> – London restaurateur, writer and broadcaster, author of three books including ‘POLPO’ (winner of the Waterstones Book of the Year 2012) and his latest cookbook ‘Venice: Four Seasons of Home Cooking’.
                    
                    <br/>
                    <br/>
                    <strong>Charlotte Mendelson</strong> – award-winning writer, editor and novelist, gardening correspondent for the New Yorker, author of four novels published by Picador/Mantle and one non-fiction book about her gardening obsession, ‘Rhapsody in Green’.

                       <br/>
                    <br/>
                    <strong>Paul Cree</strong> – poet, rapper, performer, and one half of theatre company Beats & Elements. His debut collection of poems and stories entitled ‘The Suburban’ has just been released by punk publishing company Burning Eye.
                    <br/>
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak">
                        <img src="{{\Storage::url('email/_miranda-anna.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
        <tr>
       
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                    <span style="display:block;text-align:center;">
                        <br/>
                        22nd August 2018<br/>
                        The Royal Oak, 74-76 York St, London W1H 1QN<br/>
                        Doors open 6.30pm, performances 7pm
                    </br/>
                    </span>
                </p>
            </td>
        </tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak"><strong>BOOK TICKETS HERE</strong></a>
                <br/>
                <br/>
            </td>
        </tr>
                
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <img width="200" src="{{\Storage::url('email/tanqueray.png')}}" alt="At The Table" />
                <br/>
        <br/>
            </td>
        </tr>
        
        </table>
    </td>
</tr>


        
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
    <hr/>    
    <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table-royal-oak?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no8?utm_source=newsletter&utm_medium=email&utm_campaign=royaloak">At The Table Ltd</a>
    </td>
</tr>
@endsection
