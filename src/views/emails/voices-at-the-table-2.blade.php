@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table
</strong><br/>
                        
                        </span> 
                        <br/>
                        Join us for a curated evening of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance, and food worlds.
                        <br/>
                    </p>
                </td>
            </tr>
             
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                      This collaborative series run by At The Table founder Miranda York, writer Rebecca May Johnson and poet Anna Sulan Masing, focuses on bringing people together from different industries and backgrounds, encouraging them to look at food from a fresh perspective.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2">
                        <img src="{{\Storage::url('email/Cooking.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>
                        <br/>
                         <span style="display:block;text-align:justify;">
                      The second event on Tuesday 6th June will see performances and readings from acclaimed writer and historian Bee Wilson, international best-selling novelist Kit de Waal, award-winning playwright Iskander Sharazuddin, Harper’s Bazaar features editor Helena Lee, actor Vera Chok, journalist and author Dawn Foster, writer and podcaster Bethany Rutter, poet and punk band member Jen Calleja, writer Thom Eagle, and actor and lead singer of The Hoosiers Irwin Sparkes.


                        </span>
                    </p>
                </td>
            </tr>


            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <hr/>
                    <p>
                        Cocktails from Our/London, cheese & charcuterie, iced buns from Puns & Buns, books by Review Books.
                        <br/>
                        <br/>
                        <span style="display:block;text-align:center;">
                      Tuesday 6th June 2017<br/>
                      The Drapers Arms, 44 Barnsbury St, Islington<br/>
                      6.30pm-9pm</br/>
                        </span>
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2"><strong>BOOK TICKETS HERE</strong></a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2017 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=newsletter&utm_medium=email&utm_campaign=voices-2">At The Table Ltd</a>
    </td>
</tr>
@endsection
