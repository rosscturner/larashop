@extends('layouts.app')

@section('content')
<div class="row" id="page-intro">
	<div class="small-8 small-centered text-center">
		<h2>{{$email}} has been removed from the mailing list.</h2>
	</div>
</div>
@endsection
