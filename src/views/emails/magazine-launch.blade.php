@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/issue-2-cover.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20launch">
                         Issue 2
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <strong>We’re incredibly excited to announce the launch of issue 2 of our annual food magazine!</strong>
                        <br/>
                        <br/> At The Table magazine focuses on British food culture and the stories that surround it, showcasing the best long-form food writing, photography and illustration.
                        <br/>
                        <br/>
                        In this issue, Diana Henry rhapsodises about her love of menus, Rachel Roddy explores the connections she found between the honest northern food she knew as a girl and the gutsy cooking she discovered in her adopted city of Rome, and Alexander Lobrano recalls the British food he came to love as a hungry American student in 1970s London. We are also honoured to be printing an article by the late Elizabeth David, a writer who arguably did more than anyone to change this country’s food culture for the better.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/sundae.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>

             <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/state.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
              <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/closing.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p>
                        Photographer Rachel Louise Brown captures the atmosphere of London restaurants at closing time, Jamie Freeth focuses on a very unusual form of street food, and we travel to the coast to consume preposterous ice cream sundaes and hear fishermen’s tales. And there’s a dash of poetry, fiction and a comic strip thrown into the mix for good measure.
                    </p>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">ORDER YOUR COPY HERE</a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <hr/>
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/shop?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/balloons.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                        <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=magazinelaunch&utm_medium=email&utm_campaign=Magazine%20Launch">Launch Party
                        </a>
                    </h2>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p style="margin:10px 0;">To celebrate the launch of issue 2 we’re throwing a party! Magazines, Hepple Gin cocktails &amp; delicious snacks from Gail’s Bakery. We have a limited number of tickets available for our lovely readers. Simply rsvp to <a style="text-decoration: none;color:black;" href="mailto:info@atthetable.co.uk">info@atthetable.co.uk</a> and the first 30 readers will receive a ticket. We’d love to meet you!

                    </p>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="mailto:info@atthetable.co.uk"> RSVP HERE</a>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <table class="invoice" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 100%; margin: 0px 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <hr/>
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/editorial?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20Launch">
                        <img src="{{\Storage::url('email/hobnobs.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/editorial/?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20launch">
                         Online Editorial
                         </a>
                     </h2>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:justify;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <p style="margin:10px 0;">Our new blog is now live! A culinary notebook full of articles, illustrations, notes and scribbles. In depth analysis of HobNobs? Check. Animated food tips? Check. Interviews with some of the most fascinating people on the British food scene? Well, you get the picture.
                    </p>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/editorial/?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20launch">START READING HERE</a>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20launch">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a href="https://twitter.com/__atthetable__"><img style="width:35px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:35px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/magazine-launch?utm_source=magazine-launch&utm_medium=email&utm_campaign=Magazine%20launch" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2016 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=magazine-launch&utm_medium=email&utm_campaign=magazine%20launch">At The Table Ltd</a>
    </td>
</tr>
@endsection
