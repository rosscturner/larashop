@extends('emails.layouts.wrapper') @section('content')
<!-- Main content -->
<td class="container">
    <div class="content">
        <span class="preheader">Thank you for confirming your email with Verv</span>
        <table class="main">
            <!-- Branding -->
            <tr>
                <td class="verv-logo-container">
                    <img class="verv-logo" alt="Verv" src="https://verv.energy/cdn/trialsupport/logo.png" />
                </td>
            </tr>
            
            <!-- Primary Content -->
          
            <tr>
                <td class="primary-content">
                    <p class="verv-tagline">Verv Contact Form</p>
                    <p><strong>Name:</strong> {{$contact->name}}</p>
                    <p><strong>Email:</strong> {{$contact->email}}</p>
                    <p><strong>Number:</strong> {{$contact->phone}}</p>
                    <p>{{$contact->message}}</p>
                </td>
            </tr>
            <!-- Blank seperator -->
           <tr>
                <td class="verv-seperator">&nbsp;</td>
            </tr>

            <!-- Footer Glyph Header -->
            <tr class="cancel-margins">
                <td class="cancel-margins"> <img src="https://verv.energy/cdn/trialsupport/separator-bar.png" class="seperator-bar" alt=""> </td>
            </tr>
            <!-- Footer Content -->
            <tr class="cancel-margins">
                <td class="footer-content">
                    <p class="footer-address">Green Running LTD, St. Magnus House, 3 Lower Thames Street, London, EC3R 6HD</p>
                    <p class="footer-tagline">Verv - The Advanced Home Energy Monitor</p>
                </td>
            </tr>
            <!-- Blank Bar For The Verv Gradient -->
            <tr class="cancel-margins">
                <td class="footer-bar"></td>
            </tr>
        </table>
    </div>
</td>
</tr>
@endsection
