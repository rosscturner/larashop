@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=popup&utm_medium=email&utm_campaign=popup">
                        <img style="height:25%;width:auto;margin-top:20px;" src="{{\Storage::url('pages/pop-up.png')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Food conversations, magazines and handmade tableware</strong><br/>
                        30th November - 6th December
                        </span> 
                        <br/>
                        The great intellectuals and artists of the 17th century met in literary salons to refine taste, increase knowledge and stimulate curiosity through conversation. It was rarely the solitude of the study that inspired sudden eureka moments, instead it was the fierce debates held in the salons that allowed ideas to expand and mature.   
                        <br/>
                        <br/>
                        To celebrate issue 2 of our annual food magazine, we’re thrilled to invite you to visit The Food Salon. Situated right at the heart of Soho, our pop up is designed as a sanctuary, a place to rest between hectic Christmas shopping. Engage in talks and debates about British food culture, listen to prose and poetry readings by magazine contributors, and discover bespoke handmade gifts. 
                    </p>
                </td>
            </tr>
             <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=popup&utm_medium=email&utm_campaign=popup">
                        <img src="{{\Storage::url('email/table-down.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        We’ve worked with the talented team at EACH London to design The Food Salon and bring  our ideas to life. You’ll be greeted with a complimentary cup of Joe’s Tea Co. tea and invited to enjoy the ‘reading room’ experience – sit in cosy armchairs amidst beautifully-designed furniture from Carl Hansen &amp; Søn and browse a curated collection of the best independent magazines.
                        <br/>
                        <br/>
                        <h2 style="font-size:14px;font-weight:normal;margin:10px 0;text-align: center;">
                             <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/popup?utm_source=popup&utm_medium=email&utm_campaign=popup">
                             JOIN US
                             </a>
                        </h2>
                        <span style="display:block;text-align:center;"><strong> 30th November - 6th December</strong><br/> <a style="color:black;" href="https://www.google.com/maps/place/3+DENMAN+PL,+W1D+7DT/">3 Denman Place, Ham Yard Village, Soho, W1D 7DT</a></span>
                    </p>
                </td>
            </tr>


            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/popup?utm_source=popup&utm_medium=email&utm_campaign=popup">
                        <img src="{{\Storage::url('email/joetea.jpg')}}" alt="Joe's Tea" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/popup?utm_source=popup&utm_medium=email&utm_campaign=popup">
                         Joe’s Tea Co.
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <strong>Join us at The Food Salon for a complimentary cup of tea from Joe’s Tea Co.</strong>
                        <br/>
                        <br/> 
                        Focusing on organic, sustainable and ethical sourcing, the award-winning Joe’s Tea Co. is bringing tea out of the realm of the old and stuffy and reinventing Britain’s favourite brew. Joe’s commitment to only using Ceylon tea means they go the extra mile (well 5,399 miles to be precise) to source from and support precious plantations that are now a rarity. They only use organic tea, herbs and fruit in their blends, grown using traditional farming methods.
                    </p>
                </td>
            </tr>
           <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:14px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="http://joesteacompany.co.uk">
                         joesteacompany.com
                         </a>
                     </h2>
                </td>
            </tr>



            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/magazine?utm_source=popup&utm_medium=email&utm_campaign=popup">
                        <img src="{{\Storage::url('email/mag-stack.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <h2 style="font-size:18px;font-weight:normal;margin:10px 0;">
                         <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=popup&utm_medium=email&utm_campaign=popup">
                         Shop Online
                         </a>
                     </h2>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        Can’t make it to our pop up? Starting to think/panic about Christmas presents? Order our annual food magazine and bespoke tableware collection online before December 16th and we’ll deliver in time for Christmas.
                    </p>
                </td>
            </tr>
            
            
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                    <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/magazine?utm_source=popup&utm_medium=email&utm_campaign=popup">ORDER YOUR COPY HERE</a>
                    <br/>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=popup&utm_medium=email&utm_campaign=popup">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <a style="color:black;" href="https://atthetable.co.uk/email/popup?utm_source=popup&utm_medium=email&utm_campaign=popup" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2016 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/?utm_source=popup&utm_medium=email&utm_campaign=popup">At The Table Ltd</a>
    </td>
</tr>
@endsection
