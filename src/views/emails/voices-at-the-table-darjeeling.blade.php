@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table No.9</strong><br/></span> 
                        <br/>
                        Voices At The Table is a series of curated evenings of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds. 
                        <br/>
                        <br/>
                        For our 9th event of the series we’re heading to Soho for a Sunday lunch at Asma Khan’s wonderful restaurant Darjeeling Express – soon to be featured on Netflix’s Chef's Table. Asma will be the first British chef to be featured on the programme. 
                        <br/>
                        <br/>
                        Along with an incredible line up of talented writers, chefs and performers, the afternoon will include Cobra beers, Nonsuch shrubs and delicious dishes from Asma’s debut cookbook 'Asma’s Indian Kitchen'. Lunch will be served family style and will feature Calcutta street food favourite Papri Chaat, Tangri Kabab, Bengali fish curry Macher Malaikari, and smoky aubergine Baingan Bharta (among others!).
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling">
                        <img src="{{\Storage::url('email/darjeeling.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Asma Khan</strong> – chef/owner of Darjeeling Express, Asma’s first cookbook 'Asma’s Indian Kitchen' is due out 4th October 2018 and she will be the first British chef to appear on Netflix’s Chef’s Table in 2019.


                    <br/>
                    <br/>
                    <strong>Kate Young</strong> –  food writer, cook and author, Kate’s first cookbook 'The Little Library Cookbook' was published in 2017, and her second book is due out next year. Inspired by literature, Kate also writes recipes for the Guardian. 


                    <br/>
                    <br/>
                    <strong>Jimi Famurewa</strong> – writer, podcaster and restaurant critic, Jimi is the newly appointed restaurant reviewer for ES Magazine. Jimi also hosts a music discussion podcast and his writing features in the first issue of 'The Good Journal'. 


                    <br/>
                    <br/>
                    <strong>Sybil Kapoor</strong> –  award-winning food and travel writer and author of nine books, Sybil’s latest book 'Sight Smell Touch Taste Sound: A New Way to Cook', published this month, explores multi-sensory cooking. 

                    <br/>
                    <br/>
                    <strong>Neil Rankin</strong> – chef/owner of Temper restaurants, Neil’s debut book 'Low and Slow: How to Cook Meat' was published in 2016. Neil has been at the forefront of the barbecue and live fire cooking scene in London. 
                    <br/>
                    <br/>
                    <strong>Sheena Bhattessa </strong> – actress and founder of luxury travel publication Citizen Femme, Sheena has featured in a number of Hindi films, as well as on ITV’s The Fixer, and British TV favourites Eastenders and Midsomer Murders. 
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling">
                        <img src="{{\Storage::url('email/darjeeling-food.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
        <tr>
       
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 0 0 0 0;" valign="top">
                <hr/>
                <p>
                    <span style="display:block;text-align:center;">
                        <br/>
                        Sunday 14 October 2018<br/>
                        Darjeeling Express, Top Floor, Kingly Court, London W1B 5PW 
                        <br/>Doors open 12.30pm, performances 1pm 
                    </span>
                </p>
            </td>
        </tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling"><strong>BOOK TICKETS HERE</strong></a>
                <br/>
                <br/>
            </td>
        </tr>
                
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
                <hr/>
            </td>
        </tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 30px 0;" valign="top">
                <img width="25%" src="{{\Storage::url('email/cobra.png')}}" alt="At The Table" />
                <img width="25%" style="margin-left:30px;" src="{{\Storage::url('email/nonsuch.png')}}" alt="At The Table" />
            </td>
        </tr>
        
        </table>
    </td>
</tr>


        
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>


<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
    <hr/>    
    <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table-darjeeling?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-no9?utm_source=newsletter&utm_medium=email&utm_campaign=darjeeling">At The Table Ltd</a>
    </td>
</tr>
@endsection
