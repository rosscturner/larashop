@extends('emails.layouts.email-wrapper') @section('content')
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0;" valign="top">
        <table>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach">
                        <img style="height:auto;max-width:100%;margin-top:20px;" src="{{\Storage::url('email/voices-at-the-table.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                    <p>
                        <span style="display:block;text-align:center;"><strong>Voices At The Table No.7</strong><br/></span> 
                        <br/>
                        Voices At The Table is a series of curated evenings of readings and performances around food, bringing together an eclectic mix of voices across the literary, performance and food worlds.
                        <br/>
                        <br/>
                        
For the 7th event of the series, we’ve drawn together voices from the realms of literature, theatre, history and food to read and perform at a spectacular dinner by legendary chef Henry Harris. Seated at long tables in the elegant, 1920s-inspired dining rooms of The Coach in Clerkenwell, speakers will dine with the audience, entertaining you between courses. A self-confessed classicist, Henry will draw on both British and French influences for each dish, quite simply cooking the food we all want to eat. As restaurant critic Tim Hayward wryly put it, “Henry Harris is widely regarded as the best French chef with the decency to be British.”

                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach">
                        <img src="{{\Storage::url('email/coach.jpg')}}" alt="At The Table" />
                    </a>
                    <hr/>
                </td>
            </tr>
            <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <p>
                    <span style="display:block;text-align:center;"><strong>Speakers</strong>
                    
                    </span> 
                    <br/>
                    <strong>Sarah Winman</strong> – award-winning novelist, actor and author of ‘When God Was a Rabbit’, ‘A Year of Marvellous Ways’ and ‘Tin Man’

                    <br/>
                    <br/>
                    <strong>Alex von Tunzelmann</strong> –  historian, screenwriter (‘Churchill’ starring Brian Cox) and author of four books including ‘Indian Summer: The Secret End of an Empire'
                    <br/>
                    <br/>
                    <strong>Kevin Shen</strong> – Asian-American actor who has recently performed at the National Theatre, Royal Shakespeare Company and on the West End in Lucy Kirkwood's hit play ‘Chimerica’

                    <br/>
                    <br/>
                    <strong>Sophie Mackintosh </strong> – writer, winner of the White Review Short Story Prize and author of debut novel ‘The Water Cure'

                    <br/>
                    <br/>
                    <strong>Henry Harris </strong> – chef extraordinaire, previously chef patron of Racine in Knightsbridge, now chef director of The Coach, The Hero of Maida and Three Cranes

                    <br/>
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
                <td>
                    <hr/>
                    <a href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach">
                        <img src="{{\Storage::url('email/miranda-anna.jpg')}}" alt="At The Table" />
                    </a>
                </td>
            </tr>
        <tr>
       
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:center; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 5px 0 0 0;" valign="top">
                <hr/>
                <p>
                    <span style="display:block;text-align:center;">
                        <br/>
                        25th June 2018<br/>
                        The Coach, 26-28 Ray St, London EC1R 3DJ<br/>
                        Doors open 6.30pm, dinner 7pm
                    </br/>
                    </span>
                </p>
            </td>
        </tr>
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;" valign="top">
                <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach"><strong>BOOK TICKETS HERE</strong></a>
                <br/>
                <br/>
            </td>
        </tr>
        </table>
    </td>
</tr>


        
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        <hr/>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        Discover more at <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach">atthetable.co.uk</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">

        <a href="https://twitter.com/__atthetable__"><img style="width:25px;" src="{{\Storage::url('social/twitter.png')}}" /></a>
        <a href="https://instagram.com/__atthetable__"><img style="margin-left:10px;width:25px;" src="{{\Storage::url('social/instagram.png')}}" /></a>
        <a href="https://facebook.com/joinusatthetable"><img style="margin-left:5px;width:25px;" src="{{\Storage::url('social/facebook.png')}}" /></a>
    </td>
</tr>

<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
 
            <td style="text-align:center;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px 0 5px 0;" valign="top">
            <hr/>     
            <br/>
            <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach"><strong>A NOTE ON PRIVACY</strong></a>
            </td>
</tr>
<tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;text-align:justify; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 0 0;" valign="top">
            <p>
            You’ve probably received hundreds of emails about the new GDPR laws, so we’ll keep it brief. We’ll only email you about things we think you’ll like, and we’ll never share your details with anyone else. If you’d like to read our updated Privacy Policy, you’ll find it <a href="https://atthetable.co.uk/terms-and-conditions">here</a> and you can unsubscribe from these newsletters at any time by clicking the link at the bottom of the email. 
                </p>
                <br/>
            </td>
        </tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
    <hr/>    
    <a style="color:black;" href="https://atthetable.co.uk/email/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach" style="color:black;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #348eda; text-decoration: underline; margin: 0;">View in browser</a>
    </td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
        ©2018 <a style="color:black;text-decoration:none;" href="https://atthetable.co.uk/event/voices-at-the-table-coach?utm_source=newsletter&utm_medium=email&utm_campaign=coach">At The Table Ltd</a>
    </td>
</tr>
@endsection
