@inject('countries','App\Utilities\Countries') @extends('layouts.app') @section('content')
<div id="order" class="light">
    <form method="post" class="checkout grid-container" id="billing-form" action="/checkout/payment">
        {{ csrf_field() }}
        <div style="display:none;">
            @include('checkout.partials.details')
            @include('checkout.partials.contact')
            @include('checkout.partials.billing',['billing'=>$billingAddress])
            @include('checkout.partials.shipping',['shipping'=>$shippingAddress])
        </div>
        <h3 class="cell read-more-header">Review your order</h3>
        @include('checkout.partials.order_items')
        @if($order->price == 0.00)
        <div class="row">
            <div class="small-12 column">
                <p>No eligible products in cart</p>
            </div>
        </div>
        @endif
    </form>
    @include('checkout.partials.stripe')
</div>
@endsection @section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(document).ready(function() {
    //stripe validation
    $('#submit-review').on('click', function() {
        $('#billing-form').parsley({
            errorsWrapper: '<span></span>',
            errorTemplate: '<span class="form-error"></span>'
        }).on('form:success', function(parsley) {
            //open payment form
            $('#stripe-payment').foundation('open');
        }).validate()
    });

    $('#stripe-payment').on('open.zf.reveal', function() {
        StripeBilling.init();
        $('#stripe-form').parsley({
            errorsWrapper: '',
            errorTemplate: ''
        }).on('form:success', function(parsley) {
            //open payment form
            StripeBilling.sendToken(parsley.submitEvent);
        });
    });

});
</script>
@endsection
