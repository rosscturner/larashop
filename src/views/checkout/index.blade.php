@inject('countries','App\Utilities\Countries') @extends('layouts.app') @section('content')
<div class="light">
    <div class="grid-container checkout">
        <form id="details-form" method="POST">
            <div class="grid-x small-12 align-center">
               @include('checkout.partials.details')
            </div>
            <div class="grid-x cell small-12 align-center">
                <fieldset class="cell small-12 medium-10 large-8">
                    @include('checkout.partials.contact')
                </fieldset>
            </div>
            <div class="grid-x align-center">
                <div class="cell small-12 medium-10 large-8">
                    @include('checkout.partials.billing')
                </div>
            </div>

            <div class="grid-x align-center">
                <div class="cell small-12 medium-8 grid-x grid-padding-x select-shipping">
                    <label class="cell small-12 medium-8">
                        <input id="shipping" type="checkbox" name="shipping_address" checked=checked value="shipping">
                            uncheck to enter shipping address
                    </label>
                </div>
            </div>

            <div class="grid-x align-center">
                <div class="cell small-12 medium-10 large-8">
                    @include('checkout.partials.shipping')
                </div>
            </div>

            <div class="grid-x align-center">
                <div class="cell small-12 medium-8 grid-x align-justify checkout">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a href="/" class="button cell small-3">Cancel</a>
                    <input class="button cell small-3" type="submit" name="submit" value="Continue" />
                </div>
            </div>
        </form>
    </div>
</div>



  
@endsection @section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var inputs = $('#shipping_address :input');
    var address = $('#shipping_address');
    $('#shipping').change(
        function() {
            if (!this.checked) {
                inputs.each(function() {
                    $(this).removeAttr('data-parsley-excluded');
                    $(this).removeAttr('disabled');
                    $(this).parsley().destroy();
                });
                address.show();
            } else {
                //checked
                inputs.each(function() {
                    $(this).parsley().destroy();
                    $(this).attr('data-parsley-excluded', "true");
                    $(this).attr('disabled', true);
                });
                address.hide();
            }
        });
    $('#details-form').parsley({
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="form-error"></span>'
    });
});
</script>
@endsection
