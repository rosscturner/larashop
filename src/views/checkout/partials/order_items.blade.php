<div lass="grid-container light grid-x align-center-middle" id="sideCart">
	
		<table class="table-scroll cell" width="100%">
			<thead>
				<tr>
				<th width="150">Item</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
				</tr>
			</thead>
			<tbody>
			@foreach($cart as $item)
				@if($item->shipping != false)
					@if($item->altered)
						@if($item->qty == 0)
							<tr class="callout warning"><td colspan="3">The product below is out of stock and has been removed from your order.</td></tr>
						@else
							<tr class="callout warning"><td colspan="3">The product below has been altered to reflect stock levels.</td></tr>
						@endif

					@endif
				@endif
				@if($item->shipping == false)
					<tr class="callout warning item">
						<td colspan="5">The product below cannot be shipped to your country and has been removed from your order.</td>
					</tr>
				@endif
				<tr class="item">
					<td>
						@php dd($item); @endphp
                        @if($item->model->bespoke)
                        <a href=""><img src="{{\Storage::url('products/' . $item->product_id . '/landscape-index.jpg')}}" alt=""></a> 
                        @elseif($item->model->ticket)
                        <a href=""><img src="{{\Storage::url('event/' . $item->product_id . '/landscape-index.jpg')}}" alt=""></a> 
                        @else
                        <a href=""><img src="{{\Storage::url('magazine/cover.png')}}" alt=""></a> 
                        @endif
                    </td>
					<td>{{$item->name}}</td>
					<td> &pound;{{$item->price}}</td>
					<td>{{$item->qty}}</td>
					<td> &pound;{{$item->price * $item->qty}}</td>
				</tr>
			@endforeach
			<tr class="shipping-row">
				<td colspan="5">
					<label>
						I've read and accept the terms and conditions
            			<input type="checkbox" required name="terms" />
           			</label>
        			<label>
						Sign up for news and event updates
        				<input type="checkbox" name="signup" />
        			</label>
				</td>
			</tr>
			<tr class="shipping-row">
				<td colspan="5"><b>Shipping:</b> &pound;{{number_format($cart->shipping, 2, '.', ',')}}</td>
			</tr>
			<tr class="shipping-row" >
				<td colspan="5">
					<b>Total:</b> &pound;{{$order->price}}
				</td>
			</tr>
			<tr class="shipping-row">
				<td colspan="5">
						<a class="float-left button" href="{{url()->previous()}}" >Back</a>
						<a class="float-left button" href="{{url()->previous()}}" >Cancel</a>
					@if($order->price > 0.00)
						<a class="button float-right" id="submit-review" href="#">Proceed to Payment</a>
					@endif
				</td>
			</tr>
		</tbody>
	</table>
</div>
