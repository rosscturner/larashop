<div class="reveal checkout grid-container" id="stripe-payment" data-reveal>
    <form method="post" id="stripe-form" class="grid-x grid-padding-x">
      
        <div class="cell small-12">
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3  class="cell read-more-header">Enter Your Card Details</h3>
            <div class="small-12 column">
                <small class="payment-errors"></small>
            </div>
            <input required type="hidden" id="pub-key" value="{{Config::get('stripe.publishable_key')}}" />
            <label>Card Number: <span id="cc-errors"></span></label>
            <input data-parsley-luhn minlength="12" data-parsley-errors-container="#cc-errors" required placeholder="number"  type="number" data-stripe="number" />
        </div>
        <div class="cell small-12 grid-x align-justify">
            <div class="cell small-7">
                <label>Expiration Number: <span id="exp-errors"></span></label>
                <div class="grid-x">
                    <select class="cell small-6" data-stripe="exp_month" name="exp_month" required>
                        <option value="">Month</option>
                        <option value="01">1 - January</option>
                        <option value="02">2 - February</option>
                        <option value="03">3 - March</option>
                        <option value="04">4 - April</option>
                        <option value="05">5 - May</option>
                        <option value="06">6 - June</option>
                        <option value="07">7 - July</option>
                        <option value="08">8 - August</option>
                        <option value="09">9 - September</option>
                        <option value="10">10 - October</option>
                        <option value="11">11 - November</option>
                        <option value="12">12 - December</option>
                    </select>
                
                    <select class="cell small-6" data-stripe="exp_year" name="exp_year" required>
                        <option value="">Year</option>
                        <option value="16">2016</option>
                        <option value="17">2017</option>
                        <option value="18">2018</option>
                        <option value="19">2019</option>
                        <option value="20">2020</option>
                        <option value="21">2021</option>
                        <option value="22">2022</option>
                        <option value="23">2023</option>
                        <option value="24">2024</option>
                        <option value="25">2025</option>
                    </select>
                </div>
            </div>
            <div class="cell small-4">
                <label>CVC Number: <span id="cvc-errors"></span></label>
                <input required minlength="3" data-parsley-errors-container="#cvc-errors" placeholder="3 or 4 Digits" type="number" data-stripe="cvc" />
            </div>
            <div class="cell">
                <input type="submit" id="pay-now" class="button expanded" value="Pay Now" />
            </div>
            <div class="grid-x align-justify">
                <div class="cell small-3">
                    <img src="{{\Storage::url('resources/cards.jpg')}}" alt="cards accepted" />
                </div>
                <div class="cell small-3">
                    <img src="{{\Storage::url('resources/stripe.png')}}" alt="powered by Stripe" />
                </div>
            </div>
        </div>
    </form>
</div>
