 <!--CONTACT -->
 <div class="cell small-12 grid-x grid-padding-x">
    <h3 class="cell read-more-header">Contact</h3>
    <div class="cell small-6">
        <label for="email">Email<span> * </span>
            <span id="email-errors"></span>
        </label>
        <div class="input">
            <input data-parsley-errors-container="#email-errors" data-parsley-type="email" required placeholder="Email" type="email" name="email" 
            
            @if(isset($orderUser))
                    value="{{ $orderUser->email }}"
                @else 
                    value="{{ Request::old('email') }}" 
                @endif

                />
        </div>
    </div>
    <div class="cell small-6">
        <label for="number">Phone Number</span>
            <span id="number-errors"></span>
            <input  data-parsley-errors-container="#number-errors" minlength="5"  placeholder="number" type="number" name="telephone" 
             @if(isset($phone))
                    value="{{ $phone->number }}"
                @else 
                    value="{{ Request::old('number') }}" 
                @endif />
        </label>
    </div>
</div>