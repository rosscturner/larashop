
<!--Billing Address-->
<div class="cell small-12 medium-12 grid-x grid-padding-x" id="billing_address">
    <h3 class="cell read-more-header">Billing Address</h3>
	<div class="cell small-6">
	<!-- address line 1 -->
		<label for="email">Address line 1<span> * </span>
			<span id="address-one-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-errors-container="#address-one-errors"
					required
					type="text"
					name="address_line_1"
					@if(isset($billing))
						value="{{$billing->address_line_1 }}" 
					@else
						value="{{ old('address_line_1')}}"
					@endif
					/>
		</div>
	</div>	

	<div class="small-6 cell">
		<!-- address line 2 -->
		<label for="email">Address line 2</label>
		<div class="input">
			<input 
				type="text" 
				name="address_line_2" 
				@if(isset($billing))
					value="{{$billing->address_line_2 }}" 
				@else
					value="{{ old('address_line_2') }}"
				@endif
				/>
		</div>
	</div>
		
	<!-- city -->
	<div class="cell small-6">
		<label for="email">City<span> * </span>
		<span id="city-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-errors-container="#city-errors"
					required
					type="text"
					name="city"
					@if(isset($billing))
						value="{{$billing->city }}" 
					@else
						value="{{ old('city') }}"
					@endif
					/>
		</div>
	</div>

	<!-- county/region -->
	<div class="cell small-6">
		<label for="email">County/Region<span> * </span>
			<span id="county-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-errors-container="#county-errors"
					required
					type="text"
					name="county"
					@if(isset($billing))
						value="{{$billing->county }}" 
					@else
						value="{{ old('county') }}"
					@endif
					/>
		</div>
	</div>
		<!-- Country -->
	<div class="cell small-6">
		<label for="country">Country<span> * </span>
		</label>
		<div class="input">
			<select name="country" >
				@foreach($countries::all() as $country)
					@if(isset($billing))
						@if($billing->country == $country['code'])
						<option selected="selected" value="{{ $billing->country }}">{{ $country['name'] }}</option>
						@endif
					@else
						@if(old('country') == $country['code'])
							<option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
						@endif
						<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
		<!--postcode -->
	<div class="small-6  cell">
		<label for="email">Postcode<span> * </span>
		<span id="postcode-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-errors-container="#postcode-errors"
					required
					type="text"
					name="postcode"
					@if(isset($billing))
						value="{{$billing->postcode }}" 
					@else
						value="{{ old('postcode') }}"
					@endif
					/>
		</div>
	</div>
</div>
