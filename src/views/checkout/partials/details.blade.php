<div class="cell small-12 medium-10 large-8 grid-x  grid-padding-x">
    <h3 class="cell read-more-header">Your Details</h3>
    <div class="cell small-6">
        <label for="email">First Name<span> * </span> <span id="first-name-errors"></span></label>
        <div class="input">
            <input data-parsley-errors-container="#first-name-errors" required placeholder="First Name" name="firstname" type="text" 
                @if(isset($orderUser))
                    value="{{ $orderUser->firstname }}"
                @else 
                    value="{{ Request::old('firstname') }}" 
                @endif
            />
        </div>
    </div>
    <div class="cell small-6">
        <label for="email">Surname<span> * </span><span id="surname-name-errors"></span></label>
        <div class="input">
            <input data-parsley-errors-container="#surname-name-errors" required placeholder="Surname" name="lastname" 
            type="text" 
            @if(isset($orderUser))
                value="{{ $orderUser->lastname }}"
            @else 
                value="{{ Request::old('lastname') }}"
            @endif
             />
        </div>
    </div>
</div>