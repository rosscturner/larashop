<!--Shipping Address-->
<div class="cell small-12 medium-12 grid-x grid-padding-x" id="shipping_address" style="display:none;"	 >
	<h3 class="cell read-more-header">Shipping Address</h3>
	<div class="cell small-6">
		<!-- address line 1 -->
		<label for="email">Address line 1<span> * </span>
			<span id="ship-address-one-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-excluded="true"
					@if(!isset($shipping))
						disabled
					@endif
					data-parsley-errors-container="#ship-address-one-errors"
					type="text"
					name="ship_address_line_1"
					@if(isset($shipping))
						value="{{$shipping->address_line_1 }}" 
					@else
						value="{{ old('ship_address_line_1')}}"
					@endif
					/>
		</div>
	</div>
	<!-- address line 2 -->
	<div class="small-6 cell">
		<!-- address line 2 -->
		<label for="email">Address line 2</label>
		<div class="input">
			<input 	data-parsley-excluded="true"
			@if(!isset($shipping))
						disabled
					@endif
					type="text"
					name="ship_address_line_2"
					@if(isset($shipping))
					value="{{$shipping->address_line_2 }}" 
				@else
					value="{{ old('ship_address_line_2') }}"
				@endif
				/>
		</div>
	</div>

	<!-- city -->
	<div class="cell small-6">
		<label for="city">City<span> * </span>
		<span id="ship-city-errors"></span>
		</label>
		<div class="input">
			<input 	@if(!isset($shipping))
						disabled
					@endif
					data-parsley-excluded="true"
					data-parsley-errors-container="#ship-city-errors"
					required
					type="text"
					name="ship_city"
					@if(isset($shipping))
						value="{{$shipping->city }}" 
					@else
						value="{{ old('ship_city') }}"
					@endif
					/>
		</div>
	</div>

	<!-- county/region -->
	<div class="cell small-6">
		<label for="email">County/Region<span> * </span>
			<span id="ship-county-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-excluded="true"
					data-parsley-errors-container="#ship-county-errors"
					@if(!isset($shipping))
						disabled
					@endif
					required
					type="text"
					name="ship_county"
					@if(isset($shipping))
						value="{{ $shipping->city }}" 
					@else
						value="{{ old('ship_county') }}"
					@endif
					/>
		</div>
	</div>
	<!-- Country -->
	<div class="cell small-6">
		<label for="country">Country<span> * </span>

		</label>
		<div class="input">
			<select 	
						@if(!isset($shipping))
							disabled
						@endif
						data-parsley-excluded="true"
						name="ship_country" >
				@foreach($countries::all() as $country)

					@if(isset($shipping))
						@if($shipping->country == $country['code'])
						<option selected="selected" value="{{ $shipping->country }}">{{ $country['name'] }}</option>
						@endif
					@else
					@if(old('ship_country') == $country['code'])
						<option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
					@endif
						<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	<!--postcode -->
	<div class="small-6  cell">
		<label for="postcode">Postcode<span> * </span>
		<span id="ship-postcode-errors"></span>
		</label>
		<div class="input">
			<input 	data-parsley-excluded="true"
					data-parsley-excluded
					data-parsley-errors-container="#ship-postcode-errors"
					required
					type="text"
					name="ship_postcode"
					@if(!isset($shipping))
						disabled
					@endif
					@if(isset($shipping))
						value="{{$shipping->postcode }}" 
					@else
						value="{{ old('postcode') }}"
					@endif
					/>
		</div>
	</div>
</div>
