@extends('layouts.app')

@section('content')
<div class="grid-x align-center-middle" id="success-page">
	<div class="cell small-11 medium-6 grid-x align-center-middle flex-dir-column text-center">
		<h3 class="cell read-more-header shrink">THANK YOU FOR YOUR PURCHASE</h3>
		<p >Your order reference: {{$order->id}}</p>
		<p>You will receive an order confirmation email shortly</p>
		<p><a class="button expanded" href="/shop" >Continue Shopping</a></p>
	</div>
</div>
@endsection
