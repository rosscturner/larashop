@extends('layouts.app') @section('pageType', $group->group_name . ' | Events Series | ' )@section('content')
<div class="row-section light">
    <div class="grid-x align-center text-center products event-group">
        <div class="cell flex-container align-center">
            <div data-bg="{{\Storage::url('event-groups/' . $group->id . '/hero.jpg')}}" class="lazyload teaser-image" ></div>
        </div>
        <div class="grid-x flex-column-dir align-center-middle text-center header-style-2">
            <h3 class="flex-child-shrink">Event Series</h3>
            <h2 class="cell">{{ $group->group_name}}</h2>
        </div> 
    </div>
</div>
<div class="grid-x small-12 text-center editorial-footer ">
    <h3 class="cell read-more-header">UPCOMING EVENTS</h3>
</div>

<div class="light">
    <div class="grid-container">
            @php $count = 1; $totalcount = 0; @endphp
            @foreach ($events as $event)
                @php
                    $date_facturation = \Carbon\Carbon::parse($event->start_time);
                @endphp
                @if ($date_facturation->isFuture())
                @if($count == 1)
                <div class="row-section grid-x  grid-padding-x small-up-1 medium-up-3 product-row">
                @endif
                    <a href="/event/{{$event->slug}}"  class="cell product-container">
                        <div class="flex-container align-center row-image-{{($count)}}"> 
                        @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('event/' . $event->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                        	@else
                            <div data-bg="{{\Storage::url('event/' . $event->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
		                @endif
                        </div>
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($count)}}">
                            <h3  class="flex-child-shrink">{{ date('d/m/Y', strtotime( $event->start_time) ) }}</h3>
                            <h2 class="cell">{{ $event->name}}</h2>
                            <p class="cell">{{$event->sub_heading}}</p>
                        </div>
                    </a>
                @endif
                @if($count == 3)
                </div>
                @endif
                @php
                    if($date_facturation->isFuture()) 
                    {
                        $totalcount++;
                        $count >= 3 ? $count = 1: $count ++;
                    }
                @endphp
            @endforeach
            @if($totalcount < 3)
                </div>
            @endif
    </div>
</div>

<div class="grid-x small-12 text-center editorial-footer ">
    <h3 class="cell read-more-header">PAST EVENTS</h3>
</div>

<div class="light">
    <div class="grid-container">
            @php $count = 1; $totalcount = 0; @endphp
            @foreach ($events as $event)
                @php
                    $date_facturation = \Carbon\Carbon::parse($event->start_time);
                @endphp
                @if ($date_facturation->isPast())
                @if($count == 1)
                <div class="row-section grid-x  grid-padding-x small-up-1 medium-up-3 product-row">
                @endif
                    <a href="/event/{{$event->slug}}"  class="cell product-container">
                        <div class="flex-container align-center row-image-{{($count)}}">  
                        @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('event/' .$event->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                        	@else
                            <div data-bg="{{\Storage::url('event/' . $event->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
		                @endif
                        </div>
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($count)}}">
                            <h3  class="flex-child-shrink">{{ date('d/m/Y', strtotime( $event->start_time) ) }}</h3>
                            <h2 class="cell">{{ $event->name}}</h2>
                            <p class="cell">{{$event->sub_heading}}</p>
                        </div>
                    </a>
                @endif
                @if($count == 3)
                </div>
                @endif
                @php 
                    if($date_facturation->isPast()) 
                    {
                        $totalcount++;
                        $count >= 3 ? $count = 1: $count ++;
                    }
                @endphp
            @endforeach
            @if($totalcount < 3)
                </div>
            @endif
    </div>
</div>
@endsection('content')
