@extends('layouts.app') @section('pageType', 'Events | ' )@section('content')
<div class="light">
    <div class="grid-container ">
        <div class="grid-x grid-padding-x align-center-middle text-center product-row">
            @foreach ($groups as $group)
                <a href="series/{{$group->slug}}" class="cell small-11 medium-8 product-container">
                        <img data-src="{{\Storage::url('event-groups/' . $group->id . '/hero.jpg')}}"  class="lazyload teaser-image" />
                    <div class="grid-x flex-column-dir align-center-middle text-center header-style-2">
                        <h3 class="flex-child-shrink">EVENT SERIES</h3>
                        <h2 class="cell">{{$group->name}}</h2>
                    </div>
                </a>
            @endforeach
        </div>

        <div class="grid-x small-12 text-center editorial-footer ">
            <h3 class="cell read-more-header">UPCOMING EVENTS</h3>
        </div>

        <div class="light">
            <div class="grid-container">
            @php $count = 1; $totalcount = 0; @endphp
            @foreach ($events as $event)
                @php
                    $date_facturation = \Carbon\Carbon::parse($event->start_time);
                @endphp
                @if ($date_facturation->isFuture())
                @if($count == 1)
                <div class="row-section grid-x  grid-padding-x small-up-1 medium-up-3 product-row">
                @endif
                    <a href="/event/{{$event->slug}}"  class="cell product-container">
                        <div class="flex-container align-center row-image-{{($count)}}"> 
                        @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('event/' . $event->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                        	@else
                            <div data-bg="{{\Storage::url('event/' . $event->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
		                @endif
                        </div>
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($count)}}">
                            <h3  class="flex-child-shrink">{{ date('d/m/Y', strtotime( $event->start_time) ) }}</h3>
                            <h2 class="cell">{{ $event->name}}</h2>
                            <p class="cell">{{$event->sub_heading}}</p>
                        </div>
                    </a>
                @endif
                @if($count == 3)
                </div>
                @endif
                @php
                    if($date_facturation->isFuture()) 
                    {
                        $totalcount++;
                        $count >= 3 ? $count = 1: $count ++;
                    }
                @endphp
            @endforeach
            @if($totalcount < 3)
                </div>
            @endif
            </div>
        <div class="grid-x small-12 text-center editorial-footer ">
            <h3 class="cell read-more-header">PAST EVENTS</h3>
        </div>
            @php $count = 1; @endphp
            @foreach ($events as $event)
            @if(\Carbon\Carbon::parse($event->start_time)->isPast()  )
                    @if($count == 1)
                    <div class="row-section grid-x grid-padding-x small-up-1 medium-up-3 product-row">
                    @endif
                        <a href="event/{{$event->slug}}" class="cell product-container" >
                            @if($event->teaser === null)
                            <div class="flex-container align-center row-image-{{($count)}}">    
                            @if($loop->iteration % 2 == 0)
                                <div data-bg="{{\Storage::url('event/' . $event->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                                @else
                                <div data-bg="{{\Storage::url('event/' . $event->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                            @endif
                            </div>
                            @else
                            <div class="readmore-image texture"><p>{{$event->teaser}}</p></div>
                            @endif
                            <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($count)}}">
                                <h3 class="flex-child-shrink">{{ date('d/m/Y', strtotime( $event->start_time) ) }}</h3>
                                <h2 class="cell">{{ $event->name}}</h2>
                                <p class="cell">{{$event->sub_heading}}</p>
                            </div>
                        </a>
                        @if($count == 3)
                        </div>
                        @endif
                    @php 
                        $count >= 3 ? $count = 1: $count ++ 
                    @endphp
                @endif
            @endforeach
        </div>
    </div>
</div>
@endsection('content')
