@extends('layouts.app')
@section('facebook_meta') 
<meta property="og:url" content="{{  url('/event/') }}/{{$event->slug}} ">
<meta property="og:image" content="{{\Storage::url('event/' . $event->id . '/portrait-index.jpg')}}">
<meta property="og:description" content="{{$event->name . ' - ' . $event->sub_heading }}"> 
@endsection 
@section('pageDesc',$event->excerpt)
@section('pageType', $event->sub_heading . ', ' . $event->name . ',  ' . date('D, d M, Y \a\t G::i', strtotime( $event->ticket[0]->start_time)) . ' | Event |  ' ) 
@section('content')


<div class="editorial-wrapper">
    <article role="main" class="light article">
        <div class="grid-container">
        <header class="grid-x align-justify">
                <div class="cell small-11  medium-5 align-center-middle flex-container flex-dir-column post-title  ">
                    <h3 class="flex-child-shrink">EVENT</h3>
                    <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">
                        {{$event->name}}
                    </h2>
                    <p class="flex-child-shrink">
                        {{$event->sub_heading}}
                    </p>
                </div>
                <div class="cell small-11 mobile-social">
                    <p class="social mobile-social">
                            <a href="mailto:?subject={{$event->name}}&amp;body={{ url('/event') }}/{{$event->slug}}"
                                        title="Share by Email" >
                                        <img src='/img/social/mail.svg' />
                                    </a>
                            <a onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;" href="http://twitter.com/intent/tweet?status={{$event->name}}+{{ url('/event  /') }}/{{$event->slug}}"><img src='/img/social/twitter.svg' /></a>
                                <a onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;"  href="http://www.facebook.com/sharer/sharer.php?u={{ url('/event/') }}/{{$event->slug}}&title={{$event->title}}"><img src='/img/social/facebook.svg' /></a>
                    </p>
                </div>
                <div class="cell small-11 medium-6 flex-container align-center-middle header-image slider">
                    <div class="slide">
                        <img src="{{\Storage::url('event/' . $event->id . '/landscape-index.jpg')}}" class="lazyload"/>
                    </div>
                </div>
            </header>
        </div>
        
        <div class="grid-container ">
            <div class="grid-x row-container">
                <div class='cell medium-6 post-content section-1 left'>
                    <p class="social desktop-social">
                        <a href="mailto:?subject={{$event->name}}&amp;body={{ url('/event') }}/{{$event->slug}}"
                                    title="Share by Email" >
                                    <img src='/img/social/mail.svg' />
                                </a>
                        <a onclick="window.open(this.href,'_blank',
        'width=500,height=500,toolbar=0'); return false;" href="http://twitter.com/intent/tweet?status={{$event->name}}+{{ url('/event/') }}/{{$event->slug}}"><img src='/img/social/twitter.svg' /></a>
                            <a onclick="window.open(this.href,'_blank',
        'width=500,height=500,toolbar=0'); return false;"  href="http://www.facebook.com/sharer/sharer.php?u={{ url('/event/') }}/{{$event->slug}}&title={{$event->title}}"><img src='/img/social/facebook.svg' /></a>
                    </p>
                </div>
                
                <div class='cell small-11 medium-6 product post-content section right'>
                    <p class="text-justify ">{!! nl2br($event->description) !!}</p>
                    @foreach($event->ticket as $ticket)
                    <br/>
                    <p class="cost">{{ucfirst($ticket->description)}} - &pound;{{ number_format($ticket->price,0) }}</p>
                    <form method="POST" action="{{ url('cart') }}">
                        <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if($event->stock > 0 && $event->status->name == 'published')
                        <button type="submit" class="button  expanded add-to-cart">ADD TO CART</button>
                        @elseif( $event->status->name == 'draft')
                        <button  disabled class="button  expanded add-to-cart">COMING SOON</button>
                        @else
                        <button  disabled class="button  expanded add-to-cart">SOLD OUT</button>
                        @endif
                    </form>
                    @endforeach
                </div>
            </div>
              
        </div>
    </article>
</div>
@include('partials.readmore')
@endsection('content')
@section('scripts')
<script type="text/javascript" >
$(document).ready(function(){
    $('.slidy').slick({
        lazyLoad: 'ondemand',
        prevArrow: '<div class="nav left-arrow"><img src="/img/left-arrow.png"/></div>',
        nextArrow: '<div class="nav right-arrow"><img  src="/img/right-arrow.png"/></div>',
        mobileFirst:true,
    });
});

</script>
@endsection('scripts')
