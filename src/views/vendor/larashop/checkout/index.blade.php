@inject('countries','App\Utilities\Countries') @extends('layouts.app') @section('content')
<div class="row">
    <div class="small-12 column">
        <ul class="alert-box">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-8 small-centered column">
        <form id="details-form" method="POST">
            <!--NAME-->
            <fieldset>
                <h2 class="form-header">Your Order Details</h2>
                <!--First Name -->
                <div class="row">
                    <div class="small-12 column">
                        <label for="email">First Name<span> * </span> <span id="first-name-errors"></span></label>
                        <div class="input">
                            <input data-parsley-errors-container="#first-name-errors" required placeholder="First Name" name="firstname" type="text" value="{{ Request::old('firstname') }}" />
                        </div>
                    </div>
                </div>
                <!--Surname -->
                <div class="row">
                    <div class="small-12 column">
                        <label for="email">Surname<span> * </span><span id="surname-name-errors"></span></label>
                        <div class="input">
                            <input data-parsley-errors-container="#surname-name-errors" required placeholder="Surname" name="lastname" type="text" value="{{ old('lastname') }}" />
                        </div>
                    </div>
                </div>
            </fieldset>
            <!--CONTACT -->
            <fieldset>
                <h2 class="form-header">Contact</h2>
                <div class="row">
                    <div class="small-12 column">
                        <label for="email">Email<span> * </span>
                            <span id="email-errors"></span>
                        </label>
                        <div class="input">
                            <input data-parsley-errors-container="#email-errors" data-parsley-type="email" required placeholder="Email" type="email" name="email" value="{{ old('email') }}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-9 column">
                        <label for="number">Phone Number<span> * </span>
                            <span id="number-errors"></span>
                            <input    <input  data-parsley-errors-container="#number-errors" minlength="5" required placeholder="number" type="number" name="telephone" value="{{ old('number') }}" />
                        </label>
                    </div>
                </div>
            </fieldset>
            @include('larashop::checkout.partials.billing')
            <fieldset>
                <label>Uncheck to enter shipping address</label>
                <input id="shipping" type="checkbox" name="shipping_address" checked=checked value="shipping">
            </fieldset>
            @include('larashop::checkout.partials.shipping')
            <fieldset>
                <div class="actions row">
                    <div class="small-12 column">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="/" class="button">Cancel</a>
                        <input class="button float-right" type="submit" name="submit" value="Continue" />
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
@endsection @section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var inputs = $('#shipping_address :input');
    var address = $('#shipping_address');
    $('#shipping').change(
        function() {
            if (!this.checked) {
                inputs.each(function() {
                    $(this).removeAttr('data-parsley-excluded');
                    $(this).removeAttr('disabled');
                    $(this).parsley().destroy();
                });
                address.show();
            } else {
                //checked
                inputs.each(function() {
                    $(this).parsley().destroy();
                    $(this).attr('data-parsley-excluded', "true");
                    $(this).attr('disabled', true);
                });
                address.hide();
            }
        });
    $('#details-form').parsley({
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="form-error"></span>'
    });
});
</script>
@endsection
