@extends('layouts.app')

@section('content')
<div class="row" id="page-intro">
	<div class="small-8 small-centered text-center">
		<h2>PAYMENT FAILED</h2>
		<h3>WE HAVE BEEN UNABLE TO COMPLETE YOUR ORDER</h3>
		<p>Reason: {{$errors}}</p>

	</div>
</div>
@endsection
