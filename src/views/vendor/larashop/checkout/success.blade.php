@extends('layouts.app')

@section('content')
<div class="row" id="page-intro">
	<div class="small-8 small-centered text-center">
		<h2>YOUR ORDER HAS BEEN RECEIVED</h2>
		<h3>THANK YOU FOR YOUR PURCHASE</h3>
		<p>Your order # is: {{$order->id}}</p>
		<p>You will receive an order confirmation email shortly</p>
		<p><a class="button expanded" href="/shop" >Continue Shopping</a></p>
	</div>
</div>
@endsection
