@inject('countries','App\Utilities\Countries') @extends('layouts.app') @section('content')
<div id="order">
    <form method="post" id="billing-form" action="/checkout/payment">
        <div class="row">
            <div class="small-12 column">
                <h2 class="form-header">Review your order</h2>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="small-12 large-6 column">
                <h3>Personal Details</h3>
                <hr>
                <!-- first name -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="first-name">First Name<span> * </span> <span id="first-name-errors"></span></label>
                    </div>
                    <div class="small-6 column end">
                        <input data-parsley-errors-container="#first-name-errors" required placeholder="First Name" name="firstname" type="text" value="{{$orderUser->firstname}}" />
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 column">
                        <label for="lastname">Surname<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="lastname" type="text" value="{{$orderUser->lastname}}">
                    </div>
                </div>
                <br/>
            </div>
            <div class="small-12 large-6 column">
                <!-- Contact details -->
                <h3>Contact Details</h3>
                <hr>
                <div class="row">
                    <div class="small-3 column">
                        <label for="email">E-mail<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required placeholder="email" type="email" name="email" id="email" value="{{$orderUser->email}}" />
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 column">
                        <label for="email">Telephone<span> *</span></label>
                    </div>
                    <div class="small-4 column end">
                        <input required name="telephone" type="text" value="{{$phone->number}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 large-6 column">
                <!-- Billing Address -->
                <h3>Billing Address: </h3>
                <hr>
                <div class="row">
                    <div class="small-3 column">
                        <label for="address_line_1">Address Line 1<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="address_line_1" type="text" value="{{$billingAddress->address_line_1}}">
                    </div>
                </div>
                <!-- address line 2 -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="address_line_2">Address Line 2<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input name="address_line_2" type="text" value="{{$billingAddress->address_line_2}}">
                    </div>
                </div>
                <!-- CITY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="city">City<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="city" type="text" value="{{$billingAddress->city}}">
                    </div>
                </div>
                <!-- COUNTY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="county">County<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="county" type="text" value="{{$billingAddress->county}}">
                    </div>
                </div>
                <!-- COUNTRY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="city">Country<span> *</span></label>

                    </div>
                    <div class="small-6 column end">
                        <select name="country">
                            @foreach($countries::all() as $country)
                                    @if($billingAddress->country == $country['code'])
                                        <option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
                                    @endif
                                    <option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <!-- postcode -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="city">Postcode<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="postcode" type="text" value="{{$billingAddress->postcode}}">
                    </div>
                </div>
                <br/>
            </div>
            <div class="small-12 large-6 column">
                <!-- Shipping Address -->
                <h3>Shipping Address: </h3>
                <hr>
                <div class="row">
                    <div class="small-3 column">
                        <label for="address_line_1">Address Line 1<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="ship_address_line_1" type="text" value="{{$shippingAddress->address_line_1}}">
                    </div>
                </div>
                <!-- address line 2 -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="address_line_2">Address Line 2<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input name="ship_address_line_2" type="text" value="{{$shippingAddress->address_line_2}}">
                    </div>
                </div>
                <!-- CITY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="city">City<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="ship_city" type="text" value="{{$shippingAddress->city}}">
                    </div>
                </div>
                <!-- COUNTY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="county">County<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="ship_county" type="text" value="{{$shippingAddress->county}}">
                    </div>
                </div>
                <!-- COUNTRY -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="city">Country<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <select name="ship_country">
                            @foreach($countries::all() as $country)
                                    @if($shippingAddress->country == $country['code'])
                                        <option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
                                    @endif
                                    <option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <!-- postcode -->
                <div class="row">
                    <div class="small-3 column">
                        <label for="postcode">Postcode<span> *</span></label>
                    </div>
                    <div class="small-6 column end">
                        <input required name="ship_postcode" type="text" value="{{$shippingAddress->postcode}}">
                    </div>
                </div>
                <br/>
              	<input type="hidden" name="order_id" value="{{ $order->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        @include('larashop::checkout.partials.order_items')

        @if($order->price == 0.00)
        <div class="row">
            <div class="small-12 column">
                <p>No eligible products in cart</p>
            </div>
        </div>
        @endif
    </form>
    @include('larashop::checkout.partials.stripe')
</div>
@endsection @section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(document).ready(function() {

    //stripe validation


    $('#submit-review').on('click', function() {
        $('#billing-form').parsley({
            errorsWrapper: '<span></span>',
            errorTemplate: '<span class="form-error"></span>'
        }).on('form:success', function(parsley) {
            //open payment form
            $('#stripe-payment').foundation('open');
        }).validate()
    });

    $('#stripe-payment').on('open.zf.reveal', function() {
        StripeBilling.init();
        $('#stripe-form').parsley({
            errorsWrapper: '',
            errorTemplate: ''
        }).on('form:success', function(parsley) {
            //open payment form
              StripeBilling.sendToken(parsley.submitEvent);
        });
    });

});
</script>
</script>
@endsection
