<div class="row">
			<div class="small-12 column">
				<h2>Order Items</h2>
				<hr/>
				<table width="100%">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Qty</th>
							<th>Price</th>
						</tr>
					</thead>
					<tbody>

						@foreach($cart as $item)
						@if($item->shipping != false)
							@if($item->altered)
								@if($item->qty == 0)
									<tr class="callout warning"><td colspan="3">The product below is out of stock and has been removed from your order.</td></tr>
								@else
									<tr class="callout warning"><td colspan="3">The product below has been altered to reflect stock levels.</td></tr>
								@endif

							@endif
						@endif
						@if($item->shipping == false)
								<tr class="callout warning"><td colspan="3">The product below cannot be shipped to your country and has been removed from your order.</td></tr>
							@endif
						<tr>
							<td>{{$item->name}}</td>
							<td>{{$item->qty}}</td>
							<td>&pound;{{$item->price * $item->qty}}</td>
						</tr>
						@endforeach

						<tr class="shipping-row">
							<td></td>
							<td><b>Shipping:</b></td>

							<td>&pound;{{number_format($cart->shipping, 2, '.', ',')}}</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Total:</b></td>
							<td>&pound;{{$order->price}}</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								@if($order->price > 0.00)
									<a class="button float-right" id="submit-review" href="#">Payment</a>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
