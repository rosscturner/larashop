<div class="reveal" id="stripe-payment" data-reveal>
    <form method="post" id="stripe-form">
        <div class="small-12 column">
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row">
                <div class="large-12 columns">
                    <h2>Enter your card details</h2>
                </div>
            </div>
            <div class="small-12 column">
                <small class="payment-errors"></small>
            </div>
            <input required type="hidden" id="pub-key" value="{{Config::get('stripe.publishable_key')}}" />
            <div class="row">
                <div class="large-12 columns">
                    <label>Card Number: <span id="cc-errors"></span></label>
                    <input data-parsley-luhn minlength="12" data-parsley-errors-container="#cc-errors" required placeholder="number"  type="number" data-stripe="number" />
                </div>
            </div>

            <div class="row">
                <div class="large-8 columns">
                    <label>Expiration Number: <span id="exp-errors"></span></label>
                    <div class="row">
                        <div class="large-6 columns">
                            <select data-stripe="exp_month" name="exp_month" required>
                                <option value="">Month</option>
                                <option value="01">1 - January</option>
                                <option value="02">2 - February</option>
                                <option value="03">3 - March</option>
                                <option value="04">4 - April</option>
                                <option value="05">5 - May</option>
                                <option value="06">6 - June</option>
                                <option value="07">7 - July</option>
                                <option value="08">8 - August</option>
                                <option value="09">9 - September</option>
                                <option value="10">10 - October</option>
                                <option value="11">11 - November</option>
                                <option value="12">12 - December</option>
                            </select>
                        </div>
                        <div class="large-6 columns">
                            <select data-stripe="exp_year" name="exp_year" required>
                                <option value="">Year</option>
                                <option value="16">2016</option>
                                <option value="17">2017</option>
                                <option value="18">2018</option>
                                <option value="19">2019</option>
                                <option value="20">2020</option>
                                <option value="21">2021</option>
                                <option value="22">2022</option>
                                <option value="23">2023</option>
                                <option value="24">2024</option>
                                <option value="25">2025</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="large-4 columns">
                    <label>CVC Number: <span id="cvc-errors"></span></label>
                    <input required minlength="3" data-parsley-errors-container="#cvc-errors" placeholder="cvc" type="number" data-stripe="cvc" />
                </div>
                <div class="large-4 columns">
                    <input type="submit" id="pay-now" class="button expanded" value="Pay Now" />
                </div>
                <div class="small-4 large-4 columns">
                    <img src="{{\Storage::url('resources/stripe.png')}}" alt="powered by Stripe" />
                </div>
             </div>
        </div>
    </form>
</div>
