<!--Billing Address-->
			<fieldset>
				<h2 class="form-header">Billing Address</h2>
				<div class="row">
					<div class="small-12 column">
					<!-- address line 1 -->
						<label for="email">Address line 1<span> * </span>
							<span id="address-one-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-errors-container="#address-one-errors"
									required
									type="text"
									name="address_line_1"
									value="{{ old('address_line_1') }}" />
						</div>
					</div>
				</div>

				<div class="row">
					<div class="small-12 column">
						<!-- address line 2 -->
						<label for="email">Address line 2</label>
						<div class="input">
							<input type="text" name="address_line_2" value="{{ old('address_line_2') }}"/>
						</div>
					</div>
				</div>

				<!-- city -->
				<div class="row">
					<div class="small-12 column">
						<label for="email">City<span> * </span>
						<span id="city-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-errors-container="#city-errors"
									required
									type="text"
									name="city"
									value="{{ old('city') }}"/>
						</div>
					</div>
				</div>

				<!-- county/region -->
				<div class="row">
					<div class="small-12 column">
						<label for="email">County/Region<span> * </span>
							<span id="county-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-errors-container="#county-errors"
									required
									type="text"
									name="county"
									value="{{ old('county') }}"/>
						</div>
					</div>
				</div>
				<!-- Country -->
				<div class="row">
					<div class="small-12 column">
						<label for="country">Country<span> * </span>

						</label>
						<div class="input">
							<select name="country" >
								@foreach($countries::all() as $country)
									@if(old('country') == $country['code'])
										<option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
									@endif
									<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<!--postcode -->
				<div class="row">
					<div class="small-12 column">
						<label for="email">Postcode<span> * </span>
						<span id="postcode-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-errors-container="#postcode-errors"
									required
									type="text"
									name="postcode"
									value="{{ old('postcode') }}"/>
						</div>
					</div>
				</div>
			</fieldset>
