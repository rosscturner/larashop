<!--Shipping Address-->
			<fieldset id="shipping_address" style="display:none;">
				<h2 class="form-header">Shipping Address</h2>
				<div class="row">
					<div class="small-12 column">
					<!-- address line 1 -->
						<label for="email">Address line 1<span> * </span>
							<span id="ship-address-one-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-excluded="true"
									disabled
									data-parsley-errors-container="#ship-address-one-errors"
									required
									type="text"
									name="ship_address_line_1"
									value="{{ old('ship_address_line_1') }}" />
						</div>
					</div>
				</div>
				<!-- address line 2 -->
				<div class="row">
					<div class="small-12 column">
						<!-- address line 2 -->
						<label for="email">Address line 2</label>
						<div class="input">
							<input 	data-parsley-excluded="true"
									disabled
									type="text"
									name="ship_address_line_2"
									value="{{ old('ship_address_line_2') }}"/>
						</div>
					</div>
				</div>

				<!-- city -->
				<div class="row">
					<div class="small-12 column">
						<label for="city">City<span> * </span>
						<span id="ship-city-errors"></span>
						</label>
						<div class="input">
							<input 	disabled
									data-parsley-excluded="true"
									data-parsley-errors-container="#ship-city-errors"
									required
									type="text"
									name="ship_city"
									value="{{ old('city') }}"/>
						</div>
					</div>
				</div>

				<!-- county/region -->
				<div class="row">
					<div class="small-12 column">
						<label for="email">County/Region<span> * </span>
							<span id="ship-county-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-excluded="true"
									data-parsley-errors-container="#ship-county-errors"
									required
									type="text"
									name="ship_county"
									value="{{ old('county') }}"
									disabled
									/>
						</div>
					</div>
				</div>
				<!-- Country -->
				<div class="row">
					<div class="small-12 column">
						<label for="country">Country<span> * </span>

						</label>
						<div class="input">
							<select 	data-parsley-excluded="true"
										 name="ship_country" >
								@foreach($countries::all() as $country)
									@if(old('country') == $country['code'])
										<option selected="selected" value="{{ $country['code'] }}">{{ $country['name'] }}</option>
									@endif
									<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<!--postcode -->
				<div class="row">
					<div class="small-12 column">
						<label for="postcode">Postcode<span> * </span>
						<span id="ship-postcode-errors"></span>
						</label>
						<div class="input">
							<input 	data-parsley-excluded="true"
									data-parsley-excluded
									data-parsley-errors-container="#ship-postcode-errors"
									required
									type="text"
									name="ship_postcode"
									disabled
									value="{{ old('ship_postcode') }}"/>
						</div>
					</div>
				</div>
			</fieldset>
