<div class="off-canvas position-right grid-x align-center text-center" id="sideCart" data-off-canvas data-position="right" >
    <div class="cell small-9">
        <ul >
            <li class="home flex-container align-justified align-middle grid-x flex-child-shrink">
                
                <a class="cell small-offset-1 small-10" href="/">
                    <img src="/img/table-icon.jpg" />
                </a> 
                <img src="/img/close.svg" data-toggle="sideCart" class="cell small-1" />
                <hr class="cell small-12"/>
            </li>
        </ul>    
        <section class="cart_items">
            <h2> CART </h2>
            @if(Session::has('cart'))
                @foreach(Cart::content() as $item)
                <div class="grid-x flex-dir-column cart-item align-center-middle">
                    <h3>{{$item->name}}</h3>
                    @if($item->model instanceof \Rosscturner\Larashop\Shop\Product\Bespoke)
                        <a href=""> <img src="{{\Storage::url('products/' . $item->model->product_id . '/landscape-index.jpg')}}" alt=""> </a>
                    @elseif($item->model instanceof \Rosscturner\Larashop\Shop\Product\Ticket)
                        <a href=""><img src="{{\Storage::url('event/' . $item->model->product_id . '/landscape-index.jpg')}}" alt=""></a> 
                    @else
                        <a href=""><img src="{{\Storage::url('magazine/cover.png')}}" alt=""></a>
                    @endif
                    <div class="grid-x align-justify align-middle small-12">
                        <div class="small-6 cell">
                            <div class="cart_quantity_buttons flex-container ">
                                <a class="cart_quantity_down" href='{{url("cart?product_id=$item->id&decrease=1")}}'>-</a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
                                <a class="cart_quantity_up" href='{{url("cart?product_id=$item->id&increment=1")}}'>+</a>
                            </div>
                        </div>
                        <div class="small-4 cell cart_total text-right">
                            <p class="cart_total_price">&pound;{{$item->subtotal}}</p>
                        </div>
                    </div>
                    <hr class="cell small-12"/>
                </div>
                    
                @endforeach
                <div class="grid-x align-justify align-middle small-12 total">
                        <strong class="small-6 text-left">Sub-Total:</strong> <span class="small-6 text-right">&pound; {{\Cart::total()}}</span>
                        <p>Shipping calculated on checkout</p>
                        <hr class="small-12"/>
                </div>
                <a class="button  expanded check_out" href="{{url('checkout')}}">CHECK OUT</a>    
            </div>
            <!--/#do_action-->
            @else
            <div class="row">
                <p class="small-12 column small-centered text-center" id="page-intro">YOU HAVE NO ITEMS IN YOUR SHOPPING CART</p>
            </div>
            @endif
        </section>
    </div>
</div>
