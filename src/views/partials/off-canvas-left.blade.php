<nav class="off-canvas position-left grid-x align-center text-center" id="offCanvas" data-off-canvas>
    <ul class="cell small-10">
        <li class="home flex-container align-justified align-middle">
            <img src="/img/close.svg" data-toggle="offCanvas" class="cell small-1" />
            <a class="cell small-8 small-offset-1" href="/">
                <img src="/img/table-icon.jpg" />
            </a> 
            <hr />
        </li>
        <hr/>
        <li class="icon cell"><a class="icon icon-data" href="/magazine">Magazine</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/editorial">Editorial</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/events">Events</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/podcast">Podcast</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/products">Products</a></li>
        <hr/>
        <li class="icon cell"><a class="icon icon-data" href="/about">About</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/contact">Contact</a></li>
        <li class="icon cell"><a class="icon icon-data" href="/bespoke">Bespoke</a></li>
        <hr/>
        <div class="cell flex-container small-10 flex-dir-column nav-newsletter">
            <p>NEWSLETTER</p>
            @include('partials.newsletter')
        </div>
    </ul>
    <div class="cell small-10 align-self-bottom">
        <footer id="nav-footer" class="grid-x align-justified">
            <div class="cell small-4 ">
                <!--Twitter -->
                <span class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                    <a target="_default" href="http://twitter.com/__atthetable__" class="">
                        <img src="/img/social/twitter.svg" />
                    </a>
                 </span>
            </div>
            <div class="cell small-4">
                <!-- Facebook -->
                <span class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                    <a href="https://www.facebook.com/joinusatthetable/" class="">
                    <img src="/img/social/facebook.svg" />
                    </a>
                </span>
            </div>
            <div class="cell small-4">
                <!-- insta -->
                <span class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                    <a target="_default" href="http://instagram.com/__atthetable__" class="">
                    <img src="/img/social/instagram.svg" />
                    </a>
                </span>
            </div>
        </footer>
    </div>
</nav>
