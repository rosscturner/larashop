<div class="editorial-wrapper">
    <div class="grid-container">
        <div class="grid-x small-12 text-center editorial-footer ">
            <h3 class="cell read-more-header">READ MORE</h3>
        </div>
        <div class="grid-x small-up-2 medium-up-2 large-up-4 align-center text-center grid-padding-x readmore">
            @foreach($readmore as $post)
            <a href="/editorial/{{$post->slug}}" class="cell readmore-container header-style-{{$loop->iteration}} grid-x align-center"> 
                @if($post->teaser === null || $post->teaser == '')   
                <span data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="readmore-image lazyload flex-container align-center"></span>
                @else
                <span class="teaser-image texture  texture-{{$loop->iteration}} flex-container  align-center">
                    <p>{{$post->teaser}}</p>
                </span>
                @endif
                <div class="grid-x flex-column-dir align-top text-center">
                    <div class="align-self-top">
                        <h3 class=" flex-child-shrink">{{$post->category->name}}</h3>
                    </div>
                    <h2 class="cell text-center">{{$post->title}}</h2>
                    <p class="cell align-self-bottom">{{$post->excerpt}}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
</div> 