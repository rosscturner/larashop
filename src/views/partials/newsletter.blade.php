
<div class="stay-informed">
   <form class="newletter-form">
        <div class="input-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input class="input-group-field email-input" type="text" name="email"  placeholder="Your Email">
            <p  class="valid-mail email-message">SIGNED UP!</p>
            <p  class="invalid-mail email-message">INVALID E-MAIL</p>
            <div class="input-group-button">
                <input type="submit" class="button" value="">
            </div>
        </div>
    </form>
</div>
