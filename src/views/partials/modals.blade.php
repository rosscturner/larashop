<div class="reveal modal-content" id="faq" data-reveal >
	<button class="close-button" data-close aria-label="Close modal" type="button">
	  <span aria-hidden="true">&times;</span>
	</button>
	<div class="grid-x grid-align-middle flex-dir-column">
		<h2 class="cell shrink">FAQ</h2>
		<div class="cell small-11 grid-x grid-align-middle">
			<h3 class="shrink">How do I contribute to the magazine and/or online editorial?</h3>
			<p>We have an established team but are always open to new collaborations. If you have a story to tell, please send your idea and portfolio to editorial@atthetable.co.uk.</p>
			<h3 class="shrink">I have a question regarding the shop and/or my order. Who do I contact?</h3>
			<p>Please email <a href="mailto:info@atthetable.co.uk">info@atthetable.co.uk</a> with your question and order number and we will contact you as soon as possible. </p>
			<h3 class="shrink">When will my order arrive </h3>
			<p>Unless otherwise stated, orders ship in 1-3 business days from England via Royal Mail for customers in the UK, and via Royal Air Mail for customers outside the UK. If you need a product by a specific date or want your order to be trackable, please email info@atthetable.co.uk before placing your order.</p>
			<h3 class="shrink">How do I find out when tickets go on sale for your events?</h3>
			<p>To be the first to find out about our events, sign up to our newsletter via this site. Subscribers receive the ticket link prior to anyone else. We then post on Twitter, Instagram and Facebook so keep an eye on your preferred platform for news.</p>
			<h3 class="shrink">Do you work with brands?</h3>
			<p>Quality content is at the centre of everything we do. We work regularly with brands, creating bespoke content and helping to grow engaged communities. If you’d like to work with us please email bespoke@atthetable.co.uk.</p>
			<h3 class="shrink">What content management system do you use?</h3>
			<p>We have a dedicated in-house team and have built our own platform. Do get in touch if you are looking for bespoke web development services.</p>
		</div>
	</div>
</div>

<div class="reveal modal-content" id="terms" data-reveal >
	<button class="close-button" data-close aria-label="Close modal" type="button">
	  <span aria-hidden="true">&times;</span>
	</button>
	<div class="grid-x grid-align-middle flex-dir-column">
		<h2>TERMS &amp; CONDITIONS</h2>
		<div class="cell small-11 grid-x grid-align-middle">
			<p>Thank you for shopping with At The Table Ltd. Please familiarise yourself with the below before placing an order.</p>


			<h3 class="cell shrink">PAYMENT</h3>
			<p>We are currently accepting payment via debit and credit card. To use a debit or credit card, simply enter your card information when prompted, and your account will be securely charged.</p>


			<h3 class="cell shrink">SHIPPING</h3>
			<p>Unless otherwise stated, orders ship in 1-3 business days from England via Royal Mail for customers in the UK, and via Royal Air Mail for customers outside the UK. If you need a product by a specific date or want your order to be trackable, please email info@atthetable.co.uk before placing your order.
			</p>


			<h3 class="cell shrink">RETURNS</h3>
			<p>Tickets to our events and talks are non-refundable. However, you are welcome to transfer tickets to friends and family if you are unable to attend an event. Simply notify us on info@atthetable.co.uk. At this time, we also do not offer refunds or exchanges for products purchased via our website. If the product gets damaged in the post, please contact us at info@atthetable.co.uk with images of the product and packaging.
</p>

			<h3 class="cell shrink">COPYRIGHT & USAGE</h3>
			<p>You are welcome to use our product photos on your blog or site as needed. If you do, please link back to our homepage or the product page. Photos may not be used for any other purpose and may not be altered. All content on this website is copyrighted. High resolution product photos are available upon request. </p>


			<h3 class="cell">CANCELLATIONS</h3>
			<p>We will try our best to fulfil every order as specified in our terms, but delays and cancellations may occur for reasons beyond our control. We reserve the right to cancel any orders for any reason, refunding our customers for the full amount immediately.
			</p>

			<h3 class="cell">ASSISTANCE</h3>
			<p>For any further assistance, please get in touch at <a href="mailto:info@atthetable.co.uk">info@atthetable.co.uk</a></p>

			<hr/>
			The At The Table website, At The Table Ltd and material relating to information, products and services (or to third party information, products and services), is provided ‘as is’ without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.   We do not warrant that the functions contained in the material contained in this site will be uninterrupted or error free, that defects will be corrected, or that this site or the server that make it available are free of viruses or represent the full functionality, accuracy, reliability of the materials.   In no event will we be liable for any loss or damage including, without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from use or loss of use of, data, or profits, arising out of or in connection with the use of the At The Table Ltd t/a At The Table website.   These Terms and Conditions shall be governed by and construed in accordance with the laws of England and Wales. Any dispute arising under these Terms and Conditions shall be subject to the exclusive jurisdiction of the courts of England and Wales   At The Table Ltd is registered in England and Wales Reg No 09899566.  Registered Offices: 16 Nickleby House, George Row, London SE16 4UW.

		</div>
	</div>
</div>

<div class="reveal modal-content" id="privacy" data-reveal >
	<button class="close-button" data-close aria-label="Close modal" type="button">
	  <span aria-hidden="true">&times;</span>
	</button>
	<div class="grid-x grid-align-middle flex-dir-column">
		<h2 class="cell">PRIVACY POLICY</h2>
		<p class="cell"> At The Table is committed to protecting your privacy. We will use the information that we collect about you in accordance with the Data Protection Act 1998 and the Privacy and Electronic Communications Regulations 2003. Our use of your personal data will always have a lawful basis, either because it is necessary to complete a booking or purchase, because you have consented to our use of your personal data (e.g. by subscribing to emails), or because it is in our legitimate interests. We will only share your information with companies if necessary to deliver services on our behalf. &nbsp;For example third-party payment processors, and other third parties to provide our sites and fulfil your requests, and as otherwise consented to by you or as permitted by applicable law.&nbsp;<br><br>What information do we collect?&nbsp;<br><br>You give us your information when you book an event, make a purchase from our shop or sign up to our newsletter. 
		 We use Stripe to process card transactions and do not store credit card details. &nbsp;&nbsp;<br><br>We keep a record of the emails we send you, and we may track whether you receive or open them so we can make sure we are sending you the most relevant information. We may then track any subsequent actions online.&nbsp;<br><br>Like most websites, we receive and store certain details whenever you use the At The Table website. &nbsp;We use “cookies” to help us make our site – and the way you might use it – better. There is more information on this below.&nbsp;
		<br>
		<br>
		What we use your personal information for&nbsp;
		<br>
		<br>
		When you provide us with personal information to book an event, complete a transaction, verify your credit card or place an order, we hold that information under legitimate interest and you have the right to be informed that we hold it, the right to access that data, to correct it if it is erroneous, to be forgotten, to restrict processing of that data, and to object to our processing of your data. You also have other rights under the GDPR, which you can find out about&nbsp;here&nbsp;<br><br>We aim to be clear when we collect your data and not to do anything you wouldn’t reasonably expect. If you make a purchase or book an event we usually collect your name and contact details and your bank or credit card information (if making a transaction via Stripe). &nbsp; &nbsp;&nbsp;<br><br>We will include opt-out instructions in any marketing communications you receive from us.&nbsp;<br><br>Payment<br><br>We use Stripe to process payments.
		&nbsp;Stripe adheres to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover.&nbsp;<br><br>PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers.&nbsp;<br><br>
		Third-party services&nbsp;In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.&nbsp;<br><br>However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.&nbsp;<br><br>For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.&nbsp;<br><br>In particular, remember that certain providers may be located in or have facilities that are located in a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.&nbsp;<br><br>Once you leave our store’s website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service.&nbsp;<br><br>Security&nbsp;<br><br>To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.&nbsp;<br><br>If you provide us with your credit card information, the information is encrypted using secure socket layer technology (SSL) and stored with a AES-256 encryption. &nbsp;Although no method of transmission over the Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement additional generally accepted industry standards.&nbsp;<br><br>Cookies<br><br>We use cookies on this website to provide you with a better user experience. We do this by placing a small text file on your device / computer hard drive to track how you use the website, to record or log whether you have seen particular messages that we display, to keep you logged into the website where applicable, to display relevant adverts or content, referred you to a third-party website.
		<br>
		<br>
		Some cookies are required to enjoy and use the full functionality of this website.
		We use a cookie control system which allows you to accept the use of cookies, and control which cookies are saved to your device/computer. Some cookies will be saved for specific time periods, where others may last indefinitely. Your web browser should 
		see your web browser options.
		<br>
		<br>
		Cookies that we use are;
		<br><br>
		XSRF-TOKEN
		<br><br>
		This cookie is used to prevent certain types of attacks known as ‘cross-site request forgery’ and also prevents other malicious users from hijacking or imitating a genuine users session.
		<br><br>
		atthetable_session
		<br><br>
		This cookie is set on any page when a visitor arrives. we use this to enable certain functionality when you visit our site such as our shopping cart and checkout process.
		<br><br>
		Stripe
		__stripe_mid
		__stripe_sid 
		<br><br>
		Stripe uses these cookies to remember who you are and to enable At The Table to process payments without storing any credit card information on its own servers.
		<br><br>
		Google Analytics
		_ga
		_gid
		_gat
		<br><br>
		These cookies are set or updated on any page visited on the Verv website. See here for full details. Several cookies are set but all serve the same
		<br><br>
		Google Analytics&nbsp;Google Analytics is a third-party service that collects standard internet log information about our website visitors. This includes number of visitors to a specific page, whether they are accessing the page via laptop or mobile, and general demographic data. Google Analytics paints a useful picture of who visits our website and how people find out about us online. We do not use Google Analytics to identify anyone.&nbsp;<br><br>Social Media <br><br>We use social media to broadcast messages and updates about events and news. On occasion we may reply to comments or questions you make to us on social media platforms. Depending on your settings or the privacy policies social media and messaging services like Facebook, Instagram or Twitter, you might give the third party permission to access information from those accounts or services.&nbsp;&nbsp;<br><br>Mailing Lists&nbsp;<br><br>You can unsubscribe from our mailing list at any time by clicking the unsubscribe link at the bottom of the email, or by contacting us at info@atthetable.co.uk.&nbsp;<br><br>Changes to this Privacy Policy&nbsp;<br><br>We reserve the right to modify this privacy policy at any time. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.&nbsp;<br><br>Your Rights: <br><br>You have the following rights related to your personal data:
		<br>
		The right to request a copy of personal information held about you<br>
		The right to request that inaccuracies be corrected<br>
		The right to request us to stop processing your personal data<br>
		The right to withdraw consent<br>
		The right to lodge a complaint with the Information Commissioner's Office or Fundraising Regulator<br>
		<br><br>
		Questions and Contact Information&nbsp;  If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at&nbsp;info@atthetable.co.uk or by mail at At The Table, 16 Nickleby House, George Row, London, SE16 4UW.
		<br><br>
		</p>
	</div>
</div>
