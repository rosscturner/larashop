@extends('layouts.app')  @section('pageType', 'Editorial |') @section('content')
@php
$texCount = 1
@endphp
<div class="light">
    <div class="grid-container">
		<div class="row-section grid-x grid-padding-x small-up-1 medium-up-3 product-row">
			@foreach($recent as $post)
			<a href="/editorial/{{$post->slug}}" class="cell">
				<div class="product-container">
				@if($post->teaser === null || $post->teaser == '')
					<div class="flex-container align-center row-image-{{($loop->iteration)}}">
					@if($loop->iteration % 2 == 0)
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/portrait-index.jpg')}}" class="lazyload teaser-image" ></div>
						@else
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/portrait-index.jpg')}}" class="lazyload teaser-image desktop-teaser"></div>
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser"></div>
					@endif
					</div>
				@else
					<div class="teaser-image texture texture-{{$texCount}}">
						<p>{{$post->teaser}}</p>
					</div>
					@php $texCount++; @endphp
				@endif
					<div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
						<h3 class="flex-child-shrink">{{$post->category->name}}</h3>
						<h2 class="cell">{{$post->title}}</h2>
						<p class="cell">{{$post->excerpt}}</p>
					</div>
				</div>
			</a>
			@endforeach
		</div>
	</div>
</div>
@include('pages.partials.newsletter')
<div class="light">
    <div class="grid-container">
		<div class="row-section grid-x grid-padding-x small-up-1 medium-up-3 product-row">
			@foreach($posts->splice(0,3) as $post)
			<a href="/editorial/{{$post->slug}}" class="cell">
				<div class="product-container">
				@if($post->teaser === null || $post->teaser == '')
					<div class="flex-container align-center row-image-{{($loop->iteration)}}">
					@if($loop->iteration % 2 == 0)
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/portrait-index.jpg')}}" class="lazyload teaser-image portrait-teaser"></div>
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser"></div>
						@else
						<div data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="lazyload teaser-image landscape-teaser"></div>
						
					@endif
					</div>
				@else
					<div class="teaser-image texture texture-{{$texCount}}">
						<p>{{$post->teaser}}</p>
					</div>
					@php $texCount++; @endphp
				@endif
					<div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
						<h3 class="flex-child-shrink">{{$post->category->name}}</h3>
						<h2 class="cell">{{$post->title}}</h2>
						<p class="cell">{{$post->excerpt}}</p>
					</div>
				</div>
			</a>
			@endforeach
		</div>
	</div>
</div>
@include('pages.partials.ad')

@php $count = 1; @endphp
@foreach($posts as $post)
		@if($count == 1)
		<div class="light">
			<div class="grid-container">
				<div class="row-section grid-x grid-padding-x small-up-1 medium-up-3 product-row">
		@endif
					<a href="/editorial/{{$post->slug}}" class="cell">
						<div class="product-container">
						@if($post->teaser === null || $post->teaser == '')
							<div class="flex-container align-center row-image-{{($count)}}">
							@if($count % 2 == 0)
								<div data-bg="{{\Storage::url('editorial/' . $post->id . '/portrait-index.jpg')}}" class="lazyload teaser-image desktop-teaser"></div>
								<div data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser"></div>
								@else
								<div data-bg="{{\Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" class="lazyload teaser-image landscape-teaser"></div>
							@endif
							</div>
						@else
							<div class="teaser-image texture texture-{{$texCount}}">
								<p>{{$post->teaser}}</p>
							</div>
							@php $texCount++; @endphp
						@endif
							<div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($count)}}">
								<h3 class="flex-child-shrink">{{$post->category->name}}</h3>
								<h2 class="cell">{{$post->title}}</h2>
								<p class="cell">{{$post->excerpt}}</p>
							</div>
						</div>
					</a>
		@if($count == 3)
				</div>
			</div>
		</div>
		@endif
		@php
		if($count == 3) {
			$count = 0;
		}
		$count++
		@endphp
@endforeach
@if($count !== 0)
	</div>
	</div>
	</div>
@endif
@endsection @section('scripts') @endsection
