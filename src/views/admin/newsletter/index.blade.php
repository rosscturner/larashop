@extends('layouts.dashboard')

@section('content')
<div class="small-12 small-centered end column">
  <fieldset class="edit-bar">
    <h2 class="small-9 column text-left"> ALL SIGNUPS</h2>
  </fieldset>
  <ul class="tabs" data-tabs role="tablist" id="example-tabs">
    <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">All SIGNUPS</a></li>
  </ul>
  <div class="tabs-content" data-tabs-content="example-tabs">
      <div class="tabs-panel is-active" id="panel1">
        <table>
          <thead>
            <tr>
              <th width="200">Email</th>
              <th>Stock</th>
              <th>Price</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          @foreach($products as $product)
            <tr>
              <td>{{$product->name}}</td>
              <td>{{$product->stock}}</td>
              <td>&pound;{{$product->price}}</td>
              <td><a class="button" href="/product/{{$product->id}}">View</a></td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
  </div>
</div>
@endsection
