@extends('layouts.dashboard')

@section('content')
<div class="small-12 small-centered end column">
  <fieldset class="edit-bar">
    <h2 class="small-9 column text-left"> ALL PRODUCTS</h2>
    <div class="small-2 column">
      <a  href="/admin/product/create" class="button primary float-right">CREATE PRODUCT</a>
      <input type="submit" id="submit" class="show-for-sr">
    </div>
  </fieldset>
  <ul class="tabs" data-tabs role="tablist" id="example-tabs">
    <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">All PRODUCTS</a></li>
  </ul>
  <div class="tabs-content" data-tabs-content="example-tabs">
      <div class="tabs-panel is-active" id="panel1">
        <table>
          <thead>
            <tr>
              <th width="200">Name</th>
              <th>Stock</th>
              <th>Price</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          @foreach($products as $product)
            <tr>
              <td>{{$product->name}}</td>
              <td>{{$product->stock}}</td>
              <td>&pound;{{$product->price}}</td>
              <td><a class="button" href="/product/{{$product->id}}">View</a></td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
  </div>
</div>
@endsection
