@extends('layouts.dashboard')
@section('content')
<div class="container">
    @if($errors->has(''))
    <div class="row">
    <div class="small-11 small-centered column">
   @foreach ($errors->all() as $error)
   <div class="alert callout" data-closable>
  		{{$error}}
	  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
  @endforeach
  </div>
  </div>
@endif
@include('admin.blog._form')
</div>
@endsection

@section('scripts')
<script type="text/javascript">
var redacted = false;
$(function()
{
    var sectionCount = 2;
	 document.getElementById("landscape-image").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.total > 250000) {
                $('#imageerror').text('Image too large');
                $jimage = $("#landscape-image");
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedlandscape').removeAttr('src');
                return;
            }
            $('#imageerror').text('');
            document.getElementById("uploadedlandscape").src = e.target.result; 
        };
        reader.readAsDataURL(this.files[0]);
    };

     document.getElementById("portrait-image").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.total > 250000) {
                
                $('#imageerror').text('Image too large');
                $jimage = $("#portrait-image");
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedportrait').removeAttr('src');
                return;
            }
            $('#imageerror').text('');
            document.getElementById("uploadedportrait").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    };

	$('input[name="title"]').on('keyup', function() {
		var val = $(this).val();
		var text = slugify(val);
    	$('input[name="slug"]').val(text);
    	$('h2').text(val);
	});

     $('#add-section').on('click',function(eve){
        eve.preventDefault();
        $('.accordion').append( `
            <li  class="accordion-item" data-accordion-item>
                <a href="#" class="accordion-title">Section ${sectionCount}  <span id="section-${sectionCount}-errors"></span></a>
                <div class="accordion-content" data-tab-content data-editable data-name="content[${sectionCount}][content]" class="small-12 column editor">
                        <input type="hidden" name="content[${sectionCount}][order]" value="${sectionCount}" />
                        <!-- type text -->
                        <input 	data-parsley-errors-container="section-${sectionCount}-errors" required
                                value="text"
                                name="content[${sectionCount}][type]"
                                type="radio"
                                {{isset($section) && $section->type=='text' ? 'checked=checked' : '' }}>
                                <label>Text</label>
                        <!-- type pull-out -->
                        <input 	data-parsley-errors-container="section-${sectionCount}-errors" required
                                value="pull-out"
                                name="content[${sectionCount}][type]"
                                type="radio"
                            {{isset($section) && $section->type=='pull-out' ? 'checked=checked' : '' }}>
                            <label>Pull Out</label>
                            <span class="remove-section button float-right" data-section="${sectionCount}">Delete Section</span>
                            <div class="clearfix"></div>
                        <textarea class='form-control section-${sectionCount}' placeholder="section-content" data-parsley-errors-container="#content-errors" required  name="content[${sectionCount}][content]" >{{isset($section->content) ? $section->content : old('section[${sectionCount}]') }}</textarea>
                </div>
            </li>`);

            setTimeout((function(sectionCount){ 
                reinit(sectionCount);
             }).bind(null, sectionCount), 50);
             sectionCount = sectionCount + 1;
          
        });

    $('.remove-section').on('click',function(eve){
        eve.preventDefault();
        var sectionNo = $(this).data('section');
        $('#section-' + sectionNo).remove();
    });

	$('#blog-post').parsley({
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="form-error"></span>'
    });

     function reinit(count)
    {
        Foundation.reInit($('.accordion'));
        $('.section-' + count).redactor({
            imageUpload: '/admin/blog/upload?_token=' + '{{ csrf_token() }}',
            imageManagerJson: '/admin/blog/images?_token=' + '{{ csrf_token() }}',
            plugins: ['imagemanager','fullscreen'],
            imageResizable: true,
            imagePosition: true,
            minHeight: 300,
        });
    }

    
    $('.accordion').on('down.zf.accordion',function(){
        if(!redacted){
            $('.section').redactor({
                imageUpload: '/admin/blog/upload?_token=' + '{{ csrf_token() }}',
                imageManagerJson: '/admin/blog/images?_token=' + '{{ csrf_token() }}',
                plugins: ['imagemanager','fullscreen'],
                imageResizable: true,
                imagePosition: true,
                minHeight: 300,
            });
            redacted = true;
        }
    });

   

    function slugify(text)
    {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }
});
</script>

@endsection('endsection')

