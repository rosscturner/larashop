@if(isset($post))
<form action="/admin/blog/{{$post->id}}" method="POST" id="blog-post" enctype='multipart/form-data'>
<input type="hidden" name="_method" value="PUT">
@else
<form action="/admin/blog" method="POST" id="blog-post" enctype='multipart/form-data'>
@endif
			<div class="row">
				<div class="small-12 column">
					<fieldset class="edit-bar">
						<h2 class="small-9 column text-left">{{ isset($post->title) ? $post->title : old('title') }}</h2>
						<div class="small-2 column">
							@if(isset($post))
								<a  class="button" href="/editorial/{{$post->slug}}" target="_default">Preview</a>
								<input type="button" id="cancel" class="show-for-sr">
							@endif

							<label for="submit" class="button primary float-right">Save</label>
							<input type="submit" id="submit" class="show-for-sr">
						 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						</div>
					</fieldset>
				</div>
			</div>
			<div class="row">
			<div class="small-8  column">
				<fieldset>
		    		<!-- Header and slug -->
		    		<div class="row">
						<div data-editable data-name=heading class="small-6 column">
							<label>Title<span> * </span><span id="title-errors"></span>
					       		<input type="text" name="title" data-parsley-errors-container="#title-errors" required placeholder="title" value="{{isset($post->title) ? $post->title : old('title') }}" />
					      	</label>
						</div>
						<div data-editable data-name=heading class="small-6 column">
							<label>Slug<span> * </span><span id="slug-errors"></span>
						    	<input readonly data-parsley-errors-container="#slug-errors" required type="text" name="slug" placeholder="slug" value="{{isset($post->slug) ? $post->slug : old('slug') }}" />
						    </label>
						</div>
					</div>

					<!-- sections -->
					<label>CONTENT*</label>
					<ul class="accordion" data-allow-all-closed="true" data-accordion>
						@if(isset($post))
							@foreach($post->sections as $section)
							<li  class="accordion-item" data-accordion-item id="section-{{$section->id}}">
								<a href="#" class="accordion-title">Section {{$section->order}}  <span id="section-{{$section->id}}-errors"></span></a>
								<div class="accordion-content" data-tab-content data-editable data-name="content[{{$section->id}}][content]" class="small-12 column editor">
									
										<input type="hidden" name="content[{{$section->id}}][id]" value='{{ $section->id }}' />
										<input type="hidden" name="content[{{$section->id}}][order]" value='{{ $section->order }}' />
										<!-- type text -->
										@if($section->order == 1)
										<input 	data-parsley-errors-container="section-{{$section->id}}-errors" required
												value="header"
												name="content[{{$section->id}}][type]"
												type="radio"
												{{isset($section) && $section->type=='header' ? 'checked=checked' : '' }}>
												<label>Header</label>
										@endif
										<!-- type text -->
										<input 	data-parsley-errors-container="section-{{$section->id}}-errors" required
												value="text"
												name="content[{{$section->id}}][type]"
												type="radio"
												{{isset($section) && $section->type=='text' ? 'checked=checked' : '' }}>
												<label>Text</label>
										<!-- type pull-out -->
										<input 	data-parsley-errors-container="section-{{$section->id}}-errors" required
												value="pull-out"
												name="content[{{$section->id}}][type]"
												type="radio"
											{{isset($section) && $section->type=='pull-out' ? 'checked=checked' : '' }}>
											<label>Pull Out</label>
											@if($section->order != 1)
											<span class="remove-section button float-right" data-section="{{$section->id}}">Delete Section</span>
											@endif
											<div class="clearfix"></div>
											<textarea class='form-control section' placeholder="section-content" data-parsley-errors-container="#content-errors" required  data-redactor name="content[{{$section->id}}][content]" >{{isset($section->content) ? $section->content : old('section[$section->id]') }}</textarea>
									
								</div>
							</li>
							@endforeach
						@else
							<li  class="accordion-item" data-accordion-item>
								<a href="#" class="accordion-title">Section 1  <span id="section-1-errors"></span></a>
								<div class="accordion-content" data-tab-content data-editable data-name="content[1][content]" class="small-12 column editor">
										
										<input type="hidden" name="content[1][order]" value='1' />
										<!-- type text -->
										<input 	data-parsley-errors-container="section-1-errors" required
												value="header"
												name="content[1][type]"
												checked=checked
												type="radio"
												{{isset($section) && $section->type=='header' ? 'checked=checked' : 'checked=checked' }}>
												<label>Header</label>
									
											<textarea class='form-control section' placeholder="section-content" data-redactor data-parsley-errors-container="#content-errors" required  name="content[1][content]" >{{isset($section->content) ? $section->content : old('section[1]') }}</textarea>
									
								</div>
							</li>
						@endif
						
					</ul>
					<a href="#" class="button primary expanded" id="add-section">Add Section</a>
					
					<!-- excerpt -->
					<div class="row">
						<div class="small-12 column">
							<label>Excerpt<span> * </span><span id="excerpt-errors"></span>
								<textarea minlength="10"data-parsley-errors-container="#excerpt-errors" required  class='form-control ' placeholder="excerpt" name="excerpt"  >{{isset($post->excerpt) ? $post->excerpt : old('excerpt') }}</textarea>
							</label>
						</div>
					</div>

					<!-- Teaser -->
					<div class="row">
						<div class="small-12 column">
							<label>Teaser<span> </span><span id="teaser-errors"></span>
								<textarea minlength="10" data-parsley-errors-container="#teaser-errors"  class='form-control ' placeholder="teaser" name="teaser"  >{{isset($post->teaser) ? $post->teaser : old('teaser') }}</textarea>
							</label>
						</div>
					</div>

					<!-- Author -->
					<div class="row">
						<div class="small-12 medium-6 column">
							<label>Author<span> * </span><span id="excerpt-errors"></span>
								<select name="author_id">
									@foreach($authors as $author)
										<option {{isset($post) && $post->author_id==$author->id ? 'selected=selected' : '' }} value="{{$author->id}}">{{ $author->firstname . ' ' . $author->lastname }}</option>
									@endforeach
								</select>
							</label>
						</div>
						<!-- Category -->
						<div class="small-12 medium-6 column">
							<label>Category<span> * </span><span id="excerpt-errors"></span>
								<select name="category_id">
									@foreach($categories as $category)
										<option
										{{isset($post) && $post->category_id==$category->id ? 'selected=selected' : '' }}
										value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</label>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="small-4 column">
				<fieldset>
					<!--settings -->
					<div class="small-12">
						<fieldset class="fieldset">
							<legend>Visibility<span> * </span><span id="settings-errors"></span> </legend>
							@foreach($visibility as $state)
								<input
								data-parsley-errors-container="#settings-errors" required
								value="{{$state->id}}"
								name="visibility_id"
								type="radio"

								{{isset($post) && $post->visibility_id==$state->id ? 'checked=checked' : '' }}
								><label for="visibililty">{{$state->name}}</label>
							@endforeach
	   					</fieldset>
					</div>
					<div class="small-12">
						<fieldset class="fieldset">
							<legend>Status<span> * </span><span id="status-errors"></span> </legend>
							@foreach($statuses as $status)
								<input
								data-parsley-errors-container="#status-errors" required
								value="{{$status->id}}"
								name="status_id"
								type="radio"
								{{isset($post) && $post->status_id==$status->id ? 'checked=checked' : '' }}>
								<label for="status">{{$status->name}}</label>
							@endforeach
	   					</fieldset>
					</div>
	   			</fieldset>

	   			<fieldset>
				<!-- portrait image -->
					<div class="small-12 ">
						<fieldset class="fieldset">
							<legend>Portrait Image<span> * </span><span class="form-error" id="imageerror"></span> </legend>
							<input type="hidden" name="MAX_UPLOAD_SIZE" value="250000">
							<input type="file" name="portrait-image" id="portrait-image" accept="image/jpeg" {{ isset($post) ? '': '' }}>
							
							@if(isset($post))
								<img id="uploadedportrait" width="100%;height:auto;"  src="{{Storage::url('editorial/' . $post->id . '/portrait-index.jpg')}}" />
            				@else
            					<img id="uploadedportrait" width="100%;height:auto;" />
            				@endif
        				</fieldset>
					</div>
					

				<!-- landscape image -->
				<div class="small-12 ">
						<fieldset class="fieldset">
							<legend>Landscape Image<span> * </span><span class="form-error" id="imageerror"></span> </legend>
							<input type="hidden" name="MAX_UPLOAD_SIZE" value="250000">
							<input type="file" name="landscape-image" id="landscape-image" accept="image/jpeg" {{ isset($post) ? '': '' }}>
							@if(isset($post))
								<img id="uploadedlandscape" width="100%;height:auto;"  src="{{Storage::url('editorial/' . $post->id . '/landscape-index.jpg')}}" />
            				@else
            					<img id="uploadedlandscape" width="100%;height:auto;" />
            				@endif
        				</fieldset>
					</div>
</fieldset>
				<!-- datepicker -->
					<div class="small-12">
						<div class="date" id="my_date_filed">
							<div class="input-group">
								<label>Publish Date<span> * </span><span id="publish-errors"></span>
									<span class="postfix input-group-label"><i class="fa fa-calendar fa-lg "></i></span>
									<input data-parsley-errors-container="#publish-errors" required class="input-group-field" type="date" name="publish_date" value="{{isset($post->publish_date) ? date('Y-m-d',strtotime($post->publish_date)) : old('publish_date') }}"  />
								</label>
							</div>
						</div>
					</div>
				</fieldset>
   			</div>
   		</div>

		<div class="row edit-bar">
			<div class="small-12">
				<hr />
				<div class="float-left">
					<label for="cancel" class="button">
					<a id="cancel" href="/admin/blog">Cancel</a>
				</label>
				</div>
				<div class="float-right">
					<label for="submit" class="button primary">Save</label>
					<input type="submit" id="submit" class="show-for-sr">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</div>
			</div>
		</div>
	</form>
