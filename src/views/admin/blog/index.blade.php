@extends('layouts.dashboard')

@section('content')

@if(\Session::has('success'))
<div class="row">
	<div class="small-12">
		<div class="success callout" data-closable="slide-out-right">	<strong>{{\Session::get('success')}}</strong>
			<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	</div>
</div>
	@endif

<div class="small-12 small-centered end column">
	<fieldset class="edit-bar">
		<h2 class="small-9 column text-left"> ALL POSTS</h2>
		<div class="small-2 column">
			<a  href="/admin/blog/create" class="button primary float-right">CREATE POST</a>
			<input type="submit" id="submit" class="show-for-sr">
		</div>
	</fieldset>
	<ul class="tabs" data-tabs role="tablist" id="example-tabs">
		<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">All Posts</a></li>
	</ul>
		<div class="tabs-content" data-tabs-content="example-tabs">
				<div class="tabs-panel is-active" id="panel1">
					<table class="hover">
						<thead>
							<tr>
								<th width="200">Title</th>
								<th>Slug</th>
								<th width="150">Author</th>
								<th>Status</th>
								<th>Published</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $post)
								<tr>
									<td>{{$post->title}}</td>
									<td>{{$post->slug}}</td>
									<td>{{$post->author->firstname}} {{$post->author->lastname}}</td>
									<td>{{$post->status->name}}</td>
									<td>{{  $post->publish_date->format('d M Y') }}</td>
									<td>
										<a class="button" href="/admin/blog/{{ $post->id }}">EDIT</a>
										<form action="{{ route('blog.destroy', $post->id) }}" method="POST">
											{{ method_field('DELETE') }}
											{{ csrf_field() }}
											<button class="alert button">Delete</button>
									</form>
									</td>
									
								</tr>
								@endforeach

							<tr>
							<td colspan="6">
										<a class="button expanded" href=" {{ url('admin/blog/create') }}"> CREATE POST</a>
									</td>	
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tabs-panel" id="panel2">
					<p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
				</div>
			</div>
</div>
				
@endsection
