@extends('layouts.dashboard')

@section('content')
<div class="small-12 small-centered end column">
	<fieldset class="edit-bar">
		<h2 class="small-9 column text-left"> ALL EVENTS</h2>
		<div class="small-2 column">
			<a  href="/admin/events/create" class="button primary float-right">CREATE EVENT</a>
			<input type="submit" id="submit" class="show-for-sr">
		</div>
	</fieldset>
	<ul class="tabs" data-tabs role="tablist" id="example-tabs">
		<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">All Events</a></li>
	</ul>
		<div class="tabs-content" data-tabs-content="example-tabs">
				<div class="tabs-panel is-active" id="panel1">
          <table>
            <thead>
              <tr>
                <th>Event id</th>
                <th>Name</th>
                <th>URL</th>
                <th>Stock</th>
                <th>Event Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
              <tr>
                <td>{{$event->product_id}}</td>
                <td>{{$event->name}}</td>
                <td>{{$event->slug}}</td>
                <td>{{$event->stock}}</td>
                <td>{{$event->start_time}}</td>
                <td><a class="button" href="/admin/events/{{$event->product_id}}">View</a></td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection