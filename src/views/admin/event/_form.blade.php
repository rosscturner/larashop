@if(isset($event))
<form action="/admin/events/{{$event->id}}" method="POST" id="events-post" enctype='multipart/form-data'>
<input type="hidden" name="_method" value="PUT">
@else
<form action="/admin/events" method="POST" id="events-post" enctype='multipart/form-data'>
@endif
			<div class="row">
				<div class="small-12 column">
					<fieldset class="edit-bar">
						<h2 class="small-9 column text-left">{{ isset($event->name) ? $event->name : old('name') }}</h2>
						<div class="small-2 column">
							@if(isset($event))
								<a  class="button" href="/event/{{$event->slug}}" target="_default">Preview</a>
								<input type="button" id="cancel" class="show-for-sr">
							@endif

							<label for="submit" class="button primary float-right">Save</label>
							<input type="submit" id="submit" class="show-for-sr">
						 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						</div>
					</fieldset>
				</div>
			</div>
			<div class="row">
			<div class="small-8  column">
				<fieldset>
		    		<!-- Header and slug -->
		    		<div class="row">
						<div data-editable data-name=heading class="small-6 column">
							<label>Title<span> * </span><span id="title-errors"></span>
					       		<input type="text" name="title" data-parsley-errors-container="#title-errors" required placeholder="title" value="{{isset($event->name) ? $event->name : old('name') }}" />
					      	</label>
						</div>
						<div data-editable data-name=heading class="small-6 column">
							<label>Slug<span> * </span><span id="slug-errors"></span>
						    	<input readonly data-parsley-errors-container="#slug-errors" required type="text" name="slug" placeholder="slug" value="{{isset($event->slug) ? $event->slug : old('slug') }}" />
						    </label>
						</div>
					</div>

					<!-- sections -->
                    <div class="row">
                        <div class="small-12 column editor">
                                <label>CONTENT*</label>
                                <textarea class='form-control redactor' placeholder="Event Description" data-parsley-errors-container="#description-errors" required  data-redactor name="description" >{{isset($event->description) ? $event->description : old('description') }}</textarea>
                        </div>
                    </div>	
                    <div class="row">
                        <div class="small-12 column editor">
                            <label>Sub Heading*</label>
                            <textarea class='form-control' placeholder="Sub Heading" data-parsley-errors-container="#sub_heading_errors" required  name="sub_heading" >{{isset($event->sub_heading) ? $event->sub_heading : old('sub_heading') }}</textarea>
                        </div>
                    </div>
				</div>
            </fieldset>
            <div class="small-4 column">
                    <fieldset>
                        <div class="small-12">
                            <div class="input-group">   
                                <label>No. of Tickets<span> * </span><span id="ticket-errors"></span>
                                    <input type="number" name="stock" data-parsley-errors-container="#ticket-errors" required placeholder="number" value="{{isset($event->stock) ? $event->stock : old('stock') }}" />
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="small-12">
                            <div class="input-group">   
                                <label>Price<span> * </span><span id="price-errors"></span>
                                    <input  type="number" min="0.01" step="0.01" name="price" data-parsley-errors-container="#price-errors" required placeholder="number" value="{{isset($event->price) ? $event->price : old('price') }}" />
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <!-- datepicker -->
                     <div class="small-12">
						<div class=>Start Time<span> * </span><span id="publish-errors"></div>
						<div class="date input-group" id="start_time">
							<label class="postfix input-group-label"><i class="fa fa-calendar fa-lg "></i></label>
							<input data-parsley-errors-container="#publish-errors" required class="input-group-field" type="datetime-local" name="start_time" value="{{isset($event->ticket->start_time) ? date('Y-m-d\TH:i:s', strtotime($event->ticket->start_time)) : old('start_time') }}"  />
							</div>
						</div>
                    
                     <!-- datepicker -->
                     <div class="small-12">
						<div class=>End Time<span> * </span><span id="publish-errors"></div>
						<div class="date input-group" id="end_time">
							<label class="postfix input-group-label"><i class="fa fa-calendar fa-lg "></i></label>
							<input data-parsley-errors-container="#publish-errors" required class="input-group-field" type="datetime-local" name="end_time" value="{{isset($event->ticket->end_time) ? date('Y-m-d\TH:i:s', strtotime($event->ticket->end_time)) : old('end_time') }}"  />
							</div>
						</div>

                    </fieldset>
                    
                <fieldset>
				    <!-- portrait image -->
					<div class="small-12 ">
						<fieldset class="fieldset">
							<legend>Portrait Image<span> * </span><span class="form-error" id="imageerror"></span> </legend>
							<input type="hidden" name="MAX_UPLOAD_SIZE" value="250000">
							<input type="file" name="portrait-image" id="portrait-image" accept="image/jpeg" {{ isset($event) ? '': '' }}>
							@if(isset($event))
								<img id="uploadedportrait" width="100%;height:auto;"  src="{{Storage::url('event/' . $event->id . '/portrait-index.jpg')}}" />
            				@else
            					<img id="uploadedportrait" width="100%;height:auto;" />
            				@endif
        				</fieldset>
					</div>
			

				<!-- landscape image -->
				    <div class="small-12 ">
						<fieldset class="fieldset">
							<legend>Landscape Image<span> * </span><span class="form-error" id="imageerror"></span> </legend>
							<input type="hidden" name="MAX_UPLOAD_SIZE" value="250000">
							<input type="file" name="landscape-image" id="landscape-image" accept="image/jpeg" {{ isset($event) ? '': '' }}>
							@if(isset($event))
								<img id="uploadedlandscape" width="100%;height:auto;"  src="{{Storage::url('event/' . $event->id . '/landscape-index.jpg')}}" />
            				@else
            					<img id="uploadedlandscape" width="100%;height:auto;" />
            				@endif
        				</fieldset>
					</div>

                </fieldset>
   			</div>
		</div>

			
   		</div>

		<div class="row edit-bar">
			<div class="small-12">
				<hr />
				<div class="float-left">
					<label for="cancel" class="button">
					<a id="cancel" href="/admin/blog">Cancel</a>
				</label>
				</div>
				<div class="float-right">
					<label for="submit" class="button primary">Save</label>
					<input type="submit" id="submit" class="show-for-sr">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</div>
			</div>
		</div>
	</form>
