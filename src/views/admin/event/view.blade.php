@extends('layouts.dashboard') @section('content')
<div class="row">
<div class="small-11 small-centered">
<table>
    <thead>
        <tr>
            <th width="200">Event id</th>
            <th>Name</th>
            <th>URL</th>
            <th>Start Time</th>
            <th>Tickets Sold</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->name}}</td>
            <td>{{$event->slug}}</td>
            <td>{{$event->ticket->start_time}}</td>
            <td>{{$sold}}</td>
        </tr>
    </tbody>
</table>
<h2>CUSTOMERS</h2>
<table>
    <thead>
        <tr>
            <th width="200">Name</th>
            <th>Email</th>
            <th>Stripe id</th>
            <th>Order id</th>
            <th>Created</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->firstname}} {{$user->lastname}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->stripe_id}}</td>
            <td>{{$user->order_id}}</td>
            <td>{{$user->created_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
@endsection
