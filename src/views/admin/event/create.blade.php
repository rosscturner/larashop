@extends('layouts.dashboard')
@section('content')
<div class="container">
   
    <div class="row">
        <div class="small-11 small-centered column">
            @foreach ($errors->all() as $error)
            <div class="alert callout" data-closable>
                    {{$error}}
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endforeach
        </div>
  </div>
@include('admin.event._form')
</div>
@endsection

@section('scripts')
<script type="text/javascript">
var redacted = false;
$(function()
{
    var sectionCount = 2;
    document.getElementById("landscape-image").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.total > 250000) {
                $('#imageerror').text('Image too large');
                $jimage = $("#landscape-image");
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedlandscape').removeAttr('src');
                return;
            }
            $('#imageerror').text('');
            document.getElementById("uploadedlandscape").src = e.target.result; 
        };
        reader.readAsDataURL(this.files[0]);
    };

     document.getElementById("portrait-image").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.total > 250000) {
                
                $('#imageerror').text('Image too large');
                $jimage = $("#portrait-image");
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedportrait').removeAttr('src');
                return;
            }
            $('#imageerror').text('');
            document.getElementById("uploadedportrait").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    };


	$('input[name="title"]').on('keyup', function() {
		var val = $(this).val();
		var text = slugify(val);
    	$('input[name="slug"]').val(text);
    	$('h2').text(val);
	});


	$('#events-post').parsley({
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="form-error"></span>'
    });

    $('.redactor').redactor({
                imageUpload: '/admin/events/upload?_token=' + '{{ csrf_token() }}',
                imageManagerJson: '/admin/events/images?_token=' + '{{ csrf_token() }}',
                plugins: ['imagemanager','fullscreen'],
                imageResizable: true,
                imagePosition: true,
                minHeight: 300,
            });


    function slugify(text)
    {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }
});
</script>

@endsection('endsection')

