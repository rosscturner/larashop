@extends('layouts.dashboard')
@section('content')
<div class="container">

	@if(\Session::has('success'))
		<div class="success callout" data-closable="slide-out-right">	<strong>{{\Session::get('success')}}</strong>
			<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    <span aria-hidden="true">&times;</span>
  </button>
		</div>
	@endif

    @if($errors->has(''))
    <div class="row">
    <div class="small-11 small-centered column">

   @foreach ($errors->all() as $error)
   <div class="alert callout" data-closable>
  		{{$error}}
	  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
  @endforeach
  </div>
  </div>
@endif
@include('admin.order._form')
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(function()
{

	//upload using filereader
    document.getElementById("jimage").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.total > 250000) {
                $('#imageerror').text('Image too large');
                $jimage = $("#jimage");
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedimage').removeAttr('src');
                return;
            }
            $('#imageerror').text('');
            document.getElementById("uploadedimage").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    };



	$('input[name="title"]').on('keyup', function() {
		var val = $(this).val();
		var text = slugify(val);
    	$('input[name="slug"]').val(text);
    	$('h2').text(val);
	});

    $('#content').redactor({
		imageUpload: '/admin/blog/upload?_token=' + '{{ csrf_token() }}',
        imageManagerJson: '/admin/blog/images?_token=' + '{{ csrf_token() }}',
        plugins: ['imagemanager','fullscreen'],
        imageResizable: true,
        imagePosition: true,
        minHeight: 300,
	});

	$('#blog-post').parsley({
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="form-error"></span>'
    });
});
</script>

@endsection('endsection')

