@extends('layouts.dashboard') @section('content')
<div class="small-12 small-centered end column">
@if(\Session::has('success'))
    <div class="row">
        <div class="success callout" data-closable="slide-out-right">   <strong>{{\Session::get('success')}}</strong>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif
<div class="row">
	<fieldset class="edit-bar">
		<h2 class="small-9 column text-left"> ALL ORDERS</h2>
	</fieldset>
    <ul class="tabs" data-tabs id="example-tabs">
            <li class="tabs-title is-active"><a href="#allOrders" aria-selected="true">All Orders</a></li>
            
        </li>
        <li class="tabs-title"><a href="#unfulfilled">Unfulfilled</a></li>
            <li class="tabs-title"><a href="#completed">Shipped</a></li>
    </ul>
        <div class="tabs-content" data-tabs-content="example-tabs">
        <!-- Completed -->
           <div class="tabs-panel is-active" id="allOrders">
                <table class="hover">
                    <thead>
                        <tr>
                            <th>Order #</th>
                            <th>No Item</th>
                            <th>Total</th>
                            <th>Order Received</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{count($order->items)}}</td>
                            <td>&pound;{{$order->price}}</td>
                            <td>{{$order->created_at}}</td>
                            <td><span class="callout small">{{$order->status}}</span></td>
                            <td><a class="button" href="/admin/orders/{{$order->id}}">View</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--unfulfilled -->
            <div class="tabs-panel" id="unfulfilled">
                <table>
                    <thead>
                        <tr>
                            <th>Order #</th>
                            <th>No Item</th>
                            <th>Total</th>
                            <th>Order Received</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unfulfilled as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{count($order->items)}}</td>
                            <td>&pound;{{$order->price}}</td>
                            <td>{{$order->created_at}}</td>
                            <td><span class="callout small">{{$order->status}}</span></td>
                            <td><a class="button" href="/admin/orders/{{$order->id}}">View</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

             <!--completed -->
            <div class="tabs-panel" id="completed">
                <table>
                    <thead>
                        <tr>
                            <th>Order #</th>
                            <th>No Item</th>
                            <th>Total</th>
                            <th>Order Received</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($shipped as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{count($order->items)}}</td>
                            <td>&pound;{{$order->price}}</td>
                            <td>{{$order->created_at}}</td>
                            <td><span class="callout small">{{$order->status}}</span></td>
                            <td><a class="button" href="/admin/orders/{{$order->id}}">View</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
