@if(isset($post))
<form action="/admin/blog/{{$post->id}}" method="POST" id="order" enctype='multipart/form-data'>
    <input type="hidden" name="_method" value="PUT"> @else
    <form action="/admin/blog" method="POST" id="order" enctype='multipart/form-data'>
        @endif
        <div class="row">
            <div class="small-12 column">
                <fieldset class="edit-bar">
                    <h2 class="small-9 column text-left">Order #{{ isset($order->id) ? $order->id : old('title') }}</h2>
                    <span class="button primary float-right">{{strtoupper($order->status)}}</span>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="small-8  column">
                <fieldset>
                    <table>
                        <tbody>
                            <?php $subtotal = 0.00; $shipping = 0.00; ?> 
                            @foreach($order->items as $item)
                            <tr class="order-item">
                            	@if($item->product->bespoke)
	                                <td><img src="{{\Storage::url('products/' . $item->product_item->id . '/index.jpg')}}" /></td>
	                                <td class="item-name">{{$item->product_item->name}}</td>
	                                <td>&pound;{{number_format($item->price,2)}} x {{$item->qty}}</td>
	                                <td>&pound;{{number_format($item->price * $item->qty,2)}}</td>
	                                <?php 	$subtotal += $item->price * $item->qty; 
	                                		$shipping += $item->shipping;
	                                ?>
	                            @elseif($item->product_item->ticket)
	                            	 <td><img src="{{\Storage::url('events/' . $item->product_item->id . '/index.jpg')}}" /></td>
	                                <td class="item-name">{{$item->product_item->name}}</td>
	                                <td>&pound;{{number_format($item->price,2)}} x {{$item->qty}}</td>
	                                <td>&pound;{{number_format($item->price * $item->qty,2)}}</td>
	                                <?php 	$subtotal += $item->price * $item->qty; 
	                                		$shipping += $item->shipping;
	                                ?>
	                             @else
	                            	 <td><img src="{{\Storage::url('magazine/cover.jpg')}}" /></td>
	                                <td class="item-name">{{$item->product_item->name}}</td>
	                                <td>&pound;{{number_format($item->price,2)}} x {{$item->qty}}</td>
	                                <td>&pound;{{number_format($item->price * $item->qty,2)}}</td>
	                                <?php 	$subtotal += $item->price * $item->qty; 
	                                		$shipping += $item->shipping;
	                                ?>
                                @endif
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Sub-Total:</td>
                                <td>&pound;{{number_format($subtotal,2)}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Shipping:</td>
                                <td>&pound;{{number_format($shipping,2)}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total:</td>
                                <td>&pound;{{number_format($subtotal + $shipping,2)}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr/>
                    @if($order->status == 'completed')
                    <p class="small-12 column">
                    	<div class="row">
	                    	<span class="small-4 column">Fulfill items</span>
	                    	<div class="small-4 column">
	                    		<a href="/admin/orders/fulfill/{{$order->id}}"class="primary button success float-right">Fulfill Items</a>

	                    	</div>
                    	</div>
                    </p>
                    @endif
                </fieldset>
            </div>
            <div class="small-4 column">
                <fieldset>
                    <div class="small-12">
                        <fieldset class="fieldset">
                            <legend><strong>Customer</strong></legend>
                            <p>{{$order->orderUser->firstname}} {{$order->orderUser->lastname}}</p>
                            <p>{{$order->orderUser->email}}</p>
                        </fieldset>
                    </div>
                    <hr />
                    <div class="small-12">
                        <fieldset class="fieldset">
                            <legend><strong>Shipping Address</strong></legend>
                            <p>{{$order->shippingAddress->address_line_1}}<br/>
                            	{{$order->shippingAddress->address_line_2}}<br/>
                            	{{$order->shippingAddress->city}}<br/>
                            	{{$order->shippingAddress->county}}<br/>
                            	{{$order->shippingAddress->postcode}}<br/>
                            	{{$order->shippingAddress->country}}
                             </p>
                        </fieldset>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="small-12">
                <hr />
                <!-- <div class="float-left">
                    <label for="cancel" class="button">Cancel</label>
                    <input type="button" id="cancel" class="show-for-sr">
                </div>
                <div class="float-right">
                    <label for="submit" class="button primary">Save</label>
                    <input type="submit" id="submit" class="show-for-sr">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div> -->
            </div>
        </div>
    </form>
