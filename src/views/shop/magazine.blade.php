@section('facebook_meta') 
<meta property="og:url" content="{{ url('magazine') }}">
<meta property="og:image" content="{{\Storage::url('magazine/cover.jpg')}}">
<meta property="og:description" content="{{$magazine->productItem->sub_heading}}"> 
@endsection 
@extends('layouts.app') 
@section('pageType', 'At The Table Issue 2 | Magazine | ') 
@section('pageDesc', 'Our annual magazine focuses on British food culture and the stories that surround it, showcasing the best long-form food writing, photography and illustration. Printed on thick matte paper and thread sewn, it’s a beautifully designed magazine to collect and keep on your bookshelf with timeless themes and an innovative approach to its subject.')
@section('content')
<article role="main" class="light">
    <div class="grid-x align-center-middle flex-dir-column text-center">
        <div class="cell small-11 medium-5 flex-container align-center-middle magazine-image slider">
            <div class="slide">
                <img src="{{\Storage::url('products/' . $magazine->productItem->id . '/landscape-index.jpg')}}" />
            </div>
            @foreach($images as $image)
            <div class="slide">
                <img src="{{\Storage::url('products/' . $magazine->productItem->id . '/images/' . $image->id .'.jpg')}}" />
            </div>
            @endforeach
        </div>
        <div class="cell small-11 medium-5 align-center-middle flex-container magazine-header  flex-dir-column post-title  ">
            <h3 class="flex-child-shrink">MAGAZINE</h3>
            <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">
            {{$magazine->productItem->name}} 
            </h2>
            <p class="flex-child-shrink">
            {{$magazine->productItem->name}}
            </p>
        </div>
    
        <div class='small-11 medium-5 post-content magazine'>
            <p class="text-justify">
               {!! nl2br($magazine->productItem->description) !!}
            </p>
            <p>
            {!! nl2br($magazine->productItem->description) !!}
            </p>
            <p class="cost">&pound;{{number_format($magazine->productItem->price,0)}}</p>
            <form method="POST" action="{{ url('cart') }}">
                <input type="hidden" name="product_id" value="{{ $magazine->productItem->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if($magazine->productItem->stock > 0)
                <button type="submit" class="button  expanded add-to-cart">ADD TO CART</button>
                @else
                <button  disabled class="button  expanded add-to-cart">SOLD OUT</button>
                @endif
            </form>
        </div>
    </div>
</article>

@include('partials.readmore')

@endsection('content') @section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.slider').slick({
        lazyLoad: 'ondemand',
        prevArrow: '<div class="nav left-arrow"><img src="/img/left-arrow.svg"/></div>',
        nextArrow: '<div class="nav right-arrow"><img  src="/img/right-arrow.svg"/></div>',
        mobileFirst: true,
    });
});
</script>
@endsection('scripts')
