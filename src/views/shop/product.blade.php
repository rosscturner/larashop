@section('facebook_meta') 
<meta property="og:url" content="{{  url('/product/') }}/{{$product->slug}} ">
<meta property="og:image" content="{{\Storage::url('products/' . $product->product_id . '/landscape-index.jpg')}}">
<meta property="og:description" content="{{$product->name . ' - ' . $product->sub_heading }}"> 
@endsection 
@extends('layouts.app')
@section('pageType', $product->name . ' | Products | ' )
@section('pageDesc',$product->sub_heading)
@section('content')
<div class="editorial-wrapper">
    <article role="main" class="light article">
        <div class="grid-container">
            <header class="grid-x align-justify">
                <div class="cell small-11  medium-5 align-center-middle flex-container flex-dir-column post-title  ">
                    <h3 class="flex-child-shrink">PRODUCT</h3>
                    <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">
                        {{$product->name}}
                    </h2>
                    <p class="flex-child-shrink">
                        {{$product->sub_heading}}
                    </p>
                </div>
                <div class="cell small-11 mobile-social">
                    <p class="social mobile-social">
                            <a href="mailto:?subject={{$product->name}}&amp;body={{ url('/product') }}/{{$product->slug}}"
                                        title="Share by Email" >
                                        <img src='/img/social/mail.svg' />
                                    </a>
                            <a onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;" href="http://twitter.com/intent/tweet?status={{$product->name}}+{{ url('/product/') }}/{{$product->slug}}"><img src='/img/social/twitter.svg' /></a>
                                <a onclick="window.open(this.href,'_blank',
            'width=500,height=500,toolbar=0'); return false;"  href="http://www.facebook.com/sharer/sharer.php?u={{ url('/product/') }}/{{$product->slug}}&title={{$product->title}}"><img src='/img/social/facebook.svg' /></a>
                    </p>
                </div>
                <div class="cell small-11 medium-6 flex-container align-center-middle  product-header-image slider">
                    <div class="slide">
                        <img src="{{\Storage::url('products/' . $product->id . '/landscape-index.jpg')}}" class="lazyload"/>
                    </div>
                    @foreach($images as $image)
                    <div class="slide">
                        <img src="{{\Storage::url('products/' . $product->id . '/images/' . $image->id .'.jpg')}}" class="lazyload"/>
                    </div>
                    @endforeach
                </div>
            </header>
        </div>
    
        <div class="grid-container ">
            <div class="grid-x row-container">
                <div class='cell medium-6 post-content section-1 left'>
                    <p class="social desktop-social">
                        <a href="mailto:?subject={{$product->name}}&amp;body={{ url('/product') }}/{{$product->slug}}"
                                    title="Share by Email" >
                                    <img src='/img/social/mail.svg' />
                                </a>
                        <a onclick="window.open(this.href,'_blank',
        'width=500,height=500,toolbar=0'); return false;" href="http://twitter.com/intent/tweet?status={{$product->name}}+{{ url('/product/') }}/{{$product->slug}}"><img src='/img/social/twitter.svg' /></a>
                            <a onclick="window.open(this.href,'_blank',
        'width=500,height=500,toolbar=0'); return false;"  href="http://www.facebook.com/sharer/sharer.php?u={{ url('/product/') }}/{{$product->slug}}&title={{$product->title}}"><img src='/img/social/facebook.svg' /></a>
                    </p>
                </div>
                
                <div class='cell small-11 medium-6  post-content product section right'>
                    <p class="text-justify ">{!! nl2br($product->description) !!}</p>
                    <p class="cost">&pound;{{ number_format($product->price,0) }}</p>
                    <form method="POST" action="{{ url('cart') }}">
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if($product->stock > 0)
                        <button type="submit" class="button  expanded add-to-cart">ADD TO CART</button>
                        @else
                        <button  disabled class="button  expanded add-to-cart">SOLD OUT</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>
@include('partials.readmore')
@endsection('content')
@section('scripts')
<script type="text/javascript" >
$(document).ready(function(){
    $('.slider').slick({
        lazyLoad: 'ondemand',
        dots: true,
        adaptiveHeight: true,
        prevArrow: '<div class="nav left-arrow"><img src="/img/left-arrow.svg"/></div>',
        nextArrow: '<div class="nav right-arrow"><img  src="/img/right-arrow.svg"/></div>',
        mobileFirst:true,
    });
});

</script>
@endsection('scripts')
