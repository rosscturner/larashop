@extends('layouts.app') @section('pageType', 'Products | ' )@section('content')

<!-- Quote -->
<div class="light"> 
    <div class="grid-container">
        <div class="row-section grid-x flex-column-dir align-center-middle  section text-center">
            <h3 class="flex-child-shrink">PRODUCTS</h3>
            <p class="cell grid-x align-center-middle text-center">
                <span class="cell small-11 medium-8">
                    <br/>
                    <br/>
            At The Table presents a curated collection of the finest goods for your table. Focusing on timeless products to keep and collect, every piece is handcrafted by makers with a passion for what they do. We believe in products that are sustainable, thoughtful and made with care, each with their own unique story. 
                <br/>
                <br/>
                We are committed to working with and supporting the UK’s most exciting independent makers. Each product is a bespoke collaboration exclusively available through At The Table. We work with a talented group of designers, ceramicists, wood carvers, printmakers and entrepreneurs to create beautiful things for you to enjoy every day.
            </span>  
            </p>       
        </div>
    </div>
</div>
<div id="kana">
@include('pages.partials.video',['video' => $kana])
</div>
<div class="light">
    <div class="grid-container">
        <div class="row-section grid-x grid-padding-x small-up-1 medium-up-3 product-row" >
            @foreach ($products->slice(0,3) as $product)
                <a href="/product/{{$product->slug}}" class="cell product-container">
                    <div class="flex-container align-center row-image-{{($loop->iteration)}}">    
                    @if($loop->iteration % 2 == 0)
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                    @else
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                    @endif
                    </div>
                    <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
                        <h3 class="flex-child-shrink">SHOP</h3>
                        <h2 class="cell">{{ $product->name}}</h2>
                        <p class="cell text-center">{{$product->sub_heading}}</p>
                    </div>
                </a>
                @endforeach
        </div>
    </div>
</div>
@include('pages.partials.video-print',['video' => $anita])

<div class="light">
    <div class="grid-container">
        <div class="row-section grid-x  grid-padding-x small-up-1 medium-up-3 product-row">
            @foreach ($products->slice(6,3) as $product)
                <a href="/product/{{$product->slug}}" class="cell product-container">
                    <div class="flex-container align-center row-image-{{($loop->iteration)}}">    
                    @if($loop->iteration % 2 == 0)
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                    @else
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                    @endif
                    </div>
                    <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
                        <h3  class="flex-child-shrink">SHOP</h3>
                        <h2 class="cell">{{ $product->name}}</h2>
                        <p class="cell text-center">{{$product->sub_heading}}</p>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>
@include('pages.partials.video',['video' => $havelock])
<div class="light">
    <div class="grid-container">
        <div class="row-section grid-x  grid-padding-x small-up-1 medium-up-3 product-row">
            @foreach ($products->slice(3,3) as $product)
                <a href="/product/{{$product->slug}}" class="cell product-container">
                    <div class="flex-container align-center row-image-{{($loop->iteration)}}">    
                    @if($loop->iteration % 2 == 0)
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image"></div>
                    @else
                        <div data-bg="{{\Storage::url('products/' . $product->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                    @endif
                    </div>
                    <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
                        <h3  class="flex-child-shrink">SHOP</h3>
                        <h2 class="cell">{{ $product->name}}</h2>
                        <p class="cell text-center">{{$product->sub_heading}}</p>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>

@include('pages.partials.video',['video' => $nina])  
@endsection('content')
@section('scripts')
<script src="https://player.vimeo.com/api/player.js"></script>
<script type="text/javascript">
$(function() {
      //video TODO - move to external
      $('.video-container').on('click', function(){
        var $iframe = $(this).find('iframe');
        var $playbutton = $(this).find('.play-button');
        var player = new Vimeo.Player($iframe,{playsline: false});
        player.play().then(function() {
            $playbutton.fadeOut();
            $iframe.fadeTo('slow',1.0,function(){
                player.on('ended', function() {
                    //document.webkitExitFullscreen();
                    $iframe.fadeTo('slow',0);
                    $playbutton.fadeIn();
                });
            });    
        });
    });
});
</script>
@endsection