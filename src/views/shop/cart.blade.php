@extends('layouts.app') @section('pageType','Cart | ') @section('content')
<section id="sideCart" class="light">
    <div class="grid-container light grid-x align-center-middle">
        @if(count($cart))
        <div class="table-scroll">
        <table class="cell table-scroll">
            <thead>
                <tr>
                    <th width="150">Item</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="align-center-middle">
                @foreach($cart as $item)
                @php //dd($item) @endphp
                <tr class="item">
                    <td>
                        @if($item->associatedModel == 'Rosscturner\Larashop\Shop\Product\Product')
                        <a href=""><img src="{{\Storage::url('products/' . $item->model->product_id . '/landscape-index.jpg')}}" alt=""></a> 
                        @elseif($item->associatedModel == 'Rosscturner\Larashop\Shop\Product\Ticket')
                        <a href=""><img src="{{\Storage::url('event/' . $item->model->product_id . '/landscape-index.jpg')}}" alt=""></a> 
                        @else
                        <!-- <a href=""><img src="{{\Storage::url('magazine/cover.png')}}" alt=""></a>  -->
                        @endif
                    </td>
                    <td>
                        @if($item->model instanceof \App\Shop\Product\Ticket)
                            <h2>{{$item->model->description}}</h2>
                            @else
                            <h2>{{$item->name}}</h2>
                        @endif
                    </td>
                    <td>
                        &pound;{{number_format($item->price,0)}}
                    </td>
                    <td>
                        <div class="grid-x align-justify align-center-middle small-12">
                            <div class="small-6 cell">
                                <div class="cart_quantity_buttons flex-container ">
                                    <a class="cart_quantity_down" href='{{url("cart?product_id=$item->id&decrease=1")}}'>-</a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
                                    <a class="cart_quantity_up" href='{{url("cart?product_id=$item->id&increment=1")}}'>+</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        &pound;{{$item->subtotal}}
                    </td>
                    <td>
                        <a href='{{url("cart?product_id=$item->id&remove=1")}}'><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
        <div class="grid-container">
            <div class="grid-x align-right text-right cell">
                <p><strong>Sub-Total:</strong> &pound; {{\Cart::total()}}
                    <br/><small>Shipping calculated on checkout</small>
                </p>
            </div>
            <div class="cell grid-x align-justify">
                <a class="button" href="{{url('/#shop')}}">Continue Shopping</a>
                <a class="button" href="{{url('checkout')}}">Check Out</a>
            </div>
            <!--/#do_action-->
            @else
            <div class="row">
                <h2 class="small-12 column small-centered text-center" id="page-intro">YOU HAVE NO ITEMS IN YOUR CART</h2>
            </div>
            @endif
        </div>
    </div>
</section>
<!--/#cart_items-->
@endsection
