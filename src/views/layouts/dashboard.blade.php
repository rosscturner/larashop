<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @if(!empty($meta['title']))
        <title>{{ $meta['title'] . ' | AT THE TABLE' }}</title>
    @else
        <title>AT THE TABLE</title>
    @endif
    <meta name="HandheldFriendly" content="True" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/admin.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
</head>
<body id="app-layout">
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">AT THE TABLE | ADMIN </li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li><input type="search" placeholder="Search"></li>
      <li><button type="button" class="button">Search</button></li>
    </ul>
  </div>
</div>

<div class="small-2 column">
    <ul class="vertical menu dash-menu">
        <li>
            <a class="icon icon-data" href="/admin/blog">BLOG</a>
        </li>
        <li>
            <a class="icon icon-data" href="/admin/events">EVENTS</a>
        </li>
        <li>
            <a class="icon icon-data" href="/admin/products">PRODUCTS</a>
        </li>
        <li>
            <a class="icon icon-data" href="/admin/orders">ORDERS</a>
        </li>
    </ul>
</div>
<div class="small-10  column">
    @yield('content')
</div>

{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/admin.js"></script>

@yield('scripts')
<script type="text/javascript">
$(document).on('opened.zf.offcanvas', function ()  {
$('#nav-icon').addClass('open');
});
$(document).on('closed.zf.offcanvas', function ()  {
$('#nav-icon').removeClass('open');
});
</script>
</body>
</html>
