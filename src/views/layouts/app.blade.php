<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('pageType') At The Table</title>
    <meta name="Description" content="@yield('pageDesc')">
    <meta name="HandheldFriendly" content="True" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400i,700" rel="stylesheet">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    @yield('facebook_meta')
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-79733788-1', 'auto');
    ga('send', 'pageview');
    </script>
   
</head>

<body id="@yield('pageType')">
<header  id="main-nav">
    <div class="grid-container">
        <div class="grid-x align-middle align-center text-center primary-nav">
            <!-- menu -->
            <div data-toggle="offCanvas" class="cell align-self-justify text-left small-2">
                <img  src="/img/header/menu.svg" />
            </div>
            <!-- logo -->
            <a class="cell small-8" href="/">
                <img src="/img/header/logo.svg" id="logo"/>
            </a>
            <!-- CART -->
            <button  class="cell small-2 text-right" data-toggle="sideCart">
                <img src="/img/header/basket.svg" />
            </button>
        </div>
    </div>
</header>
    <div>
        <!-- Your menu or Off-canvas content goes here -->
        @include('partials.off-canvas-left')
        @include('partials.off-canvas-right')
        <div class="off-canvas-content" data-off-canvas-content>
            
            <div class="page-wrap">
            <!-- primary-content -->
                @yield('content')
            </div>
            <!-- footer -->
            <div class="grid-container">
                <footer class="grid-x small-up-1 medium-up-5" id="main-footer">
                    <span class="cell">
                        <img  src="/img/table-icon.jpg" />
                    </span>
                    <ul class="cell">
                        <li><a href="/about">ABOUT</a></li>
                        <li><a href="/contact">CONTACT US</a></li>
                        <li><a href="/bespoke">BESPOKE</a></li>
                    </ul>  
                    <ul class="cell">
                        <li><a href="/magazine">MAGAZINE</a></li>
                        <li><a href="/editorial">EDITORIAL</a></li>
                        <li><a href="/series/voices-at-the-table">EVENTS</a></li>
                        <li><a href="/products">PRODUCTS</a></li>
                    </ul>   
                    <ul class="cell">
                        <li><a href="#" data-open="faq">FAQ</a></li>
                        <li><a href="#" data-open="terms">TERMS &amp; CONDITIONS</a></li>
                        <li><a href="#" data-open="privacy">PRIVACY POLICY</a></li>
                    </ul>   
                    <div class="cell footer-social">
                        <!--Twitter -->
                        <a  target="_default" href="http://twitter.com/__atthetable__">
                            <img src="/img/social/twitter.svg" alt="Follow us on Twitter"/>
                        </a>
                        <!-- Facebook -->
                        <a  href="https://www.facebook.com/joinusatthetable/"  target="_default">
                            <img src="/img/social/facebook.svg" alt="Follow us on Facebook" />
                        </a>
                        <!-- insta -->
                        <a  target="_default" href="http://instagram.com/__atthetable__" >
                            <img src="/img/social/instagram.svg" alt="Follow us on Instagram" />
                        </a>
                    </div>
                    <div class="grid-x align-center-middle footer-newsletter">
                        <p class="small-11 cell medium-5 text-right">
                            NEWSLETTER
                        </p>
                        <div class="small-11 medium-6 cell">
                            <div class="small-5 medium-12">
                            @include('partials.newsletter')
                            </div>
                        </div>
                    </div>
                    </div>
                </footer>
            </div>
            @include('partials.modals')
        </div>
    </div>
</body>
<!-- /st-content -->
<!--/st-container-->
<!-- JavaScripts -->


<script type="text/javascript" src="/js/main.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/all.js"></script>
@yield('scripts')
<script type="text/javascript">
$(document).on('opened.zf.offcanvas', function() {
    $('#nav-icon').addClass('open');
});
$(document).on('closed.zf.offcanvas', function() {
    $('#nav-icon').removeClass('open');
});
</script>
</body>

</html>
