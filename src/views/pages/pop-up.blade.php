@extends('layouts.app') @section('pageType', 'PopUp | ') @section('content')
<div id="hero">
    <div id="hero-content" data-bg="{{\Storage::url('pages/pop-up-hero.jpg')}}" type="pop-up-image" class="lazyload" >
    </div>
</div>
<div clas="row">
    <div class="small-11 medium-6 column small-centered text-justify content">
        <p>
            <strong>Food conversations, magazines and handmade tableware</strong>
            <br/>
            30th November - 6th December 2016
            <br/><br/> The great intellectuals and artists of the 17th century met in literary salons to refine taste, increase knowledge and stimulate curiosity through conversation. It was rarely the solitude of the study that inspired sudden Eureka-moments, instead it was the fierce debates held in the salons that allowed ideas to expand and mature.
            <br/>
            <br/> To celebrate issue 2 of our annual food magazine, we’re thrilled to invite you to visit The Food Salon. Situated right at the heart of Soho, our pop up is designed as a sanctuary, a place to rest between hectic Christmas shopping. Engage in talks and debates about British food culture, listen to prose and poetry readings by magazine contributors, and discover bespoke handmade gifts.
            </p>
            <hr/>
            <img src="{{\Storage::url('email/joe-tea.jpg')}}" alt="Pop Up" />
             <hr/>
            <img src="{{\Storage::url('pages/shop-interior.jpg')}}" alt="Joe's Tea" />
            <hr/>
            <p>
            We’ve worked with the talented team at <a target="_default" href="http://eachlondon.com/">EACH</a> London to design The Food Salon and bring our ideas to life. You’ll be greeted with a complimentary cup of <a target="_default" href="http://joesteacompany.com">Joe’s Tea Co.</a> tea and invited to enjoy the ‘reading room’ experience – sit in cosy armchairs amidst beautifully-designed furniture from Carl Hansen & Søn and browse a curated collection of the best independent magazines.
        <br/>
        <br/>
        <p> To find out about our next pop up sign up to our newsletter below... </p>
        </p>
    </div>
</div>

<div class="clearfix"></div>
@endsection('content')
