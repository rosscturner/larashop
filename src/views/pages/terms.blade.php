@extends('layouts.app') 
@section('pageType', 'Terms and Conditions | ') 
@section('content')@section('pageDesc','At The Table is a creative platform that explores and celebrates British food culture')
@section('content')
<div id="hero">
    <div id="hero-content" data-bg="{{\Storage::url('pages/about.jpg')}}" type="about-image" class="lazyload" >
    </div>
</div>
<div clas="row">
    <div class="small-11 medium-8 column small-centered text-justify">
    <h2>PRIVACY POLICY</h2>
    <br/>
    <br/>
At The Table is committed to protecting your privacy. We will use the information that we collect about you in accordance with the Data Protection Act 1998 and the Privacy and Electronic Communications Regulations 2003. Our use of your personal data will always have a lawful basis, either because it is necessary to complete a booking or purchase, because you have consented to our use of your personal data (e.g. by subscribing to emails), or because it is in our legitimate interests. We will only share your information with companies if necessary to deliver services on our behalf.  For example third-party payment processors, and other third parties to provide our sites and fulfil your requests, and as otherwise consented to by you or as permitted by applicable law. <br/><br/>What information do we collect? <br/><br/>You give us your information when you book an event, make a purchase from our shop or sign up to our newsletter. 
 We use Stripe to process card transactions and do not store credit card details.   <br/><br/>We keep a record of the emails we send you, and we may track whether you receive or open them so we can make sure we are sending you the most relevant information. We may then track any subsequent actions online. <br/><br/>Like most websites, we receive and store certain details whenever you use the At The Table website.  We use “cookies” to help us make our site – and the way you might use it – better. There is more information on this below. 
<br/>
<br/>
What we use your personal information for 
<br/>
<br/>
When you provide us with personal information to book an event, complete a transaction, verify your credit card or place an order, we hold that information under legitimate interest and you have the right to be informed that we hold it, the right to access that data, to correct it if it is erroneous, to be forgotten, to restrict processing of that data, and to object to our processing of your data. You also have other rights under the GDPR, which you can find out about here <br/><br/>We aim to be clear when we collect your data and not to do anything you wouldn’t reasonably expect. If you make a purchase or book an event we usually collect your name and contact details and your bank or credit card information (if making a transaction via Stripe).     <br/><br/>We will include opt-out instructions in any marketing communications you receive from us. <br/><br/>Payment<br/><br/>We use Stripe to process payments.
 Stripe adheres to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover. <br/><br/>PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers. <br/><br/>
Third-party services In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us. <br/><br/>However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions. <br/><br/>For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers. <br/><br/>In particular, remember that certain providers may be located in or have facilities that are located in a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located. <br/><br/>Once you leave our store’s website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service. <br/><br/>Security <br/><br/>To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed. <br/><br/>If you provide us with your credit card information, the information is encrypted using secure socket layer technology (SSL) and stored with a AES-256 encryption.  Although no method of transmission over the Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement additional generally accepted industry standards. <br/><br/>Cookies<br/><br/>We use cookies on this website to provide you with a better user experience. We do this by placing a small text file on your device / computer hard drive to track how you use the website, to record or log whether you have seen particular messages that we display, to keep you logged into the website where applicable, to display relevant adverts or content, referred you to a third-party website.
<br/>
<br/>
Some cookies are required to enjoy and use the full functionality of this website.
We use a cookie control system which allows you to accept the use of cookies, and control which cookies are saved to your device/computer. Some cookies will be saved for specific time periods, where others may last indefinitely. Your web browser should 
see your web browser options.
<br/>
<br/>
Cookies that we use are;
<br/><br/>
XSRF-TOKEN
<br/><br/>
This cookie is used to prevent certain types of attacks known as ‘cross-site request forgery’ and also prevents other malicious users from hijacking or imitating a genuine users session.
<br/><br/>
atthetable_session
<br/><br/>
This cookie is set on any page when a visitor arrives. we use this to enable certain functionality when you visit our site such as our shopping cart and checkout process.
<br/><br/>
Stripe
__stripe_mid
__stripe_sid 
<br/><br/>
Stripe uses these cookies to remember who you are and to enable At The Table to process payments without storing any credit card information on its own servers.
<br/><br/>
Google Analytics
_ga
_gid
_gat
<br/><br/>
These cookies are set or updated on any page visited on the Verv website. See here for full details. Several cookies are set but all serve the same
<br/><br/>
Google Analytics Google Analytics is a third-party service that collects standard internet log information about our website visitors. This includes number of visitors to a specific page, whether they are accessing the page via laptop or mobile, and general demographic data. Google Analytics paints a useful picture of who visits our website and how people find out about us online. We do not use Google Analytics to identify anyone. <br/><br/>Social Media <br/><br/>We use social media to broadcast messages and updates about events and news. On occasion we may reply to comments or questions you make to us on social media platforms. Depending on your settings or the privacy policies social media and messaging services like Facebook, Instagram or Twitter, you might give the third party permission to access information from those accounts or services.  <br/><br/>Mailing Lists <br/><br/>You can unsubscribe from our mailing list at any time by clicking the unsubscribe link at the bottom of the email, or by contacting us at info@atthetable.co.uk. <br/><br/>Changes to this Privacy Policy <br/><br/>We reserve the right to modify this privacy policy at any time. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it. <br/><br/>Your Rights: <br/><br/>You have the following rights related to your personal data:
<br/>
The right to request a copy of personal information held about you<br/>
The right to request that inaccuracies be corrected<br/>
The right to request us to stop processing your personal data<br/>
The right to withdraw consent<br/>
The right to lodge a complaint with the Information Commissioner's Office or Fundraising Regulator<br/>
<br/><br/>
Questions and Contact Information   If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at info@atthetable.co.uk or by mail at At The Table, 16 Nickleby House, George Row, London, SE16 4UW.
<br/><br/>
    </div>
</div>

        @endsection('content')
