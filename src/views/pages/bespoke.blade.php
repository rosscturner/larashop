@extends('layouts.app') 
@section('pageType', 'Bespoke Content Creation | ') @section('content')
@section('content')@section('pageDesc','At The Table is available for select consultancy opportunities as well as film, event, web and editorial projects throughout the year.')
<div class="light">
    <div class="grid-container pages">
        <!-- <img src="{{\Storage::url('pages/about.jpg')}}" type="about-image" class="lazyload" /> -->
        <div class="grid-x align-center-middle flex-dir-column text-center">
            <div class="cell small-11 medium-6 flex-container align-center-middle">
                <img src="{{\Storage::url('pages/bespoke.jpg')}}" alt="" />
            </div>
            <div class="cell small-11 medium-6 text-justify grid-x flex-dir-column align-center-middle">
                <h3 class="flex-child-shrink">BESPOKE</h3>
                <div  class="text-center quote ">
                    <p>
                        <span>At The Table </span> is available for select consultancy opportunities
                    as well as film, event, web and editorial projects throughout the year. 
                    </p>
                </div>
                <p>
                    <br/>
                    We’ve worked with some of the best food and drink companies in the UK. Here’s a small selection:
                    <br/>
                    <br/> Fortnum &amp; Mason, Yauatcha, Bibendum, Natoora, Selfridges, Royal Doulton, Nyetimber, Hawksmoor, The Ginger Pig, Riedel, Carl Hansen, Firmdale, Square Peg, Kamm &amp; Sons, Ten Speed Press, Material Gallery, Gail’s Bakery, Second Home, Our London, Wholefoods, Smitten Kitchen, Habitat, Joe’s Tea Co, Rude Health, Borough Market, Cereal, Town Hall Hotel, Kit and Ace, Lyle’s, Rosewood London, Action Against Hunger…
                    <br/>
                </p>
            </div>
           
            <div class="small-11 medium-8 cell text-center get-in-touch">
                <a href="mailto:bespoke@atthetable.co.uk">
                    <span>Get in touch</span>
                    <br/>
                    bespoke@atthetable.co.uk
                </a>
            </div>

        </div>
    </div>
</div>
@endsection('content')
