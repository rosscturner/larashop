@extends('layouts.app') @section('pageType', '') @section('content')@section('pageDesc','At The Table is a creative platform that explores and celebrates British food culture')
@php
$texCount = 1;
@endphp
<div class="section light header">
    @include('pages.partials.video',['video' => $video])
</div>


<!-- Quote -->
<div class="quote"> 
    <div class="grid-container">
        <div class="row grid-x align-middle align-center section text-center stand-out">
            <p class="cell">
            <span>At The Table</span> is a creative <br/>
                platform that explores and celebrates <br/>
                British food culture 
            </p>
            <img  src="/img/table-icon.jpg">
        </div>
    </div>
</div>

<div class="section light">
    <div class="grid-container">
        @include('pages.partials.row', ['texCount' => $texCount])
        @php $texCount = $texCount + 3; @endphp
    </div>
</div>
<div class="section">
    @include('pages.partials.video-print',['video' => $videos->pop()])
</div>



<div class="section light">
    <div class="grid-container">
    @include('pages.partials.row', ['texCount' => $texCount])
    @php $texCount = $texCount + 3; @endphp
    </div>
</div>

<!-- newsletter -->
@include('pages.partials.newsletter')

<div class="section light">
    <div class="grid-container">
    @include('pages.partials.row', ['texCount' => $texCount])
    @php $texCount = $texCount + 3; @endphp
    </div>
</div>

<div class="section light ad-container-mobile">
    <a target="_default" href="http://www.ourvodka.com">
        <img class="lazyload" src="{{\Storage::url('ads/our-london.jpg')}} " />
    </a>
</div>


@include('pages.partials.ad')

<div class="section light" >
    <div class="grid-container">
    @include('pages.partials.row', ['texCount' => $texCount])
    @php $texCount = $texCount + 3; @endphp
    </div>
</div>
<div id="anita">
@include('pages.partials.video',['video' => $videos->pop()])
</div>

<div class="section light" >
    <div class="grid-container">
    @include('pages.partials.row', ['texCount' => $texCount])
    @php $texCount = $texCount + 3; @endphp
    </div>
</div>
<div id="havelock">
    @include('pages.partials.video',['video' => $videos->pop()])
</div>
<div class="section light" >
    <div class="grid-container">
    @include('pages.partials.row', ['texCount' => $texCount])
    @php $texCount = $texCount + 3; @endphp
    </div>
</div>
@endsection @section('scripts')
<script src="https://player.vimeo.com/api/player.js"></script>
<script type="text/javascript">
$(function() {
    // Init Controller
    var scrollMagicController = new ScrollMagic.Controller();
    // Create Animation for 0.5s
    var tween = TweenMax.to('.ad-container-desktop', 0, {
        scale: 1.04
    });

    var scene = new ScrollMagic.Scene({
        triggerElement: '#post-ad',
        duration: 300,
        reverse:true /* How many pixels to scroll / animate */
    })
    .setTween(tween)
    .addTo(scrollMagicController);
    });

    //video TODO - move to external
    $('.video-container').on('click', function(){
        var $iframe = $(this).find('iframe');
        var $playbutton = $(this).find('.play-button');
        var player = new Vimeo.Player($iframe,{playsline: false});
        player.play().then(function() {
            $playbutton.fadeOut();
            $iframe.fadeTo('slow',1.0,function(){
                player.on('ended', function() {
                    //document.webkitExitFullscreen();
                    $iframe.fadeTo('slow',0);
                    $playbutton.fadeIn();
                });
            });    
        });
    });

    //Podcast = TODO: move to external
    $('.podcast-container').on('click', function(){
        var $audio = $(this).find('audio')[0];
        var $playbutton = $(this).find('.play-button');
        var $pausebutton = $(this).find('.pause-button'); 
        $audio.play().then(function() {
            $playbutton.fadeOut(); 
            $pausebutton.fadeTo( "slow" , 1.0);
            $($audio).addClass('active');
        });

        $pausebutton.on('click', function(eve){
                eve.stopPropagation();
                $audio.pause();
                $pausebutton.fadeTo( "slow" , 0);
                $playbutton.fadeIn(); 
               
         
        });
    });


</script>
@endsection
