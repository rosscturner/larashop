@extends('layouts.app')
@section('pageType', 'About | ') 
@section('content')@section('pageDesc','At The Table is a creative platform that explores and celebrates British food culture')
@section('content')
<div class="light">
    <div class="grid-container pages">
        <!-- <img src="{{\Storage::url('pages/about.jpg')}}" type="about-image" class="lazyload" /> -->
        <div class="grid-x align-center-middle flex-dir-column text-center">
            <div class="cell small-11 medium-6 flex-container align-center-middle">
                <img src="https://at-the-table.s3-eu-west-1.amazonaws.com/pages/about.jpg" alt="" />
            </div>
            <div class="cell small-11 medium-6 text-justify grid-x flex-dir-column align-center-middle">
                <h3 class="flex-child-shrink">ABOUT</h3>
                <div  class="text-center quote">
                    <p >
                        <span>At The Table</span> is a creative <br/>
                            platform that explores and celebrates <br/>
                            British food culture 
                    </p>
                </div>
                <p>
                    <br/> We're interested in the stories, ideas and people behind what we eat and drink, exploring the gastronomic world through events, print, online and film. By bringing people together from different industries and backgrounds – chefs, producers, writers, historians, scientists, artists, filmmakers, entrepreneurs – we aim to look at food from a fresh perspective.
                    <br/>
                    <br/> Food can be a banal, everyday necessity, but it can also inspire and bring people together: to share conversations, to share ideas, to share knowledge, and feel connected. Our happiest moments are often created sitting round a table, sharing food and stories with people we love, people we admire, or even people we’ve just met.
                    <br/>
                    <br/> Join us at the table.
                    <br/>
                    <br/>
                </p>
            </div>
       
            <div class="cell small-11 medium-6 text-justify grid-x flex-dir-column align-center-middle">
                <h3 class="flex-child-shrink">HISTORY</h3>
                <div  class="text-center quote small-8">
                    <p >
                     We want to change the way people talk about food
                    </p>
                </div>
                <p>
                    <br/> In 2012 we founded TOAST Festival, a two-day event featuring 40 speakers across 15 events, as well as a pop up shop, bespoke coffee bar and art exhibition. In doing so we created a space for people to discuss and debate food culture.
                    <br/>
                    <br/> Since the festival, we’ve held events all over London: from debates on the future of meat and the role of women in the food industry, to dinners showcasing British produce, slow food brunches, wine talks and dessert parties.
                    <br/>
                    <br/> Our events are fun and informative, but they are fleeting. We therefore looked to create something more permanent, where food could be discussed in an intelligent, creative way. A space for writers, chefs, illustrators and photographers to explore the topics that matter most to them, irrespective of trends. Our annual print magazine is the result.
                    <br/>
                    <br/> We’ve continued the story through our short food films, online editorial, podcast and creative collaborations with both <a href="/shop">makers</a> and <a href="/bespoke">brands</a>.
                    <br/>
                    <br/>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection('content')
