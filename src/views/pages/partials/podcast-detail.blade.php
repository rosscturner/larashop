<div class="grid-container podcast">
    <div class="grid-x align-justify align-center-middle text-center" >
        <div class="cell small-10 medium-6 ">
            <div class="podcast-container">
                <div class="podcast-inner">
                    <img data-src="{{\Storage::url( $podcast->podcast->url . '.jpg')}}" class="lazyload"/>
                    <span class="bg-overlay" ></span>
                    <div class="play-button grid-x align-middle text-center">
                        <img data-src="/img/play-button.svg" class="lazyload"/>
                    </div>
                    <div class="pause-button grid-x align-middle text-center">
                        <img data-src="/img/pause-button.svg" class="lazyload"/>
                    </div>
                    <audio controls="true">
                        <source src="{{\Storage::url($podcast->podcast->url . '.mp3')}}" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                </div>
            </div>
        </div>
    
        <div class="cell medium-5 video-description align-center-middle flex-container flex-dir-column">
            <h3 class="category flex-child-shrink">{{$podcast->category->name}}</h3>
            <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">{{$podcast->title}}</h2>
            <p class="flex-child-shrink">  Hosts Miranda and Anna look back at the first ever
Voices At The Table event at LASSCO, an atmospheric antique and salvage warehouse in
Bermondsey, with readings from food writer Kay Plunkett-Hogge and writer and essayist
Megan Nolan. 
            <br/>
            <br/>
            Produced by Joel Porter at Dot Dot Dot Productions.
            </p>
        </div>
    </div>
</div>