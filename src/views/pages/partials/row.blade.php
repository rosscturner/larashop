<!-- All -->
<div class="grid-x grid-padding-x  small-up-1 medium-up-3 row-section">
    @foreach($collection->slice(0,3)  as $key => $item)
        @switch($item)
            @case($item instanceof \App\Blog\Post)
                <a href="/editorial/{{$item->slug}}" class="cell">
                    <div class="product-container">
                        @if($item->teaser === null || $item->teaser == '')
                        <div class="flex-container align-center row-image-{{($loop->iteration)}}">
                            @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('editorial/' . $item->id . '/portrait-index.jpg')}}" class="lazyload teaser-image portrait-teaser"></div>
                            <div data-bg="{{\Storage::url('editorial/' . $item->id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser"></div>
                            @else
                            <div data-bg="{{\Storage::url('editorial/' . $item->id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                            
                            @endif
                        </div>
                        @else
                        <div class="teaser-image texture texture-{{$texCount}}">
                            <p>{{$item->teaser}}</p>
                        </div>
                        @php $texCount++; @endphp
                        @endif
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{($loop->iteration)}}">
                            <h3 class="flex-child-shrink">{{$item->category->name}}</h3>
                            <h2 class="cell">{{ $item->title}}</h2>
                            <p class="cell">{{$item->excerpt}}</p>
                        </div>
                    </div>
                </a>
                <?php $collection->forget($key); ?>
            @break
            @case(isset($item->start_time))
                <a href="event/{{$item->slug}}" class="cell">
                    <div class="product-container">
                        @if($item->teaser === null)
                        <div class="flex-container align-center row-image-{{($loop->iteration)}}">
                        @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('event/' . $item->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image portrait-teaser"></div>
                            <div data-bg="{{\Storage::url('event/' . $item->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser"></div>
                        @else
                            <div data-bg="{{\Storage::url('event/' . $item->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image"></div>
                        @endif
                        </div>
                        @else
                        <div class="teaser-image texture texture-{{$texCount}}"><p>{{$item->teaser}}</p></div>
                        @php $texCount++; @endphp
                        @endif
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{$loop->iteration % 2 ? '1':'2'}}">
                            <h3 class=" flex-child-shrink">EVENT</h3>
                            <h2 class="cell">{{$item->name}}</h2>
                            <p class="cell">{{ date('d/m/Y', strtotime( $item->start_time) ) }}</p>
                        
                        </div>
                    </div>
                </a>
                <?php $collection->forget($key); ?>
                @break
            @case($item instanceof \Rosscturner\Larashop\Shop\Product\Product)
                <a href="product/{{$item->slug}}" class="cell">
                    <div class="product-container">
                        @if($item->teaser === null)
                        <div class="flex-container align-center row-image-{{($loop->iteration)}}">
                        @if($loop->iteration % 2 == 0)
                            <div data-bg="{{\Storage::url('products/' . $item->product_id . '/portrait-index.jpg')}}" class="lazyload teaser-image portrait-teaser" ></div>
                            <div data-bg="{{\Storage::url('products/' . $item->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image mobile-teaser" ></div>
                        @else
                            <div data-bg="{{\Storage::url('products/' . $item->product_id . '/landscape-index.jpg')}}" class="lazyload teaser-image" ></div>
                            
                        @endif
                        </div>
                        @else
                        <div class="teaser-image texture texture-{{$texCount}}">
                            <p>{{$item->teaser}}</p>
                        </div>
                        @php $texCount++; @endphp
                        @endif
                        <div class="grid-x flex-column-dir align-center-middle text-center header-style-{{$loop->iteration % 2 ? '1':'2'}}">
                            <h3 class="flex-child-shrink">SHOP</h3>
                            <h2 class="cell">{{ $item->sub_heading}}</h2>
                            <p class="cell">{{$item->name}}</p>
                        </div>
                    </div>
                </a>
                <?php $collection->forget($key); ?>
                @break
        @endswitch
    @endforeach
</div>