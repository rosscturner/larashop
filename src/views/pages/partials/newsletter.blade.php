<div class="grid-y grid-padding-y text-center newsletter" id="newsletter">
    <div class="grid-x align-center cell">
        <h3 class="cell shrink">NEWSLETTER</h3>
    </div>
    <div class="grid-x align-center cell">
        <p class="cell small-10 medium-4">Sign up to our monthly bulletin to receive food notes, event invitations and more... </p>
    </div>
    <div class="grid-x align-center cell">
        <div class="cell small-10 medium-4">
            @include('partials.newsletter')
        </div>
    </div>
</div>