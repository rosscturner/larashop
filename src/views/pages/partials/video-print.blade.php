
    <div class="grid-container video">
        <div class="grid-x align-justify align-center-middle text-center" data-open="videoModal">
            <div class="cell small-10 medium-6 ">
                <div class="video-container">
                    <div class="video-inner">
                        <img data-src="{{\Storage::url('videos/' . $video->video->id . '/index.jpg')}} " class="lazyload"/>
                        <span class="bg-overlay" ></span>
                        <div class="play-button grid-x align-middle text-center">
                            <img data-src="/img/play-button.svg" class="lazyload"/>
                        </div>
                        <iframe src="{{$video->video->url}}?autoplay=0&autopause=0&loop=0&?playsinline=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allow="autoplay fullscreen" ></iframe>
                    </div>
                </div>
            </div>
       
            <div class="cell medium-5 video-description align-center-middle flex-container flex-dir-column">
                <h3 class="category flex-child-shrink">{{$video->category->name}}</h3>
                <h2 class="flex-child-auto align-self-middle flex-container align-center-middle">{{$video->title}}</h2>
                <p class="flex-child-shrink">{{$video->excerpt}}</p>
                <a href="{{$video->video->call_to_action}}" class="button ">BUY PRINT</a>
            </div>
        </div>
    </div>