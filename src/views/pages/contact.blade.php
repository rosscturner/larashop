@extends('layouts.app') 
@section('pageType', 'Contact | ') @section('content')
@section('content')@section('pageDesc','At The Table is a creative platform that explores and celebrates British food culture')
<div class="light">
    <div class="grid-container pages">
        <!-- <img src="{{\Storage::url('pages/about.jpg')}}" type="about-image" class="lazyload" /> -->
        <div class="grid-x align-center-middle flex-dir-column text-center">
            <div class="cell small-11 medium-6 flex-container align-center-middle">
                <img src="{{\Storage::url('pages/contact.jpg')}}" alt="" />
            </div>
            <div class="cell small-11 medium-6 text-justify grid-x flex-dir-column align-center-middle">
                    <h3 class="flex-child-shrink">CONTACT</h3>
                    <div  class="text-center quote">
                        <p>
                        16 Nickley House<br/>
                            George Row<br/>
                            London <br/>
                            SE16 4UW
                            <br/>
                            <br/>
                        </p>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <span class="get-in-touch">
                    <a href="mailto:info@atthetable.co.uk">
                    <span>General</span>
                    <br/>
                        info@atthetable.co.uk
                    </a>
                </span>
                <br/><br/>
                <span class="get-in-touch">
                    <a href="mailto:editorial@atthetable.co.uk">
                        <span>Editorial</span>
                        <br/>
                        editorial@atthetable.co.uk
                    </a>
                </span>
                <br/><br/>
                <span class="get-in-touch">
                    <a href="mailto:bespoke@atthetable.co.uk">
                        <span>Bespoke</span>
                        <br/>
                        bespoke@atthetable.co.uk
                    </a>
                </span>
                <br/><br/>
            </div>
        </div>
    </div>
</div>
@endsection('content')
